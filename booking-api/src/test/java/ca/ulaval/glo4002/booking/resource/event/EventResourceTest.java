package ca.ulaval.glo4002.booking.resource.event;

import ca.ulaval.glo4002.booking.resource.event.request.EventRequest;
import ca.ulaval.glo4002.service.event.EventService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.verify;

import java.time.LocalDate;

@ExtendWith(MockitoExtension.class)
public class EventResourceTest {

    private static final LocalDate A_NEW_FESTIVAL_START_DATE = LocalDate.of(2050, 07, 20);
    private static final LocalDate A_NEW_FESTIVAL_END_DATE = LocalDate.of(2050, 07, 27);

    @Mock
    private EventService eventService;

    private EventResource eventResource;

    @BeforeEach
    public void setUp() {
        eventResource = new EventResource(eventService);
    }

    @Test
    public void whenChangeFestivalDates_thenEventServiceIsCalledToUpdateFestivalDates() {
        // when
        EventRequest eventRequest = createEventRequest();
        eventResource.changeFestivalDates(eventRequest);

        // then
        verify(eventService).updateFestivalStartDate(A_NEW_FESTIVAL_START_DATE);
        verify(eventService).updateFestivalEndDate(A_NEW_FESTIVAL_END_DATE);
    }

    @Test
    public void whenChangeFestivalDates_thenEventServiceIsCalledToUpdateOrderingPeriodDates() {
        // when
        EventRequest eventRequest = createEventRequest();
        eventResource.changeFestivalDates(eventRequest);

        // then
        verify(eventService).updateOrderingPeriodDates(A_NEW_FESTIVAL_START_DATE);
    }

    private EventRequest createEventRequest(){
        return new EventRequest(A_NEW_FESTIVAL_START_DATE, A_NEW_FESTIVAL_END_DATE);
    }
}
