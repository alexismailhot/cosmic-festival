package ca.ulaval.glo4002.booking.resource.program;

import ca.ulaval.glo4002.booking.resource.program.assembler.ArtistResponseAssembler;
import ca.ulaval.glo4002.booking.resource.program.assembler.ProgramSubmissionAssembler;
import ca.ulaval.glo4002.booking.resource.program.request.DailyProgramRequest;
import ca.ulaval.glo4002.booking.resource.program.request.ProgramRequest;
import ca.ulaval.glo4002.domain.artist.ArtistSortingType;
import ca.ulaval.glo4002.service.artist.ArtistService;
import ca.ulaval.glo4002.service.program.ProgramUnveilingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ProgramResourceTest {

    private static final ArtistSortingType LOW_COSTS = ArtistSortingType.LOW_COSTS;
    private static final ArtistSortingType POPULARITY = ArtistSortingType.MOST_POPULAR;
    private static final LocalDate A_LOCAL_DATE = LocalDate.of(2050, 7, 17);
    private static final String AN_ACTIVITY_VALUE = "yoga";
    private static final String A_SHOW_VALUE = "30 Seconds to Mars";
    private static final DailyProgramRequest A_DAILY_PROGRAM_REQUEST = new DailyProgramRequest(A_LOCAL_DATE, AN_ACTIVITY_VALUE, A_SHOW_VALUE);
    private static final List<DailyProgramRequest> SOME_DAILY_PROGRAM_REQUESTS = Collections.singletonList(A_DAILY_PROGRAM_REQUEST);
    private static final ProgramRequest A_PROGRAM_REQUEST = new ProgramRequest(SOME_DAILY_PROGRAM_REQUESTS);

    private ProgramResource programResource;

    @Mock
    private ArtistService artistService;

    @Mock
    private ProgramUnveilingService programUnveilingService;

    @BeforeEach
    public void setUp() {
        programResource = new ProgramResource(artistService, new ArtistResponseAssembler(), programUnveilingService,
                new ProgramSubmissionAssembler());
    }

    @Test
    public void whenGetOrderedByLowCosts_thenServiceIsCalledToGetArtistsByLowCosts() {
        // when
        programResource.getArtistsOrderedBy(LOW_COSTS);

        // then
        verify(artistService).getArtistsByLowCosts();
    }

    @Test
    public void whenGetOrderedByMostPopular_thenServiceIsCalledToGetArtistsByPopularity() {
        // when
        programResource.getArtistsOrderedBy(POPULARITY);

        // then
        verify(artistService).getArtistsByPopularity();
    }

    @Test
    public void whenCreateFestivalProgram_theProgramUnveilingServiceIsCalledToSetProgram() {
        // when
        programResource.createFestivalProgram(A_PROGRAM_REQUEST);

        // then
        verify(programUnveilingService, atLeast(1)).setFestivalProgram(any());
    }
}
