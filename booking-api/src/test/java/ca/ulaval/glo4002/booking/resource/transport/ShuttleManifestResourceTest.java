package ca.ulaval.glo4002.booking.resource.transport;

import ca.ulaval.glo4002.booking.resource.transport.assembler.ShuttleResponseAssembler;
import ca.ulaval.glo4002.booking.resource.transport.assembler.TransportResponseAssembler;
import ca.ulaval.glo4002.service.transport.ShuttleService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ShuttleManifestResourceTest {

    private static final String A_DATE = "2050-07-18";

    @Mock
    private ShuttleService shuttleService;

    private ShuttleManifestResource shuttleManifestResource;

    @BeforeEach
    void setUp() {
        TransportResponseAssembler transportResponseAssembler = new TransportResponseAssembler(new ShuttleResponseAssembler());
        shuttleManifestResource = new ShuttleManifestResource(shuttleService, transportResponseAssembler);
    }

    @Test
    void whenGetShuttlesWithNullDate_thenShuttleServiceIsCalledToReturnAllShuttles() {
        // when
        shuttleManifestResource.getShuttleManifests(null);

        // then
        verify(shuttleService).getAllShuttles();
    }

    @Test
    void whenGetShuttles_thenShuttleServiceIsCalledToGetShuttlesForGivenDate() {
        // when
        shuttleManifestResource.getShuttleManifests(A_DATE);

        // then
        verify(shuttleService).getShuttles(LocalDate.parse(A_DATE));
    }
}
