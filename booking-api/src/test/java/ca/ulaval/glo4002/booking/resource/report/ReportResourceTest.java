package ca.ulaval.glo4002.booking.resource.report;

import ca.ulaval.glo4002.booking.resource.report.assembler.OxygenResponseAssembler;
import ca.ulaval.glo4002.booking.resource.report.assembler.ProfitResponseAssembler;
import ca.ulaval.glo4002.domain.oxygen.OxygenInventory;
import ca.ulaval.glo4002.domain.price.Price;
import ca.ulaval.glo4002.domain.profit.IncomeStatement;
import ca.ulaval.glo4002.service.oxygen.OxygenService;
import ca.ulaval.glo4002.service.profit.ProfitService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportResourceTest {

    private static final Price A_PRICE = Price.of(1000);
    private static final IncomeStatement AN_INCOME_STATEMENT = new IncomeStatement(A_PRICE, A_PRICE, A_PRICE);

    @Mock
    private OxygenService oxygenService;

    @Mock
    private ProfitService profitService;

    @Mock
    private OxygenInventory oxygenInventory;

    private ReportResource reportResource;

    @BeforeEach
    public void setUp() {
        reportResource = new ReportResource(oxygenService, profitService, new OxygenResponseAssembler(), new ProfitResponseAssembler());
    }

    @Test
    public void whenGetOxygenReport_thenOxygenServiceIsCalledToGetOxygenInventory() {
        // when
        when(oxygenService.getInventory()).thenReturn(oxygenInventory);
        reportResource.getOxygenReport();

        // then
        verify(oxygenService).getInventory();
    }

    @Test
    public void whenGetOxygenReport_thenOxygenServiceIsCalledToGetAllOxygenHistories() {
        // when
        when(oxygenService.getInventory()).thenReturn(oxygenInventory);
        reportResource.getOxygenReport();

        // then
        verify(oxygenService).getAllOxygenHistories();
    }

    @Test
    public void whenGetProfitReport_thenProfitServiceIsCalledToGetIncomeStatement() {
        // when
        when(profitService.getIncomeStatement()).thenReturn(AN_INCOME_STATEMENT);
        reportResource.getProfitReport();

        // then
        verify(profitService).getIncomeStatement();
    }
}
