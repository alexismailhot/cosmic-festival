package ca.ulaval.glo4002.booking.resource.pass.factory;

import ca.ulaval.glo4002.booking.resource.pass.assembler.PassResponseAssembler;
import ca.ulaval.glo4002.booking.resource.pass.assembler.SinglePassResponseAssembler;
import ca.ulaval.glo4002.booking.resource.pass.response.PassResponse;
import ca.ulaval.glo4002.booking.resource.pass.response.SinglePassResponse;
import ca.ulaval.glo4002.domain.pass.DailyEventPass;
import ca.ulaval.glo4002.domain.pass.EventPass;
import ca.ulaval.glo4002.domain.pass.PassCategory;
import ca.ulaval.glo4002.domain.pass.PassNumber;
import ca.ulaval.glo4002.domain.price.Price;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static com.google.common.truth.Truth.assertThat;

class PassResponseFactoryTest {

    private static final PassNumber A_PASS_ID = new PassNumber(0L);
    private static final PassCategory A_PASS_CATEGORY = PassCategory.NEBULA;
    private static final Price A_PRICE = Price.of(100);
    private static final LocalDate AN_EVENT_DATE = LocalDate.of(2050, 7, 14);
    private PassResponseFactory passResponseFactory;

    @BeforeEach
    public void setUp() {
        Map<Class<? extends EventPass>, PassResponseAssembler> assemblers = new HashMap<>();
        assemblers.put(DailyEventPass.class, new SinglePassResponseAssembler());
        assemblers.put(EventPass.class, new PassResponseAssembler());
        passResponseFactory = new PassResponseFactory(assemblers);
    }

    @Test
    public void whenCreateAndPassHasOptionPackage_thenReturnedObjectIsNotOfTypeSinglePassResponse() {
        // when
        PassResponse passResponse = passResponseFactory.create(new EventPass(A_PASS_ID, A_PASS_CATEGORY, A_PRICE));

        // then
        assertThat(passResponse).isNotInstanceOf(SinglePassResponse.class);
    }

    @Test
    public void whenCreateAndPassHasOptionSinglePass_thenReturnedObjectIsOfTypeSinglePassResponse() {
        // when
        PassResponse passResponse = passResponseFactory.create(new DailyEventPass(A_PASS_ID, A_PASS_CATEGORY, AN_EVENT_DATE, A_PRICE));

        // then
        assertThat(passResponse).isInstanceOf(SinglePassResponse.class);
    }
}
