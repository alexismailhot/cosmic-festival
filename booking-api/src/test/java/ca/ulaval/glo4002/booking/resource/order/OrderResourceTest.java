package ca.ulaval.glo4002.booking.resource.order;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ca.ulaval.glo4002.booking.resource.order.assembler.OrderNumberAssembler;
import ca.ulaval.glo4002.booking.resource.order.assembler.OrderResponseAssembler;
import ca.ulaval.glo4002.booking.resource.order.request.OrderRequest;
import ca.ulaval.glo4002.booking.resource.pass.assembler.PassResponseAssembler;
import ca.ulaval.glo4002.booking.resource.pass.assembler.SinglePassResponseAssembler;
import ca.ulaval.glo4002.booking.resource.pass.factory.PassResponseFactory;
import ca.ulaval.glo4002.booking.resource.pass.request.PassRequest;
import ca.ulaval.glo4002.domain.order.Order;
import ca.ulaval.glo4002.domain.order.OrderNumber;
import ca.ulaval.glo4002.domain.order.VendorCode;
import ca.ulaval.glo4002.domain.order.exception.InvalidOrderException;
import ca.ulaval.glo4002.domain.pass.DailyEventPass;
import ca.ulaval.glo4002.domain.pass.EventPass;
import ca.ulaval.glo4002.domain.pass.PassCategory;
import ca.ulaval.glo4002.domain.pass.PassNumber;
import ca.ulaval.glo4002.domain.pass.PassOption;
import ca.ulaval.glo4002.domain.price.Price;
import ca.ulaval.glo4002.service.order.OrderService;
import ca.ulaval.glo4002.service.pass.PassService;

@ExtendWith(MockitoExtension.class)
class OrderResourceTest {

    private static final LocalDate AN_ORDER_DATE = LocalDate.of(2050, 6, 25);
    private static final LocalDate A_LOCAL_DATE = LocalDate.of(2050, 7, 12);
    private static final List<LocalDate> SOME_LOCAL_DATES = Collections.singletonList(A_LOCAL_DATE);
    private static final VendorCode A_VENDOR_CODE = VendorCode.TEAM;
    private static final PassOption A_PACKAGE_PASS_OPTION = PassOption.PACKAGE;
    private static final LocalDate A_DATE = LocalDate.of(2050, 7, 12);
    private static final List<LocalDate> SOME_DATES = Collections.singletonList(A_DATE);
    private static final PassCategory A_PASS_CATEGORY = PassCategory.NEBULA;
    private static final String AN_ORDER_NUMBER_VALUE = "anOrderNumber";
    private static final OrderNumber AN_ORDER_NUMBER = new OrderNumber(AN_ORDER_NUMBER_VALUE);
    private static final PassOption A_SINGLE_PASS_OPTION = PassOption.SINGLEPASS;
    private static final Price A_PRICE = Price.of(100);
    private static final PassNumber A_PASS_NUMBER = new PassNumber(0L);
    private static final EventPass AN_EVENT_PASS = new EventPass(A_PASS_NUMBER, A_PASS_CATEGORY, A_PRICE);
    private static final List<EventPass> SOME_EVENT_PASSES = Collections.singletonList(AN_EVENT_PASS);
    private static final Order AN_ORDER = new Order(AN_ORDER_NUMBER, SOME_EVENT_PASSES, Collections.emptyList(), Collections.emptyList());

    @Mock
    private OrderService orderService;

    @Mock
    private PassService passService;

    private OrderResource orderResource;

    @BeforeEach
    public void setUp() {
        Map<Class<? extends EventPass>, PassResponseAssembler> assemblers = new HashMap<>();
        assemblers.put(DailyEventPass.class, new SinglePassResponseAssembler());
        assemblers.put(EventPass.class, new PassResponseAssembler());
        OrderResponseAssembler orderResponseAssembler = new OrderResponseAssembler(new PassResponseFactory(assemblers));
        orderResource = new OrderResource(orderService, passService, orderResponseAssembler, new OrderNumberAssembler());
    }

    @Test
    public void whenCreateOrder_thenResponseWithLocationHeaderIsReturned() {
        // when
        when(passService.buyEventPass(refEq(AN_ORDER_DATE), eq(A_VENDOR_CODE), eq(A_PASS_CATEGORY))).thenReturn(AN_ORDER_NUMBER);
        Response response = orderResource.createOrder(createDefaultOrderRequest());

        // then
        assertThat(response.getHeaders().get("Location").get(0)).isEqualTo(URI.create("/orders/" + AN_ORDER_NUMBER.toString()));
    }

    @Test
    public void whenCreateOrderWithPassOptionIsPackage_thenPassServiceIsCalledToBuyEventPass() {
        // when
        when(passService.buyEventPass(refEq(AN_ORDER_DATE), eq(A_VENDOR_CODE), eq(A_PASS_CATEGORY))).thenReturn(AN_ORDER_NUMBER);
        orderResource.createOrder(createOrderRequest(A_PACKAGE_PASS_OPTION, null));

        // then
        verify(passService).buyEventPass(refEq(AN_ORDER_DATE), eq(A_VENDOR_CODE), eq(A_PASS_CATEGORY));
    }

    @Test
    public void whenCreateOrderWithPassOptionIsSinglePass_thenPassServiceIsCalledToBuyDailyEventPasses() {
        // when
        when(passService.buyDailyEventPasses(refEq(AN_ORDER_DATE), eq(A_VENDOR_CODE), eq(A_PASS_CATEGORY), eq(SOME_DATES)))
                        .thenReturn(AN_ORDER_NUMBER);
        orderResource.createOrder(createOrderRequest(A_SINGLE_PASS_OPTION, SOME_LOCAL_DATES));

        // then
        verify(passService).buyDailyEventPasses(refEq(AN_ORDER_DATE), eq(A_VENDOR_CODE), eq(A_PASS_CATEGORY), eq(SOME_DATES));
    }

    @Test
    public void whenCreateOrderWithPassOptionIsPackageAndEventDates_thenInvalidOrderExceptionIsThrown() {
        // then
        Assertions.assertThrows(InvalidOrderException.class, () -> {
            // when
            OrderRequest orderRequest = createOrderRequest(A_PACKAGE_PASS_OPTION, SOME_LOCAL_DATES);
            orderResource.createOrder(orderRequest);
        });
    }

    @Test
    public void whenGetOrder_thenOrderServiceIsCalledToGetOrder() {
        // when
        when(orderService.getOrder(AN_ORDER_NUMBER)).thenReturn(AN_ORDER);
        orderResource.getOrder(AN_ORDER_NUMBER_VALUE);

        // then
        verify(orderService).getOrder(AN_ORDER_NUMBER);
    }

    private OrderRequest createDefaultOrderRequest() {
        return createOrderRequest(A_PACKAGE_PASS_OPTION, null);
    }

    private OrderRequest createOrderRequest(PassOption passOption, List<LocalDate> eventDates) {
        return new OrderRequest(AN_ORDER_DATE, A_VENDOR_CODE, createPassRequest(passOption, eventDates));
    }

    private PassRequest createPassRequest(PassOption passOption, List<LocalDate> eventDates) {
        return new PassRequest(A_PASS_CATEGORY, passOption, eventDates);
    }
}
