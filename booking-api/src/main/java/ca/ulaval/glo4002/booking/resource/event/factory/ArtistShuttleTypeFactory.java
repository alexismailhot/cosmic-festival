package ca.ulaval.glo4002.booking.resource.event.factory;

import ca.ulaval.glo4002.domain.artist.ArtistShuttleType;
import ca.ulaval.glo4002.domain.artist.configuration.ArtistShuttleTypeConfiguration;
import ca.ulaval.glo4002.domain.transport.ShuttleType;
import ca.ulaval.glo4002.infra.artist.GLOW4002ArtistShuttleTypeConfiguration;

public class ArtistShuttleTypeFactory {

    private static final ShuttleType ET_SPACESHIP = ShuttleType.ET_SPACESHIP;
    private static final ShuttleType MILLENIUM_FALCON = ShuttleType.MILLENIUM_FALCON;

    public ArtistShuttleType create() {
        ArtistShuttleTypeConfiguration artistShuttleTypeConfiguration = new GLOW4002ArtistShuttleTypeConfiguration(ET_SPACESHIP, MILLENIUM_FALCON);
        return new ArtistShuttleType(artistShuttleTypeConfiguration);
    }
}
