package ca.ulaval.glo4002.booking.resource.program.factory;

import ca.ulaval.glo4002.booking.resource.program.ProgramResource;
import ca.ulaval.glo4002.booking.resource.program.assembler.ArtistResponseAssembler;
import ca.ulaval.glo4002.booking.resource.program.assembler.ProgramSubmissionAssembler;
import ca.ulaval.glo4002.domain.artist.ArtistOxygenNeeds;
import ca.ulaval.glo4002.domain.artist.ArtistPriceComparator;
import ca.ulaval.glo4002.domain.artist.ArtistShuttleType;
import ca.ulaval.glo4002.domain.event.FestivalDates;
import ca.ulaval.glo4002.domain.pass.repository.EventPassRepository;
import ca.ulaval.glo4002.domain.profit.factory.TransactionFactory;
import ca.ulaval.glo4002.domain.profit.repository.TransactionRepository;
import ca.ulaval.glo4002.infra.artist.ExternalServiceApiRepository;
import ca.ulaval.glo4002.infra.artist.assembler.ArtistAssembler;
import ca.ulaval.glo4002.infra.artist.assembler.ArtistNumberAssembler;
import ca.ulaval.glo4002.service.AppConfig;
import ca.ulaval.glo4002.service.artist.ArtistService;
import ca.ulaval.glo4002.service.oxygen.OxygenService;
import ca.ulaval.glo4002.service.program.ProgramUnveilingService;
import ca.ulaval.glo4002.service.program.ProgramValidator;
import ca.ulaval.glo4002.service.program.assembler.DailyProgramAssembler;
import ca.ulaval.glo4002.domain.transport.TransportBooker;

public class ProgramResourceFactory {

    private final AppConfig appConfig;

    public ProgramResourceFactory(AppConfig appConfig) {
        this.appConfig = appConfig;
    }

    public ProgramResource create(TransportBooker transportBooker,
                                  EventPassRepository eventPassRepository, OxygenService oxygenService,
                                  FestivalDates festivalDates,
                                  TransactionRepository transactionRepository,
                                  ArtistOxygenNeeds artistOxygenNeeds,
                                  ArtistShuttleType artistShuttleType,
                                  TransactionFactory transactionFactory) {
        ArtistResponseAssembler artistResponseAssembler = new ArtistResponseAssembler();
        ArtistAssembler artistAssembler = new ArtistAssembler(new ArtistNumberAssembler());
        ExternalServiceApiRepository artistRepository = new ExternalServiceApiRepository(artistAssembler, appConfig.getExternalServiceApiUrl());
        ArtistService artistService = new ArtistService(artistRepository, new ArtistPriceComparator());

        ProgramUnveilingService programUnveilingService = new ProgramUnveilingService(transportBooker,
                artistService,
                oxygenService,
                transactionRepository,
                eventPassRepository,
                festivalDates,
                new ProgramValidator(festivalDates),
                new DailyProgramAssembler(),
                artistOxygenNeeds,
                artistShuttleType,
                transactionFactory);

        return new ProgramResource(artistService, artistResponseAssembler, programUnveilingService, new ProgramSubmissionAssembler());
    }
}
