package ca.ulaval.glo4002.booking.resource.program.response;

import java.util.List;

public class ArtistResponse {

    private final List<String> artists;

    public ArtistResponse(List<String> artists) {
        this.artists = artists;
    }

    public List<String> getArtists() {
        return artists;
    }
}
