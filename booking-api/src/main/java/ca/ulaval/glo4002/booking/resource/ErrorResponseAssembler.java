package ca.ulaval.glo4002.booking.resource;

public class ErrorResponseAssembler {

    public ErrorResponse assemble(String errorMessage, String errorDescription) {
        return new ErrorResponse(errorMessage, errorDescription);
    }
}
