package ca.ulaval.glo4002.booking.mapper;

import ca.ulaval.glo4002.booking.resource.ErrorResponse;
import ca.ulaval.glo4002.booking.resource.ErrorResponseAssembler;
import org.glassfish.jersey.server.ParamException.PathParamException;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

public class PathParamExceptionMapper implements ExceptionMapper<PathParamException> {

    private static final String ERROR_MESSAGE = "INVALID_FORMAT";
    private static final String ERROR_DESCRIPTION = "invalid format";

    private final ErrorResponseAssembler errorResponseAssembler;

    public PathParamExceptionMapper(ErrorResponseAssembler errorResponseAssembler) {
        this.errorResponseAssembler = errorResponseAssembler;
    }

    @Override
    public Response toResponse(PathParamException e) {
        ErrorResponse errorResponse = errorResponseAssembler.assemble(ERROR_MESSAGE, ERROR_DESCRIPTION);
        return Response.status(Status.BAD_REQUEST)
                .entity(errorResponse)
                .build();
    }
}
