package ca.ulaval.glo4002.booking.mapper;

import ca.ulaval.glo4002.booking.resource.ErrorResponse;
import ca.ulaval.glo4002.booking.resource.ErrorResponseAssembler;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

public class InvalidFormatExceptionMapper implements ExceptionMapper<InvalidFormatException> {

    private static final String ERROR_MESSAGE = "INVALID_FORMAT";
    private static final String ERROR_DESCRIPTION = "invalid format";

    private final ErrorResponseAssembler errorResponseAssembler;

    public InvalidFormatExceptionMapper(ErrorResponseAssembler errorResponseAssembler) {
        this.errorResponseAssembler = errorResponseAssembler;
    }

    @Override
    public Response toResponse(InvalidFormatException e) {
        ErrorResponse errorResponse = errorResponseAssembler.assemble(ERROR_MESSAGE, ERROR_DESCRIPTION);
        return Response.status(Status.BAD_REQUEST)
                .entity(errorResponse)
                .build();
    }
}
