package ca.ulaval.glo4002.booking.resource.transport.response;

import java.util.List;

public class ShuttleResponse {

    private final String date;
    private final String shuttleName;
    private final List<Long> passengers;

    public ShuttleResponse(String date, String shuttleName, List<Long> passengers) {
        this.date = date;
        this.shuttleName = shuttleName;
        this.passengers = passengers;
    }

    public String getDate() {
        return date;
    }

    public String getShuttleName() {
        return shuttleName;
    }

    public List<Long> getPassengers() {
        return passengers;
    }
}
