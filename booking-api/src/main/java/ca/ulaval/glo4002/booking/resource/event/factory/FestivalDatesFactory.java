package ca.ulaval.glo4002.booking.resource.event.factory;

import ca.ulaval.glo4002.domain.event.FestivalDates;
import ca.ulaval.glo4002.infra.event.configuration.GLOW4002FestivalDatesConfiguration;

import java.time.LocalDate;

public class FestivalDatesFactory {

    private static final LocalDate FESTIVAL_START_DATE = LocalDate.of(2050, 7, 17);
    private static final LocalDate FESTIVAL_END_DATE = LocalDate.of(2050, 7, 24);
    private static final LocalDate FESTIVAL_PROGRAM_UNVEILING_DATE = LocalDate.of(2050, 7, 12);

    public FestivalDates create() {
        GLOW4002FestivalDatesConfiguration festivalDatesConfiguration = new GLOW4002FestivalDatesConfiguration(FESTIVAL_START_DATE,
                FESTIVAL_END_DATE, FESTIVAL_PROGRAM_UNVEILING_DATE);
        return new FestivalDates(festivalDatesConfiguration);
    }
}
