package ca.ulaval.glo4002.booking.resource.report.assembler;

import java.util.List;
import java.util.stream.Collectors;

import ca.ulaval.glo4002.booking.resource.report.response.OxygenHistoryResponse;
import ca.ulaval.glo4002.booking.resource.report.response.OxygenInventoryResponse;
import ca.ulaval.glo4002.booking.resource.report.response.OxygenResponse;
import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;
import ca.ulaval.glo4002.domain.oxygen.OxygenHistory;
import ca.ulaval.glo4002.domain.oxygen.OxygenInventory;

public class OxygenResponseAssembler {

    public OxygenResponse assemble(OxygenInventory oxygenInventory, List<OxygenHistory> oxygenHistories) {
        return new OxygenResponse(assembleInventoryResponses(oxygenInventory), assembleHistoryResponses(oxygenHistories));
    }

    private List<OxygenInventoryResponse> assembleInventoryResponses(OxygenInventory oxygenInventory) {
        return oxygenInventory.getDistinctProducedGrades()
                .stream()
                .map(oxygenGrade -> assembleInventoryResponse(oxygenInventory, oxygenGrade))
                .collect(Collectors.toList());

    }

    private OxygenInventoryResponse assembleInventoryResponse(OxygenInventory oxygenInventory, OxygenGrade oxygenGrade) {
        return new OxygenInventoryResponse(oxygenGrade.toString(), oxygenInventory.getNumberOfTanksForGrade(oxygenGrade));
    }

    private List<OxygenHistoryResponse> assembleHistoryResponses(List<OxygenHistory> oxygenHistories) {
        return oxygenHistories.stream().map(this::assembleHistoryResponse).collect(Collectors.toList());
    }

    private OxygenHistoryResponse assembleHistoryResponse(OxygenHistory oxygenHistory) {
        return new OxygenHistoryResponse(oxygenHistory.getDate().toString(), oxygenHistory.getNumberOfTanksBought(),
                oxygenHistory.getLitersOfWaterUsed(), oxygenHistory.getNumberOfCandlesUsed(), oxygenHistory.getNumberOfTanksMade());
    }
}
