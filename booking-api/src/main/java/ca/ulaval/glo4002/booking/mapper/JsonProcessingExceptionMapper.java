package ca.ulaval.glo4002.booking.mapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.core.JsonProcessingException;

import ca.ulaval.glo4002.booking.resource.ErrorResponse;
import ca.ulaval.glo4002.booking.resource.ErrorResponseAssembler;

@Provider
public class JsonProcessingExceptionMapper implements ExceptionMapper<JsonProcessingException> {

    private static final String ERROR_MESSAGE = "INVALID_PROGRAM";
    private static final String ERROR_DESCRIPTION = "the program is invalid";

    private final ErrorResponseAssembler errorResponseAssembler;

    public JsonProcessingExceptionMapper(ErrorResponseAssembler errorResponseAssembler) {
        this.errorResponseAssembler = errorResponseAssembler;
    }

    @Override
    public Response toResponse(JsonProcessingException e) {
        try {
            String message = e.getCause().getMessage();
            if (message.contains("Duplicate field 'pm'") || message.contains("Duplicate field 'am'")) {
                ErrorResponse errorResponse = errorResponseAssembler.assemble(ERROR_MESSAGE, ERROR_DESCRIPTION);
                return Response.status(Response.Status.BAD_REQUEST)
                        .entity(errorResponse)
                        .build();
            }
        } catch (Exception ignored) { }
        return Response.status(Response.Status.BAD_REQUEST)
                .build();
    }
}
