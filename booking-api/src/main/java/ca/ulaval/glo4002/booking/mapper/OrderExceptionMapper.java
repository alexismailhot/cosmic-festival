package ca.ulaval.glo4002.booking.mapper;

import ca.ulaval.glo4002.booking.resource.ErrorResponse;
import ca.ulaval.glo4002.booking.resource.ErrorResponseAssembler;
import ca.ulaval.glo4002.domain.order.exception.OrderException;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

public abstract class OrderExceptionMapper<T extends OrderException> implements ExceptionMapper<T> {

    private final Status status;
    private final ErrorResponseAssembler errorResponseAssembler;

    public OrderExceptionMapper(Status status, ErrorResponseAssembler errorResponseAssembler) {
        this.status = status;
        this.errorResponseAssembler = errorResponseAssembler;
    }

    @Override
    public Response toResponse(T orderException) {
        String errorMessage = orderException.getErrorMessage();
        String errorDescription = orderException.getErrorDescription();
        ErrorResponse errorResponse = errorResponseAssembler.assemble(errorMessage, errorDescription);
        return Response.status(status)
                .entity(errorResponse)
                .build();
    }
}
