package ca.ulaval.glo4002.booking.resource.report.factory;

import ca.ulaval.glo4002.service.profit.IncomeStatementFactory;
import ca.ulaval.glo4002.domain.profit.repository.TransactionRepository;
import ca.ulaval.glo4002.service.profit.ProfitService;

public class ProfitServiceFactory {
    public ProfitService create(TransactionRepository transactionRepository, IncomeStatementFactory incomeStatementFactory) {
        return new ProfitService(transactionRepository, incomeStatementFactory);
    }
}
