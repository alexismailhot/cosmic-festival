package ca.ulaval.glo4002.booking.resource.order.factory;

import ca.ulaval.glo4002.domain.discount.Discounts;
import ca.ulaval.glo4002.domain.discount.MultiplePassesForSameCategoryPassDiscount;
import ca.ulaval.glo4002.domain.discount.MultiplePassesOfSameCategoryOrderDiscount;
import ca.ulaval.glo4002.domain.discount.OrderDiscount;
import ca.ulaval.glo4002.domain.discount.PassDiscount;
import ca.ulaval.glo4002.domain.discount.configuration.DiscountsConfiguration;
import ca.ulaval.glo4002.domain.order.factory.OrderFactory;
import ca.ulaval.glo4002.domain.order.factory.OrderNumberFactory;
import ca.ulaval.glo4002.domain.order.repository.OrderRepository;
import ca.ulaval.glo4002.domain.pass.PassCategory;
import ca.ulaval.glo4002.domain.price.Price;
import ca.ulaval.glo4002.domain.profit.factory.TransactionFactory;
import ca.ulaval.glo4002.domain.profit.repository.TransactionRepository;
import ca.ulaval.glo4002.infra.discount.configuration.GLOW4002DiscountsConfiguration;
import ca.ulaval.glo4002.infra.order.repository.OrderRepositoryInMemory;
import ca.ulaval.glo4002.service.order.OrderService;

import java.util.ArrayList;
import java.util.List;

public class OrderServiceFactory {

    private static final Price FIVE_OR_MORE_SUPERGIANT_PASS_DISCOUNT = Price.of(10000);
    private static final int FOUR_OR_MORE_NEBULA_PERCENT_DISCOUNT = 10;

    public OrderService create(TransactionRepository transactionRepository, TransactionFactory transactionFactory) {
        OrderRepository orderRepository = new OrderRepositoryInMemory();
        OrderNumberFactory orderNumberFactory = new OrderNumberFactory(orderRepository);

        List<OrderDiscount> currentOrderDiscounts = new ArrayList<>();
        currentOrderDiscounts.add(new MultiplePassesOfSameCategoryOrderDiscount(PassCategory.NEBULA, 4,
                FOUR_OR_MORE_NEBULA_PERCENT_DISCOUNT));

        List<PassDiscount> currentPassDiscounts = new ArrayList<>();
        currentPassDiscounts.add(new MultiplePassesForSameCategoryPassDiscount(PassCategory.SUPERGIANT, 5,
                FIVE_OR_MORE_SUPERGIANT_PASS_DISCOUNT));

        DiscountsConfiguration discountsConfiguration = new GLOW4002DiscountsConfiguration(currentOrderDiscounts, currentPassDiscounts);
        Discounts discounts = new Discounts(discountsConfiguration);
        OrderFactory orderFactory = new OrderFactory(orderNumberFactory, discounts);

        return new OrderService(orderFactory, orderRepository, transactionRepository, transactionFactory);
    }
}
