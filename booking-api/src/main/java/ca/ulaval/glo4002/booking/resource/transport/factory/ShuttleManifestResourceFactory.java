package ca.ulaval.glo4002.booking.resource.transport.factory;

import ca.ulaval.glo4002.booking.resource.transport.ShuttleManifestResource;
import ca.ulaval.glo4002.booking.resource.transport.assembler.ShuttleResponseAssembler;
import ca.ulaval.glo4002.booking.resource.transport.assembler.TransportResponseAssembler;
import ca.ulaval.glo4002.service.transport.ShuttleService;

public class ShuttleManifestResourceFactory {

    private final ShuttleService shuttleService;

    public ShuttleManifestResourceFactory(ShuttleService shuttleService) {
        this.shuttleService = shuttleService;
    }

    public ShuttleManifestResource create() {
        ShuttleResponseAssembler shuttleResponseAssembler = new ShuttleResponseAssembler();
        TransportResponseAssembler transportResponseAssembler = new TransportResponseAssembler(
                shuttleResponseAssembler);

        return new ShuttleManifestResource(shuttleService, transportResponseAssembler);
    }
}
