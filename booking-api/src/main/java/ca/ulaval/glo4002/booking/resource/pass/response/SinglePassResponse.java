package ca.ulaval.glo4002.booking.resource.pass.response;

public class SinglePassResponse extends PassResponse {

    private final String eventDate;

    public SinglePassResponse(long passNumber, String passCategory, String passOption, String eventDate) {
        super(passNumber, passCategory, passOption);
        this.eventDate = eventDate;
    }

    public String getEventDate() {
        return eventDate;
    }
}
