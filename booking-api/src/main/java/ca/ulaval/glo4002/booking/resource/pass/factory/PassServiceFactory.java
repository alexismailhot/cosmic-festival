package ca.ulaval.glo4002.booking.resource.pass.factory;

import ca.ulaval.glo4002.domain.pass.PassPrices;
import ca.ulaval.glo4002.domain.pass.factory.EventPassFactory;
import ca.ulaval.glo4002.domain.pass.factory.PassNumberFactory;
import ca.ulaval.glo4002.domain.pass.repository.EventPassRepository;
import ca.ulaval.glo4002.service.order.OrderService;
import ca.ulaval.glo4002.service.order.OrderValidator;
import ca.ulaval.glo4002.service.oxygen.OxygenService;
import ca.ulaval.glo4002.service.pass.PassService;
import ca.ulaval.glo4002.domain.transport.TransportBooker;

public class PassServiceFactory {

    private final OrderService orderService;
    private final TransportBooker transportBooker;
    private final OxygenService oxygenService;
    private final OrderValidator orderValidator;

    public PassServiceFactory(OrderService orderService, TransportBooker transportBooker, OxygenService oxygenService,
                              OrderValidator orderValidator) {
        this.orderService = orderService;
        this.transportBooker = transportBooker;
        this.oxygenService = oxygenService;
        this.orderValidator = orderValidator;
    }

    public PassService create(EventPassRepository eventPassRepository, PassPrices passPrices) {
        PassNumberFactory passNumberFactory = new PassNumberFactory(eventPassRepository);

        EventPassFactory eventPassFactory = new EventPassFactory(passNumberFactory, passPrices);

        return new PassService(eventPassFactory, eventPassRepository, orderService, transportBooker, oxygenService, orderValidator);
    }
}
