package ca.ulaval.glo4002.booking.mapper;

import ca.ulaval.glo4002.booking.resource.ErrorResponseAssembler;
import ca.ulaval.glo4002.domain.order.exception.OrderNotFoundException;

import javax.ws.rs.core.Response.Status;

public class OrderNotFoundExceptionMapper extends OrderExceptionMapper<OrderNotFoundException> {

    public OrderNotFoundExceptionMapper(ErrorResponseAssembler errorResponseAssembler) {
        super(Status.NOT_FOUND, errorResponseAssembler);
    }
}
