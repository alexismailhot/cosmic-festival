package ca.ulaval.glo4002.booking.resource.order.assembler;

import ca.ulaval.glo4002.booking.resource.order.response.OrderResponse;
import ca.ulaval.glo4002.booking.resource.pass.factory.PassResponseFactory;
import ca.ulaval.glo4002.booking.resource.pass.response.PassResponse;
import ca.ulaval.glo4002.domain.order.Order;

import java.util.List;
import java.util.stream.Collectors;

public class OrderResponseAssembler {

    private final PassResponseFactory passResponseFactory;

    public OrderResponseAssembler(PassResponseFactory passResponseFactory) {
        this.passResponseFactory = passResponseFactory;
    }

    public OrderResponse assemble(Order order) {
        List<PassResponse> passResponses = order.getEventPasses().stream()
                                                .map(passResponseFactory::create)
                                                .collect(Collectors.toList());
        return new OrderResponse(order.calculateTotalPrice().getValue(), passResponses);
    }
}
