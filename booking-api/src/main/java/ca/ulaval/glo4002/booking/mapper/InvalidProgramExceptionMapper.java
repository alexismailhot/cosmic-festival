package ca.ulaval.glo4002.booking.mapper;

import ca.ulaval.glo4002.booking.resource.ErrorResponse;
import ca.ulaval.glo4002.booking.resource.ErrorResponseAssembler;
import ca.ulaval.glo4002.domain.program.exception.InvalidProgramException;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

public class InvalidProgramExceptionMapper implements ExceptionMapper<InvalidProgramException> {

    private static final String ERROR_MESSAGE = "INVALID_PROGRAM";
    private static final String ERROR_DESCRIPTION = "the program is invalid";
    private final ErrorResponseAssembler errorResponseAssembler;

    public InvalidProgramExceptionMapper(ErrorResponseAssembler errorResponseAssembler) {
        this.errorResponseAssembler = errorResponseAssembler;
    }

    @Override
    public Response toResponse(InvalidProgramException exception) {
        ErrorResponse errorResponse = errorResponseAssembler.assemble(ERROR_MESSAGE, ERROR_DESCRIPTION);
        return Response.status(Status.BAD_REQUEST)
                .entity(errorResponse)
                .build();
    }
}
