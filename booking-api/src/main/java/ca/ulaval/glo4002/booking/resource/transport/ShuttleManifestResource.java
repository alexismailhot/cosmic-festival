package ca.ulaval.glo4002.booking.resource.transport;

import ca.ulaval.glo4002.booking.resource.transport.assembler.TransportResponseAssembler;
import ca.ulaval.glo4002.booking.resource.transport.response.TransportResponse;
import ca.ulaval.glo4002.domain.transport.Shuttle;
import ca.ulaval.glo4002.service.transport.ShuttleService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Provider
@Path("/shuttle-manifests")
@Produces(MediaType.APPLICATION_JSON)
public class ShuttleManifestResource {

    private final ShuttleService shuttleService;
    private final TransportResponseAssembler transportResponseAssembler;

    public ShuttleManifestResource(ShuttleService shuttleService, TransportResponseAssembler transportResponseAssembler) {
        this.shuttleService = shuttleService;
        this.transportResponseAssembler = transportResponseAssembler;
    }

    @GET
    public TransportResponse getShuttleManifests(@QueryParam("date") String date) {
        List<Shuttle> shuttles = Optional.ofNullable(date)
                .map(LocalDate::parse)
                .map(shuttleService::getShuttles)
                .orElse(shuttleService.getAllShuttles());
        return transportResponseAssembler.assemble(shuttles);
    }
}
