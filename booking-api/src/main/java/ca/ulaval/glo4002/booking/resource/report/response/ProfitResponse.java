package ca.ulaval.glo4002.booking.resource.report.response;

import java.math.BigDecimal;

public class ProfitResponse {

    private final BigDecimal in;
    private final BigDecimal out;
    private final BigDecimal profit;

    public ProfitResponse(BigDecimal in, BigDecimal out, BigDecimal profit) {
        this.in = in;
        this.out = out;
        this.profit = profit;
    }

    public BigDecimal getIn() {
        return in;
    }

    public BigDecimal getOut() {
        return out;
    }

    public BigDecimal getProfit() {
        return profit;
    }
}
