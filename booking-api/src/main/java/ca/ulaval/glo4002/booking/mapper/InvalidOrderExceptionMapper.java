package ca.ulaval.glo4002.booking.mapper;

import ca.ulaval.glo4002.booking.resource.ErrorResponseAssembler;
import ca.ulaval.glo4002.domain.order.exception.InvalidOrderException;

import javax.ws.rs.core.Response.Status;

public class InvalidOrderExceptionMapper extends OrderExceptionMapper<InvalidOrderException> {

    public InvalidOrderExceptionMapper(ErrorResponseAssembler errorResponseAssembler) {
        super(Status.BAD_REQUEST, errorResponseAssembler);
    }
}
