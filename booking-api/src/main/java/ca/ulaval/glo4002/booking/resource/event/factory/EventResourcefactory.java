package ca.ulaval.glo4002.booking.resource.event.factory;

import ca.ulaval.glo4002.booking.resource.event.EventResource;
import ca.ulaval.glo4002.domain.event.FestivalDates;
import ca.ulaval.glo4002.domain.order.OrderingPeriod;
import ca.ulaval.glo4002.service.event.EventService;

public class EventResourcefactory {

    public EventResource create(FestivalDates festivalDates, OrderingPeriod orderingPeriod) {
        EventService eventService = new EventService(festivalDates, orderingPeriod);
        return new EventResource(eventService);
    }
}
