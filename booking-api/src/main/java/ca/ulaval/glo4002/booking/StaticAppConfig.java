package ca.ulaval.glo4002.booking;

import ca.ulaval.glo4002.service.AppConfig;

public class StaticAppConfig implements AppConfig {

    private static final String EXTERNAL_SERVICE_API_URL = "http://localhost:8080/artists";

    @Override
    public String getExternalServiceApiUrl() {
        return EXTERNAL_SERVICE_API_URL;
    }
}
