package ca.ulaval.glo4002.booking;

import ca.ulaval.glo4002.booking.resource.event.factory.*;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

import ca.ulaval.glo4002.booking.mapper.CatchAllExceptionMapper;
import ca.ulaval.glo4002.booking.mapper.InvalidFormatExceptionMapper;
import ca.ulaval.glo4002.booking.mapper.InvalidOrderExceptionMapper;
import ca.ulaval.glo4002.booking.mapper.InvalidProgramExceptionMapper;
import ca.ulaval.glo4002.booking.mapper.JsonProcessingExceptionMapper;
import ca.ulaval.glo4002.booking.mapper.NotAllowedExceptionMapper;
import ca.ulaval.glo4002.booking.mapper.NotFoundExceptionMapper;
import ca.ulaval.glo4002.booking.mapper.OrderNotFoundExceptionMapper;
import ca.ulaval.glo4002.booking.mapper.PathParamExceptionMapper;
import ca.ulaval.glo4002.booking.resource.ErrorResponseAssembler;
import ca.ulaval.glo4002.booking.resource.order.factory.OrderResourceFactory;
import ca.ulaval.glo4002.booking.resource.order.factory.OrderServiceFactory;
import ca.ulaval.glo4002.booking.resource.pass.factory.PassPricesFactory;
import ca.ulaval.glo4002.booking.resource.pass.factory.PassServiceFactory;
import ca.ulaval.glo4002.booking.resource.program.factory.ProgramResourceFactory;
import ca.ulaval.glo4002.booking.resource.report.ReportResource;
import ca.ulaval.glo4002.booking.resource.report.assembler.OxygenResponseAssembler;
import ca.ulaval.glo4002.booking.resource.report.assembler.ProfitResponseAssembler;
import ca.ulaval.glo4002.booking.resource.report.factory.OxygenServiceFactory;
import ca.ulaval.glo4002.booking.resource.report.factory.ProfitServiceFactory;
import ca.ulaval.glo4002.booking.resource.transport.factory.ShuttleManifestResourceFactory;
import ca.ulaval.glo4002.booking.resource.transport.factory.TransportBookerFactory;
import ca.ulaval.glo4002.domain.artist.ArtistOxygenNeeds;
import ca.ulaval.glo4002.domain.artist.ArtistShuttleType;
import ca.ulaval.glo4002.domain.event.FestivalDates;
import ca.ulaval.glo4002.domain.order.OrderingPeriod;
import ca.ulaval.glo4002.domain.pass.PassPrices;
import ca.ulaval.glo4002.domain.pass.repository.EventPassRepository;
import ca.ulaval.glo4002.service.profit.IncomeStatementFactory;
import ca.ulaval.glo4002.domain.profit.factory.TransactionFactory;
import ca.ulaval.glo4002.domain.profit.repository.TransactionRepository;
import ca.ulaval.glo4002.domain.transport.repository.ShuttleRepository;
import ca.ulaval.glo4002.infra.pass.repository.EventPassRepositoryInMemory;
import ca.ulaval.glo4002.infra.profit.repository.TransactionRepositoryInMemory;
import ca.ulaval.glo4002.infra.transport.repository.ShuttleRepositoryInMemory;
import ca.ulaval.glo4002.service.AppConfig;
import ca.ulaval.glo4002.service.order.OrderService;
import ca.ulaval.glo4002.service.order.OrderValidator;
import ca.ulaval.glo4002.service.oxygen.OxygenService;
import ca.ulaval.glo4002.service.profit.ProfitService;
import ca.ulaval.glo4002.service.transport.ShuttleService;
import ca.ulaval.glo4002.domain.transport.TransportBooker;

public class BookingServer implements Runnable {

    private static final int PORT = 8181;

    public static void main(String[] args) {
        new BookingServer().run();
    }

    public void run() {
        ResourceConfig packageConfig = new ResourceConfig();
        packageConfig.register(com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider.class);
        ErrorResponseAssembler errorResponseAssembler = new ErrorResponseAssembler();
        packageConfig.register(new CatchAllExceptionMapper());
        packageConfig.register(new InvalidOrderExceptionMapper(errorResponseAssembler));
        packageConfig.register(new OrderNotFoundExceptionMapper(errorResponseAssembler));
        packageConfig.register(new InvalidFormatExceptionMapper(errorResponseAssembler));
        packageConfig.register(new PathParamExceptionMapper(errorResponseAssembler));
        packageConfig.register(new InvalidProgramExceptionMapper(errorResponseAssembler));
        packageConfig.register(new JsonProcessingExceptionMapper(errorResponseAssembler));
        packageConfig.register(new NotAllowedExceptionMapper());
        packageConfig.register(new NotFoundExceptionMapper());
        packageConfig.register(new ObjectMapperContextResolver());

        AppConfig appConfig = new StaticAppConfig();
        OrderingPeriodFactory orderingPeriodFactory = new OrderingPeriodFactory();
        ArtistShuttleTypeFactory artistShuttleTypeFactory = new ArtistShuttleTypeFactory();
        ArtistOxygenNeedsFactory artistOxygenNeedsFactory = new ArtistOxygenNeedsFactory();
        FestivalDatesFactory festivalDatesFactory = new FestivalDatesFactory();

        OrderingPeriod orderingPeriod = orderingPeriodFactory.create();
        ArtistShuttleType artistShuttleType = artistShuttleTypeFactory.create();
        ArtistOxygenNeeds artistOxygenNeeds = artistOxygenNeedsFactory.create();
        FestivalDates festivalDates = festivalDatesFactory.create();

        OrderValidator orderValidator = new OrderValidator(orderingPeriod, festivalDates);

        TransactionRepository transactionRepository = new TransactionRepositoryInMemory();
        IncomeStatementFactory incomeStatementFactory = new IncomeStatementFactory();

        ProfitServiceFactory profitServiceFactory = new ProfitServiceFactory();
        ProfitService profitService = profitServiceFactory.create(transactionRepository, incomeStatementFactory);

        TransactionFactory transactionFactory = new TransactionFactory();

        TransportBookerFactory transportBookerFactory = new TransportBookerFactory(festivalDates);
        ShuttleRepository shuttleRepository = new ShuttleRepositoryInMemory();
        TransportBooker transportBooker = transportBookerFactory.create(transactionRepository, transactionFactory, shuttleRepository);
        OxygenServiceFactory oxygenServiceFactory = new OxygenServiceFactory();
        OxygenService oxygenService = oxygenServiceFactory.create(festivalDates, transactionFactory, transactionRepository);

        OrderServiceFactory orderServiceFactory = new OrderServiceFactory();

        OrderService orderService = orderServiceFactory.create(transactionRepository, transactionFactory);

        PassServiceFactory passServiceFactory = new PassServiceFactory(orderService, transportBooker, oxygenService, orderValidator);
        OrderResourceFactory orderResourceFactory = new OrderResourceFactory(passServiceFactory, orderService);
        ProgramResourceFactory programResourceFactory = new ProgramResourceFactory(appConfig);
        EventResourcefactory eventResourcefactory = new EventResourcefactory();
        EventPassRepository eventPassRepository = new EventPassRepositoryInMemory();

        PassPricesFactory passPricesFactory = new PassPricesFactory();
        PassPrices passPrices = passPricesFactory.create();

        packageConfig.register(programResourceFactory.create(transportBooker, eventPassRepository, oxygenService, festivalDates,
                transactionRepository, artistOxygenNeeds, artistShuttleType, transactionFactory));
        packageConfig.register(eventResourcefactory.create(festivalDates, orderingPeriod));
        packageConfig.register(orderResourceFactory.create(eventPassRepository, passPrices));
        packageConfig.register(new ReportResource(oxygenService,
                                                  profitService, 
                                                  new OxygenResponseAssembler(),
                                                  new ProfitResponseAssembler()));

        ShuttleService shuttleService = new ShuttleService(shuttleRepository, festivalDates);
        ShuttleManifestResourceFactory shuttleManifestResourceFactory = new ShuttleManifestResourceFactory(shuttleService);
        packageConfig.register(shuttleManifestResourceFactory.create());

        ServletContainer container = new ServletContainer(packageConfig);
        ServletHolder servletHolder = new ServletHolder(container);
        Server server = new Server(PORT);
        ServletContextHandler contextHandler = new ServletContextHandler(server, "/");
        contextHandler.addServlet(servletHolder, "/*");

        try {
            server.start();
            server.join();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            server.destroy();
        }
    }
}
