package ca.ulaval.glo4002.booking.resource.transport.assembler;

import ca.ulaval.glo4002.booking.resource.transport.response.ShuttleResponse;
import ca.ulaval.glo4002.booking.resource.transport.response.TransportResponse;
import ca.ulaval.glo4002.domain.transport.Shuttle;
import ca.ulaval.glo4002.domain.transport.ShuttleDirection;

import java.util.List;
import java.util.stream.Collectors;

public class TransportResponseAssembler {

    private final ShuttleResponseAssembler shuttleResponseAssembler;

    public TransportResponseAssembler(ShuttleResponseAssembler shuttleResponseAssembler) {
        this.shuttleResponseAssembler = shuttleResponseAssembler;
    }

    public TransportResponse assemble(List<Shuttle> shuttles) {
        List<ShuttleResponse> departureShuttlesResponse = shuttles.stream()
                .filter(shuttle -> shuttle.getShuttleDirection().equals(ShuttleDirection.DEPARTURE))
                .map(shuttleResponseAssembler::assemble)
                .collect(Collectors.toList());

        List<ShuttleResponse> arrivalShuttlesResponse = shuttles.stream()
                .filter(shuttle -> shuttle.getShuttleDirection().equals(ShuttleDirection.ARRIVAL))
                .map(shuttleResponseAssembler::assemble)
                .collect(Collectors.toList());

        return new TransportResponse(departureShuttlesResponse, arrivalShuttlesResponse);
    }
}
