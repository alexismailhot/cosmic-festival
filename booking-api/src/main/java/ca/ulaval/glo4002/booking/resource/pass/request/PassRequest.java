package ca.ulaval.glo4002.booking.resource.pass.request;

import ca.ulaval.glo4002.domain.pass.PassCategory;
import ca.ulaval.glo4002.domain.pass.PassOption;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;
import java.util.List;

public class PassRequest {

    private final PassCategory passCategory;
    private final PassOption passOption;
    private final List<LocalDate> eventDates;

    @JsonCreator
    public PassRequest(@JsonProperty(value = "passCategory", required = true) PassCategory passCategory,
                       @JsonProperty(value = "passOption", required = true) PassOption passOption,
                       @JsonProperty(value = "eventDates") List<LocalDate> eventDates) {
        this.passCategory = passCategory;
        this.passOption = passOption;
        this.eventDates = eventDates;
    }

    public PassCategory getPassCategory() {
        return passCategory;
    }

    public PassOption getPassOption() {
        return passOption;
    }

    public List<LocalDate> getEventDates() {
        return eventDates;
    }
}
