package ca.ulaval.glo4002.booking.resource.program.assembler;

import ca.ulaval.glo4002.booking.resource.program.request.DailyProgramRequest;
import ca.ulaval.glo4002.booking.resource.program.request.ProgramRequest;
import ca.ulaval.glo4002.domain.program.EventActivity;
import ca.ulaval.glo4002.service.program.DailyProgramSubmission;
import ca.ulaval.glo4002.service.program.ProgramSubmission;

import java.util.stream.Collectors;

public class ProgramSubmissionAssembler {

    public ProgramSubmission assemble(ProgramRequest programRequest) {
        return new ProgramSubmission(programRequest.getProgram()
                .stream()
                .map(this::assembleDailyProgramSubmission)
                .collect(Collectors.toList()));
    }

    private DailyProgramSubmission assembleDailyProgramSubmission(DailyProgramRequest dailyProgramRequest) {
        return new DailyProgramSubmission(dailyProgramRequest.getEventDate(), EventActivity.fromString(dailyProgramRequest.getAM()),
                dailyProgramRequest.getPM());
    }
}
