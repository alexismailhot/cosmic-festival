package ca.ulaval.glo4002.booking.resource.transport.assembler;

import ca.ulaval.glo4002.booking.resource.transport.response.ShuttleResponse;
import ca.ulaval.glo4002.domain.transport.PassengerNumber;
import ca.ulaval.glo4002.domain.transport.Shuttle;

import java.util.List;
import java.util.stream.Collectors;

public class ShuttleResponseAssembler {

    public ShuttleResponse assemble(Shuttle shuttle) {
        return new ShuttleResponse(shuttle.getEventDate().toString(), shuttle.getShuttleType().getName(),
                assembleResponsePassengers(shuttle.getPassengers()));
    }

    private List<Long> assembleResponsePassengers(List<PassengerNumber> passengers) {
        return passengers.stream()
                .map(PassengerNumber::getValue)
                .collect(Collectors.toList());
    }
}
