package ca.ulaval.glo4002.booking.resource.event;

import ca.ulaval.glo4002.booking.resource.event.request.EventRequest;
import ca.ulaval.glo4002.service.event.EventService;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDate;

@Path("/configuration")
@Produces(MediaType.APPLICATION_JSON)
public class EventResource {

    private final EventService eventService;

    public EventResource(EventService eventService) {
        this.eventService = eventService;
    }

    @POST
    public Response changeFestivalDates(EventRequest eventRequest) {
        updateFestivalDates(eventRequest);
        return Response.ok().build();
    }

    private void updateFestivalDates(EventRequest eventRequest) {
        LocalDate beginDate = eventRequest.getBeginDate();
        LocalDate endDate = eventRequest.getEndDate();

        eventService.updateFestivalStartDate(beginDate);
        eventService.updateFestivalEndDate(endDate);
        eventService.updateOrderingPeriodDates(beginDate);
    }
}
