package ca.ulaval.glo4002.booking.resource.order.request;

import ca.ulaval.glo4002.booking.resource.pass.request.PassRequest;
import ca.ulaval.glo4002.domain.order.VendorCode;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;

public class OrderRequest {

    private final LocalDate orderDate;
    private final VendorCode vendorCode;
    private final PassRequest passes;

    @JsonCreator
    public OrderRequest(@JsonProperty(value = "orderDate", required = true) LocalDate orderDate,
                        @JsonProperty(value = "vendorCode", required = true) VendorCode vendorCode,
                        @JsonProperty(value = "passes", required = true) PassRequest passes) {
        this.orderDate = orderDate;
        this.vendorCode = vendorCode;
        this.passes = passes;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public VendorCode getVendorCode() {
        return vendorCode;
    }

    public PassRequest getPasses() {
        return passes;
    }
}
