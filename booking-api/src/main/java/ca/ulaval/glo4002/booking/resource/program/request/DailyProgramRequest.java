package ca.ulaval.glo4002.booking.resource.program.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;

public class DailyProgramRequest {

    private final LocalDate eventDate;
    private final String am;
    private final String pm;

    public DailyProgramRequest(@JsonProperty(value = "eventDate", required = true) LocalDate eventDate,
                               @JsonProperty(value = "am", required = true) String am,
                               @JsonProperty(value = "pm", required = true) String pm) {
        this.eventDate = eventDate;
        this.am = am;
        this.pm = pm;
    }

    public String getPM() {
        return pm;
    }

    public String getAM() {
        return am;
    }

    public LocalDate getEventDate() {
        return eventDate;
    }
}
