package ca.ulaval.glo4002.booking.resource.pass.factory;

import ca.ulaval.glo4002.domain.pass.PassCategory;
import ca.ulaval.glo4002.domain.pass.PassPrices;
import ca.ulaval.glo4002.domain.pass.configuration.PassPricesConfiguration;
import ca.ulaval.glo4002.domain.price.Price;
import ca.ulaval.glo4002.infra.pass.GLOW4002PassPricesConfiguration;

import java.util.EnumMap;

public class PassPricesFactory {

    public PassPrices create() {
        EnumMap<PassCategory, Price> eventPassPrices = createEventPassPrices();
        EnumMap<PassCategory, Price> dailyEventPassPrices = createDailyEventPassPrices();

        PassPricesConfiguration passPricesConfiguration = new GLOW4002PassPricesConfiguration(eventPassPrices, dailyEventPassPrices);

        return new PassPrices(passPricesConfiguration);
    }

    private EnumMap<PassCategory, Price> createEventPassPrices() {
        EnumMap<PassCategory, Price> eventPassPrices = new EnumMap<>(PassCategory.class);

        eventPassPrices.put(PassCategory.NEBULA, Price.of(250000));
        eventPassPrices.put(PassCategory.SUPERGIANT, Price.of(500000));
        eventPassPrices.put(PassCategory.SUPERNOVA, Price.of(700000));

        return eventPassPrices;
    }

    private EnumMap<PassCategory, Price> createDailyEventPassPrices() {
        EnumMap<PassCategory, Price> dailyEventPassPrices = new EnumMap<>(PassCategory.class);

        dailyEventPassPrices.put(PassCategory.NEBULA, Price.of(50000));
        dailyEventPassPrices.put(PassCategory.SUPERGIANT, Price.of(100000));
        dailyEventPassPrices.put(PassCategory.SUPERNOVA, Price.of(150000));

        return dailyEventPassPrices;
    }
}
