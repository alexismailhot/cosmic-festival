package ca.ulaval.glo4002.booking.resource.order.assembler;

import ca.ulaval.glo4002.domain.order.OrderNumber;

public class OrderNumberAssembler {

    public OrderNumber create(String orderNumber) {
        return new OrderNumber(orderNumber);
    }
}
