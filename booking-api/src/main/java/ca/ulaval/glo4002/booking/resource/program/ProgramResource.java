package ca.ulaval.glo4002.booking.resource.program;

import ca.ulaval.glo4002.booking.resource.program.assembler.ArtistResponseAssembler;
import ca.ulaval.glo4002.booking.resource.program.assembler.ProgramSubmissionAssembler;
import ca.ulaval.glo4002.booking.resource.program.request.ProgramRequest;
import ca.ulaval.glo4002.booking.resource.program.response.ArtistResponse;
import ca.ulaval.glo4002.domain.artist.Artist;
import ca.ulaval.glo4002.domain.artist.ArtistSortingType;
import ca.ulaval.glo4002.service.artist.ArtistService;
import ca.ulaval.glo4002.service.program.ProgramUnveilingService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path("/program")
@Produces(MediaType.APPLICATION_JSON)
public class ProgramResource {

    private final ArtistService artistService;
    private final ArtistResponseAssembler artistResponseAssembler;
    private final ProgramUnveilingService programUnveilingService;
    private final ProgramSubmissionAssembler programSubmissionAssembler;

    public ProgramResource(ArtistService artistService,
                           ArtistResponseAssembler artistResponseAssembler,
                           ProgramUnveilingService programUnveilingService,
                           ProgramSubmissionAssembler programSubmissionAssembler) {
        this.artistService = artistService;
        this.artistResponseAssembler = artistResponseAssembler;
        this.programUnveilingService = programUnveilingService;
        this.programSubmissionAssembler = programSubmissionAssembler;
    }

    @POST
    public Response createFestivalProgram(ProgramRequest programRequest) {
        programUnveilingService.setFestivalProgram(programSubmissionAssembler.assemble(programRequest));
        return Response.ok().build();
    }

    @Path("/artists")
    @GET
    public ArtistResponse getArtistsOrderedBy(@QueryParam("orderBy") ArtistSortingType orderBy) {
        List<Artist> artists;
        switch (orderBy) {
            case LOW_COSTS:
                artists = artistService.getArtistsByLowCosts();
                break;
            case MOST_POPULAR:
                artists = artistService.getArtistsByPopularity();
                break;
            default:
                throw new BadRequestException();
        }
        return artistResponseAssembler.assemble(artists
                .stream()
                .map(Artist::getName)
                .collect(Collectors.toList()));
    }
}
