package ca.ulaval.glo4002.booking.resource.order.factory;

import ca.ulaval.glo4002.booking.resource.order.OrderResource;
import ca.ulaval.glo4002.booking.resource.order.assembler.OrderNumberAssembler;
import ca.ulaval.glo4002.booking.resource.order.assembler.OrderResponseAssembler;
import ca.ulaval.glo4002.booking.resource.pass.assembler.PassResponseAssembler;
import ca.ulaval.glo4002.booking.resource.pass.assembler.SinglePassResponseAssembler;
import ca.ulaval.glo4002.booking.resource.pass.factory.PassResponseFactory;
import ca.ulaval.glo4002.booking.resource.pass.factory.PassServiceFactory;
import ca.ulaval.glo4002.domain.pass.DailyEventPass;
import ca.ulaval.glo4002.domain.pass.EventPass;
import ca.ulaval.glo4002.domain.pass.PassPrices;
import ca.ulaval.glo4002.domain.pass.repository.EventPassRepository;
import ca.ulaval.glo4002.service.order.OrderService;
import ca.ulaval.glo4002.service.pass.PassService;

import java.util.HashMap;
import java.util.Map;

public class OrderResourceFactory {

    private final PassServiceFactory passServiceFactory;
    private final OrderService orderService;

    public OrderResourceFactory(PassServiceFactory passServiceFactory, OrderService orderService) {
        this.passServiceFactory = passServiceFactory;
        this.orderService = orderService;
    }

    public OrderResource create(EventPassRepository eventPassRepository, PassPrices passPrices) {
        Map<Class<? extends EventPass>, PassResponseAssembler> assemblers = new HashMap<>();
        assemblers.put(DailyEventPass.class, new SinglePassResponseAssembler());
        assemblers.put(EventPass.class, new PassResponseAssembler());
        PassResponseFactory passResponseFactory = new PassResponseFactory(assemblers);
        OrderResponseAssembler orderResponseAssembler = new OrderResponseAssembler(passResponseFactory);
        OrderNumberAssembler orderNumberAssembler = new OrderNumberAssembler();

        PassService passService = passServiceFactory.create(eventPassRepository, passPrices);

        return new OrderResource(orderService, passService, orderResponseAssembler, orderNumberAssembler);
    }
}
