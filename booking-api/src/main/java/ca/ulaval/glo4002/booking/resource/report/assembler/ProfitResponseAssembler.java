package ca.ulaval.glo4002.booking.resource.report.assembler;

import ca.ulaval.glo4002.booking.resource.report.response.ProfitResponse;
import ca.ulaval.glo4002.domain.profit.IncomeStatement;

import java.math.BigDecimal;

public class ProfitResponseAssembler {
    public ProfitResponse assemble(IncomeStatement incomeStatement) {
        BigDecimal income = roundHalfUpAndSetTwoDecimals(incomeStatement.getRevenues().getValue());
        BigDecimal expense = roundHalfUpAndSetTwoDecimals(incomeStatement.getExpenses().getValue());
        BigDecimal profit = roundHalfUpAndSetTwoDecimals(incomeStatement.getProfits().getValue());
        return new ProfitResponse(income, expense, profit);
    }

    private BigDecimal roundHalfUpAndSetTwoDecimals(BigDecimal amount) {
        return amount.setScale(2, BigDecimal.ROUND_HALF_UP);
    }
}
