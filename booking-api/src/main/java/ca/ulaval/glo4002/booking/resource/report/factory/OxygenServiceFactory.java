package ca.ulaval.glo4002.booking.resource.report.factory;

import ca.ulaval.glo4002.domain.event.FestivalDates;
import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;
import ca.ulaval.glo4002.domain.oxygen.OxygenInventory;
import ca.ulaval.glo4002.domain.oxygen.factory.OxygenHistoryFactory;
import ca.ulaval.glo4002.domain.oxygen.factory.OxygenTankFactory;
import ca.ulaval.glo4002.domain.oxygen.method.CandlesOxygenSupplyMethod;
import ca.ulaval.glo4002.domain.oxygen.method.ElectrolysisOxygenSupplyMethod;
import ca.ulaval.glo4002.domain.oxygen.method.OxygenSupplyMethod;
import ca.ulaval.glo4002.domain.oxygen.method.PremadeOxygenSupplyMethod;
import ca.ulaval.glo4002.domain.oxygen.repository.OxygenHistoryRepository;
import ca.ulaval.glo4002.domain.oxygen.repository.OxygenTankRepository;
import ca.ulaval.glo4002.domain.price.Price;
import ca.ulaval.glo4002.domain.profit.factory.TransactionFactory;
import ca.ulaval.glo4002.domain.profit.repository.TransactionRepository;
import ca.ulaval.glo4002.infra.oxygen.repository.OxygenHistoryRepositoryInMemory;
import ca.ulaval.glo4002.infra.oxygen.repository.OxygenTankRepositoryInMemory;
import ca.ulaval.glo4002.service.oxygen.OxygenService;
import ca.ulaval.glo4002.service.oxygen.OxygenSupplyMethodFactory;

import java.util.Arrays;
import java.util.List;

public class OxygenServiceFactory {

    public OxygenServiceFactory() {
    }

    public OxygenService create(FestivalDates festivalDates, TransactionFactory transactionFactory, TransactionRepository transactionRepository) {
        OxygenTankRepository oxygenTankRepository = new OxygenTankRepositoryInMemory();
        OxygenInventory oxygenInventory = new OxygenInventory(oxygenTankRepository);
        OxygenHistoryRepository oxygenHistoryRepository = new OxygenHistoryRepositoryInMemory();
        OxygenHistoryFactory oxygenHistoryFactory = new OxygenHistoryFactory();
        OxygenTankFactory oxygenTankFactory = new OxygenTankFactory();

        OxygenSupplyMethod premadeMethod = new PremadeOxygenSupplyMethod(oxygenTankFactory, OxygenGrade.GRADE_E, 0,
                0, 1, Price.of(5000));

        OxygenSupplyMethod electrolysisMethod = new ElectrolysisOxygenSupplyMethod(oxygenTankFactory, OxygenGrade.GRADE_B,
                3, 10, 8, Price.of(4800));
        OxygenSupplyMethod candlesMethod = new CandlesOxygenSupplyMethod(oxygenTankFactory, OxygenGrade.GRADE_A,
                5, 20, 15, Price.of(9750));

        List<OxygenSupplyMethod> oxygenSupplyMethods = Arrays.asList(premadeMethod, electrolysisMethod, candlesMethod);

        OxygenSupplyMethodFactory oxygenSupplyMethodFactory = new OxygenSupplyMethodFactory(festivalDates,
                oxygenSupplyMethods);

        return new OxygenService(festivalDates, oxygenInventory, oxygenSupplyMethodFactory, oxygenHistoryRepository, oxygenHistoryFactory,
                transactionRepository, transactionFactory);
    }
}
