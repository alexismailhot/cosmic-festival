package ca.ulaval.glo4002.booking.resource.order;

import java.net.URI;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ca.ulaval.glo4002.booking.resource.order.assembler.OrderNumberAssembler;
import ca.ulaval.glo4002.booking.resource.order.assembler.OrderResponseAssembler;
import ca.ulaval.glo4002.booking.resource.order.request.OrderRequest;
import ca.ulaval.glo4002.booking.resource.order.response.OrderResponse;
import ca.ulaval.glo4002.domain.order.Order;
import ca.ulaval.glo4002.domain.order.OrderNumber;
import ca.ulaval.glo4002.domain.order.VendorCode;
import ca.ulaval.glo4002.domain.order.exception.InvalidOrderException;
import ca.ulaval.glo4002.domain.pass.PassCategory;
import ca.ulaval.glo4002.domain.pass.PassOption;
import ca.ulaval.glo4002.service.order.OrderService;
import ca.ulaval.glo4002.service.pass.PassService;

@Path("/orders")
@Produces(MediaType.APPLICATION_JSON)
public class OrderResource {

    private final OrderService orderService;
    private final OrderResponseAssembler orderResponseAssembler;
    private final OrderNumberAssembler orderNumberFactory;
    private final PassService passService;

    public OrderResource(OrderService orderService,
                         PassService passService,
                         OrderResponseAssembler orderResponseAssembler,
                         OrderNumberAssembler orderNumberFactory) {
        this.orderService = orderService;
        this.passService = passService;
        this.orderResponseAssembler = orderResponseAssembler;
        this.orderNumberFactory = orderNumberFactory;
    }

    @POST
    public Response createOrder(OrderRequest orderRequest) {
        OrderNumber orderNumber = buyPasses(orderRequest);
        String location = String.format("/orders/%s", orderNumber.toString());
        return Response.created(URI.create(location)).build();
    }

    private OrderNumber buyPasses(OrderRequest orderRequest) {
        VendorCode vendorCode = orderRequest.getVendorCode();
        PassCategory passCategory = orderRequest.getPasses().getPassCategory();
        PassOption passOption = orderRequest.getPasses().getPassOption();
        LocalDate orderDate = orderRequest.getOrderDate();

        switch (passOption) {
            case PACKAGE:
                if (eventDatesFieldIsNotNull(orderRequest)) {
                    throw new InvalidOrderException();
                }
                return passService.buyEventPass(orderDate, vendorCode, passCategory);

            case SINGLEPASS:
                List<LocalDate> dates = new ArrayList<>(orderRequest.getPasses().getEventDates());
                return passService.buyDailyEventPasses(orderDate, vendorCode, passCategory, dates);

            default:
                throw new BadRequestException(String.format("no case defined for enum value %s", passOption.getName()));
        }
    }

    private boolean eventDatesFieldIsNotNull(OrderRequest orderRequest) {
        return orderRequest.getPasses().getEventDates() != null;
    }

    @GET
    @Path("/{orderNumber}")
    public OrderResponse getOrder(@PathParam("orderNumber") String orderNumber) {
        Order order = orderService.getOrder(orderNumberFactory.create(orderNumber));
        return orderResponseAssembler.assemble(order);
    }
}
