package ca.ulaval.glo4002.booking.resource.report.response;

public class OxygenInventoryResponse {

    private final String gradeTankOxygen;
    private final int quantity;

    public OxygenInventoryResponse(String gradeTankOxygen, int quantity) {
        this.gradeTankOxygen = gradeTankOxygen;
        this.quantity = quantity;
    }

    public String getGradeTankOxygen() {
        return gradeTankOxygen;
    }

    public int getQuantity() {
        return quantity;
    }
}
