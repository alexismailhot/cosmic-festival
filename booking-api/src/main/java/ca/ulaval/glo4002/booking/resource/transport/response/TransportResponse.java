package ca.ulaval.glo4002.booking.resource.transport.response;

import java.util.List;

public class TransportResponse {

    private final List<ShuttleResponse> departures;
    private final List<ShuttleResponse> arrivals;

    public TransportResponse(List<ShuttleResponse> departures, List<ShuttleResponse> arrivals) {
        this.departures = departures;
        this.arrivals = arrivals;
    }

    public List<ShuttleResponse> getDepartures() {
        return departures;
    }

    public List<ShuttleResponse> getArrivals() {
        return arrivals;
    }
}
