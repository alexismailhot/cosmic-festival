package ca.ulaval.glo4002.booking.resource.order.response;

import ca.ulaval.glo4002.booking.resource.pass.response.PassResponse;

import java.math.BigDecimal;
import java.util.List;

public class OrderResponse {

    private final BigDecimal orderPrice;
    private final List<PassResponse> passes;

    public OrderResponse(BigDecimal orderPrice, List<PassResponse> passes) {
        this.orderPrice = orderPrice;
        this.passes = passes;
    }

    public BigDecimal getOrderPrice() {
        return orderPrice;
    }

    public List<PassResponse> getPasses() {
        return passes;
    }
}
