package ca.ulaval.glo4002.booking.resource.transport.factory;

import ca.ulaval.glo4002.domain.event.FestivalDates;
import ca.ulaval.glo4002.domain.profit.factory.TransactionFactory;
import ca.ulaval.glo4002.domain.profit.repository.TransactionRepository;
import ca.ulaval.glo4002.domain.transport.factory.ShuttleFactory;
import ca.ulaval.glo4002.domain.transport.repository.ShuttleRepository;
import ca.ulaval.glo4002.domain.transport.TransportBooker;

public class TransportBookerFactory {

    private final FestivalDates festivalDates;

    public TransportBookerFactory(FestivalDates festivalDates) {
        this.festivalDates = festivalDates;
    }

    public TransportBooker create(TransactionRepository transactionRepository, TransactionFactory transactionFactory, ShuttleRepository shuttleRepository) {
        return new TransportBooker(festivalDates, shuttleRepository, new ShuttleFactory(), transactionRepository, transactionFactory);
    }
}
