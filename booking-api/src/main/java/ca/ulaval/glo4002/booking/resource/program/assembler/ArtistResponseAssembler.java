package ca.ulaval.glo4002.booking.resource.program.assembler;

import ca.ulaval.glo4002.booking.resource.program.response.ArtistResponse;

import java.util.List;

public class ArtistResponseAssembler {

    public ArtistResponse assemble(List<String> artistNames) {
        return new ArtistResponse(artistNames);
    }
}
