package ca.ulaval.glo4002.booking.resource.program.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ProgramRequest {

    private final List<DailyProgramRequest> program;

    @JsonCreator
    public ProgramRequest(@JsonProperty(value = "program", required = true) List<DailyProgramRequest> program) {
        this.program = program;
    }

    public List<DailyProgramRequest> getProgram() {
        return program;
    }
}
