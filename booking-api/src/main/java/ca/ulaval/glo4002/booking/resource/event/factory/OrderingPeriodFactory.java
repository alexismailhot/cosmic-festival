package ca.ulaval.glo4002.booking.resource.event.factory;

import ca.ulaval.glo4002.domain.order.OrderingPeriod;
import ca.ulaval.glo4002.domain.order.configuration.OrderingPeriodConfiguration;
import ca.ulaval.glo4002.infra.order.configuration.GLOW4002OrderingPeriodConfiguration;

import java.time.LocalDate;


public class OrderingPeriodFactory {

    private static final LocalDate ORDER_START_DATE = LocalDate.of(2050, 1, 18);
    private static final LocalDate ORDER_END_DATE = LocalDate.of(2050, 7, 16);

    public OrderingPeriod create() {
        OrderingPeriodConfiguration orderingPeriodConfiguration = new GLOW4002OrderingPeriodConfiguration(ORDER_START_DATE, ORDER_END_DATE);
        return new OrderingPeriod(orderingPeriodConfiguration);
    }
}
