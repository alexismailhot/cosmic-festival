package ca.ulaval.glo4002.booking.resource.pass.factory;

import ca.ulaval.glo4002.booking.resource.pass.assembler.PassResponseAssembler;
import ca.ulaval.glo4002.booking.resource.pass.response.PassResponse;
import ca.ulaval.glo4002.domain.pass.EventPass;

import java.util.Map;

public class PassResponseFactory {
    
    private final Map<Class<? extends EventPass>, PassResponseAssembler> assemblers;

    public PassResponseFactory(Map<Class<? extends EventPass>, PassResponseAssembler> assemblers) {
        this.assemblers = assemblers;
    }

    public PassResponse create(EventPass eventPass) {
        return assemblers.get(eventPass.getClass()).assemble(eventPass);
    }
}
