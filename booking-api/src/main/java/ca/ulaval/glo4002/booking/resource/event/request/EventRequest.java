package ca.ulaval.glo4002.booking.resource.event.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;

public class EventRequest {

    private final LocalDate beginDate;
    private final LocalDate endDate;

    public EventRequest(@JsonProperty(value = "beginDate", required = true)LocalDate beginDate,
                        @JsonProperty(value = "endDate", required = true)LocalDate endDate) {
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    public LocalDate getBeginDate() {
        return beginDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }
}
