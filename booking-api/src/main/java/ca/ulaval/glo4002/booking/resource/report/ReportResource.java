package ca.ulaval.glo4002.booking.resource.report;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ca.ulaval.glo4002.booking.resource.report.assembler.OxygenResponseAssembler;
import ca.ulaval.glo4002.booking.resource.report.assembler.ProfitResponseAssembler;
import ca.ulaval.glo4002.booking.resource.report.response.OxygenResponse;
import ca.ulaval.glo4002.booking.resource.report.response.ProfitResponse;
import ca.ulaval.glo4002.service.oxygen.OxygenService;
import ca.ulaval.glo4002.service.profit.ProfitService;

@Path("/report")
@Produces(MediaType.APPLICATION_JSON)
public class ReportResource {

    private final OxygenService oxygenService;
    private final OxygenResponseAssembler oxygenResponseAssembler;
    private final ProfitResponseAssembler profitResponseAssembler;
    private final ProfitService profitService;

    public ReportResource(OxygenService oxygenService,
                          ProfitService profitService,
                          OxygenResponseAssembler oxygenResponseAssembler,
                          ProfitResponseAssembler profitResponseAssembler) {
        this.oxygenService = oxygenService;
        this.oxygenResponseAssembler = oxygenResponseAssembler;
        this.profitResponseAssembler = profitResponseAssembler;
        this.profitService = profitService;
    }

    @Path("/o2")
    @GET
    public OxygenResponse getOxygenReport() {
        return oxygenResponseAssembler.assemble(oxygenService.getInventory(), oxygenService.getAllOxygenHistories());
    }

    @Path("/profits")
    @GET
    public ProfitResponse getProfitReport() {
        return profitResponseAssembler.assemble(profitService.getIncomeStatement());
    }
}
