package ca.ulaval.glo4002.booking.resource.pass.assembler;

import ca.ulaval.glo4002.booking.resource.pass.response.PassResponse;
import ca.ulaval.glo4002.domain.pass.EventPass;
import ca.ulaval.glo4002.domain.pass.PassOption;

public class PassResponseAssembler {

    public PassResponse assemble(EventPass eventPass) {
        return new PassResponse(eventPass.getPassNumber().getValue(), eventPass.getPassCategory().getName(), PassOption.PACKAGE.getName());
    }
}
