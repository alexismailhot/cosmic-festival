package ca.ulaval.glo4002.booking.resource.report.response;

import java.util.List;

public class OxygenResponse {

    private final List<OxygenInventoryResponse> inventory;
    private final List<OxygenHistoryResponse> history;

    public OxygenResponse(List<OxygenInventoryResponse> inventory, List<OxygenHistoryResponse> history) {
        this.inventory = inventory;
        this.history = history;
    }

    public List<OxygenInventoryResponse> getInventory() {
        return inventory;
    }

    public List<OxygenHistoryResponse> getHistory() {
        return history;
    }
}
