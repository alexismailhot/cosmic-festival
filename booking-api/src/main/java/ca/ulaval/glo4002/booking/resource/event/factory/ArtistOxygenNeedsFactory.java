package ca.ulaval.glo4002.booking.resource.event.factory;

import ca.ulaval.glo4002.domain.artist.ArtistOxygenNeeds;
import ca.ulaval.glo4002.domain.artist.configuration.ArtistOxygenNeedsConfiguration;
import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;
import ca.ulaval.glo4002.infra.artist.GLOW4002ArtistOxygenNeedsConfiguration;

public class ArtistOxygenNeedsFactory {

    private static final OxygenGrade MINIMUM_OXYGEN_GRADE_NEEDED = OxygenGrade.GRADE_E;
    private static final int NUMBER_OF_OXYGEN_TANKS_NEEDED = 6;

    public ArtistOxygenNeeds create() {
        ArtistOxygenNeedsConfiguration artistOxygenNeedsConfiguration = new GLOW4002ArtistOxygenNeedsConfiguration(MINIMUM_OXYGEN_GRADE_NEEDED, NUMBER_OF_OXYGEN_TANKS_NEEDED);
        return new ArtistOxygenNeeds(artistOxygenNeedsConfiguration);
    }
}
