package ca.ulaval.glo4002.booking.resource.pass.assembler;

import ca.ulaval.glo4002.booking.resource.pass.response.PassResponse;
import ca.ulaval.glo4002.booking.resource.pass.response.SinglePassResponse;
import ca.ulaval.glo4002.domain.pass.DailyEventPass;
import ca.ulaval.glo4002.domain.pass.EventPass;
import ca.ulaval.glo4002.domain.pass.PassOption;

public class SinglePassResponseAssembler extends PassResponseAssembler {

    @Override
    public PassResponse assemble(EventPass eventPass) {
        DailyEventPass singlePass = (DailyEventPass) eventPass;
        return new SinglePassResponse(singlePass.getPassNumber().getValue(), singlePass.getPassCategory().getName(),
                                      PassOption.SINGLEPASS.getName(), singlePass.getEventDate().toString());
    }
}
