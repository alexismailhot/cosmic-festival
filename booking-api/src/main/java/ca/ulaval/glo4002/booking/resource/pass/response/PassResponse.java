package ca.ulaval.glo4002.booking.resource.pass.response;

public class PassResponse {

    private final long passNumber;
    private final String passCategory;
    private final String passOption;

    public PassResponse(long passNumber, String passCategory, String passOption) {
        this.passNumber = passNumber;
        this.passCategory = passCategory;
        this.passOption = passOption;
    }

    public String getPassOption() {
        return passOption;
    }

    public String getPassCategory() {
        return passCategory;
    }

    public long getPassNumber() {
        return passNumber;
    }
}
