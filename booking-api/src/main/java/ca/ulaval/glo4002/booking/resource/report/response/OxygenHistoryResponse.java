package ca.ulaval.glo4002.booking.resource.report.response;

public class OxygenHistoryResponse {

    private final String date;
    private final int qtyOxygenTankBought;
    private final int qtyWaterUsed;
    private final int qtyCandlesUsed;
    private final int qtyOxygenTankMade;

    public OxygenHistoryResponse(String date, int qtyOxygenTankBought, int qtyWaterUsed, int qtyCandlesUsed, int qtyOxygenTankMade) {
        this.date = date;
        this.qtyOxygenTankBought = qtyOxygenTankBought;
        this.qtyWaterUsed = qtyWaterUsed;
        this.qtyCandlesUsed = qtyCandlesUsed;
        this.qtyOxygenTankMade = qtyOxygenTankMade;
    }

    public String getDate() {
        return date;
    }

    public int getQtyOxygenTankBought() {
        return qtyOxygenTankBought;
    }

    public int getQtyWaterUsed() {
        return qtyWaterUsed;
    }

    public int getQtyCandlesUsed() {
        return qtyCandlesUsed;
    }

    public int getQtyOxygenTankMade() {
        return qtyOxygenTankMade;
    }
}
