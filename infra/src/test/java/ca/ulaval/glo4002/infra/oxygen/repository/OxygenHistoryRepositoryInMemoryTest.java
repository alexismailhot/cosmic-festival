package ca.ulaval.glo4002.infra.oxygen.repository;

import static com.google.common.truth.Truth.assertThat;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ca.ulaval.glo4002.domain.oxygen.OxygenHistory;

public class OxygenHistoryRepositoryInMemoryTest {

    private OxygenHistoryRepositoryInMemory oxygenHistoryRepositoryInMemory;

    private static final LocalDate A_DATE = LocalDate.of(2050, 7, 12);

    @BeforeEach
    public void setUp() {
        oxygenHistoryRepositoryInMemory = new OxygenHistoryRepositoryInMemory();
    }

    @Test
    public void givenHistoryIsNotInMemory_whenUpdateHistory_thenOxygenHistoryIsAdded() {
        // given
        OxygenHistory oxygenHistory = createAnHistory();

        // when
        oxygenHistoryRepositoryInMemory.updateHistory(oxygenHistory);

        // then
        assertThat(oxygenHistoryRepositoryInMemory.findByDate(A_DATE)).isEqualTo(Optional.of(oxygenHistory));
    }

    @Test
    public void givenHistoryIsInMemory_whenUpdateHistory_thenOxygenHistoryIsUpdated() {
        // given
        givenOxygenHistoryIsInMemory();

        // when
        OxygenHistory oxygenHistoryUpdated = createAnHistory();
        oxygenHistoryRepositoryInMemory.updateHistory(oxygenHistoryUpdated);

        // then
        assertThat(oxygenHistoryRepositoryInMemory.findByDate(A_DATE)).isEqualTo(Optional.of(oxygenHistoryUpdated));
    }

    @Test
    public void givenNoAssociatedOxygenHistoryInMemory_whenFindByDate_thenReturnEmptyOptional() {
        // when
        Optional<OxygenHistory> oxygenHistory = oxygenHistoryRepositoryInMemory.findByDate(A_DATE);

        // then
        assertThat(oxygenHistory).isEqualTo(Optional.empty());
    }

    @Test
    public void givenAssociatedOxygenHistoryInMemory_whenFindByDate_thenReturnOxygenHistory() {
        // given
        OxygenHistory oxygenHistory = givenOxygenHistoryIsInMemory();

        // when
        Optional<OxygenHistory> foundOxygenHistory = oxygenHistoryRepositoryInMemory.findByDate(A_DATE);

        // then
        assertThat(foundOxygenHistory.get()).isEqualTo(oxygenHistory);
    }

    private OxygenHistory createAnHistory() {
        return new OxygenHistory(A_DATE);
    }

    private OxygenHistory givenOxygenHistoryIsInMemory() {
        OxygenHistory oxygenHistory = createAnHistory();
        oxygenHistoryRepositoryInMemory.updateHistory(oxygenHistory);
        return oxygenHistory;
    }
}
