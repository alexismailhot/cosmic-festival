package ca.ulaval.glo4002.infra.profit.repository;

import ca.ulaval.glo4002.domain.price.Price;
import ca.ulaval.glo4002.domain.profit.Transaction;
import ca.ulaval.glo4002.domain.profit.TransactionType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.google.common.truth.Truth.assertThat;

class TransactionRepositoryInMemoryTest {

    private static final Transaction A_TRANSACTION = new Transaction(Price.of(10), TransactionType.EXPENSE);
    private static final Transaction ANOTHER_TRANSACTION = new Transaction(Price.of(15), TransactionType.EXPENSE) ;
    private TransactionRepositoryInMemory transactionRepository;

    @BeforeEach
    public void setUp() {
        transactionRepository = new TransactionRepositoryInMemory();
    }

    @Test
    public void whenAdd_thenTransactionIsAddedToRepository() {
        //when
        transactionRepository.add(A_TRANSACTION);

        //then
        assertThat(transactionRepository.getTransactions()).contains(A_TRANSACTION);
    }

    @Test
    void whenGetTransactions_thenAllTransactionsAreReturned() {
        //when
        transactionRepository.add(A_TRANSACTION);
        transactionRepository.add(ANOTHER_TRANSACTION);

        List<Transaction> transactions = transactionRepository.getTransactions();

        //then
        assertThat(transactions).containsExactly(A_TRANSACTION, ANOTHER_TRANSACTION);
    }
}
