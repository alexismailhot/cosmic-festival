package ca.ulaval.glo4002.infra.pass.repository;

import ca.ulaval.glo4002.domain.pass.DailyEventPass;
import ca.ulaval.glo4002.domain.pass.EventPass;
import ca.ulaval.glo4002.domain.pass.PassCategory;
import ca.ulaval.glo4002.domain.pass.PassNumber;
import ca.ulaval.glo4002.domain.price.Price;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static com.google.common.truth.Truth.assertThat;

class EventPassRepositoryInMemoryTest {

    private static final PassCategory A_PASS_CATEGORY = PassCategory.NEBULA;
    private static final PassNumber A_PASS_ID = new PassNumber(0L);
    private static final PassNumber ANOTHER_PASS_ID = new PassNumber(1L);
    private static final Price A_PRICE = Price.of(100);
    private static final LocalDate AN_EVENT_DATE = LocalDate.of(2050, 7, 12);
    private static final LocalDate ANOTHER_EVENT_DATE = LocalDate.of(2050, 7, 11);

    private EventPassRepositoryInMemory eventPassRepository;

    @BeforeEach
    public void setUp() {
        eventPassRepository = new EventPassRepositoryInMemory();
    }

    @Test
    public void whenAdd_thenPassIsPersisted() {
         // when
        EventPass eventPass = new EventPass(A_PASS_ID, A_PASS_CATEGORY, A_PRICE);
        eventPassRepository.add(eventPass);

          // then
        assertThat(eventPassRepository.getCount()).isEqualTo(1);
    }

    @Test
    public void givenTwoPersistedPassesWithDifferentDates_whenGetCountByDate_thenCountOfValidOrderOnPassedDateOnlyIsReturned() {
        //given
        DailyEventPass dailyEventPass = new DailyEventPass(A_PASS_ID, A_PASS_CATEGORY, AN_EVENT_DATE, A_PRICE);
        DailyEventPass another_dailyEventPass = new DailyEventPass(ANOTHER_PASS_ID, A_PASS_CATEGORY, ANOTHER_EVENT_DATE, A_PRICE);
        eventPassRepository.add(dailyEventPass);
        eventPassRepository.add(another_dailyEventPass);

        //when
        int persistedPassCountForADate = eventPassRepository.getCountByDate(AN_EVENT_DATE);

        //then
        assertThat(persistedPassCountForADate).isEqualTo(1);
    }

}