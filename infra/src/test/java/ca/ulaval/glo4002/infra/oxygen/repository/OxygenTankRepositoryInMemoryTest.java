package ca.ulaval.glo4002.infra.oxygen.repository;

import static com.google.common.truth.Truth.assertThat;

import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;
import ca.ulaval.glo4002.domain.oxygen.OxygenTank;

@ExtendWith(MockitoExtension.class)
public class OxygenTankRepositoryInMemoryTest {

    private static final int A_QTY_OF_GRADE_B_TANKS = 2;
    private static final int A_QTY_OF_GRADE_A_TANKS = 4;
    private static final List<OxygenTank> A_LIST_OF_GRADE_A_TANKS = Collections.nCopies(A_QTY_OF_GRADE_A_TANKS,
                                                                                        new OxygenTank(OxygenGrade.GRADE_A));
    private static final List<OxygenTank> A_LIST_OF_GRADE_B_TANKS = Collections.nCopies(A_QTY_OF_GRADE_B_TANKS,
                                                                                        new OxygenTank(OxygenGrade.GRADE_B));
    private OxygenTankRepositoryInMemory oxygenTankRepositoryInMemory;

    @BeforeEach
    public void setUp() {
        oxygenTankRepositoryInMemory = new OxygenTankRepositoryInMemory();
    } 

    @Test
    public void givenNoTanksInRepository_whenFindByGrade_thenEmptyListReturned() {
        // when
        List<OxygenTank> foundTanks = oxygenTankRepositoryInMemory.findByGrade(OxygenGrade.GRADE_A);

        // then
        assertThat(foundTanks.isEmpty()).isTrue();
    }

    @Test
    public void givenTanksInRepository_whenFindByGrade_thenOnlyTanksOfSameGradeReturned() {
        // given
        oxygenTankRepositoryInMemory.addOxygenTanks(A_LIST_OF_GRADE_A_TANKS);
        oxygenTankRepositoryInMemory.addOxygenTanks(A_LIST_OF_GRADE_B_TANKS);

        // when
        List<OxygenTank> foundTanks = oxygenTankRepositoryInMemory.findByGrade(OxygenGrade.GRADE_B);

        // then
        assertThat(foundTanks).containsAllIn(A_LIST_OF_GRADE_B_TANKS);
    }

    @Test
    public void givenNoTanksInRepository_whenFindAvailableTanksByGrade_thenEmptyListReturned() {
        // when
        List<OxygenTank> foundTanks = oxygenTankRepositoryInMemory.findAvailableTanksByGrade(OxygenGrade.GRADE_A);

        // then
        assertThat(foundTanks.isEmpty()).isTrue();
    }

    @Test
    public void givenTanksInRepository_whenFindAvailableTanksByGrade_thenListOfUnattributedTanksFilteredByGradeIsReturned() {
        // given
        oxygenTankRepositoryInMemory.addOxygenTanks(A_LIST_OF_GRADE_A_TANKS);
        OxygenTank attributedTank = createAttributedGradeATank();
        oxygenTankRepositoryInMemory.addOxygenTanks(Collections.singletonList(attributedTank));

        // when
        List<OxygenTank> foundTanks = oxygenTankRepositoryInMemory.findAvailableTanksByGrade(OxygenGrade.GRADE_A);

        // then
        assertThat(foundTanks).containsAllIn(A_LIST_OF_GRADE_A_TANKS);
        assertThat(foundTanks).doesNotContain(attributedTank);
    }

    @Test
    public void givenAttributedTanksInRepository_whenFindAvailableTanksByGrade_thenEmptyListIsReturned() {
        // given
        List<OxygenTank> listOfAttributedTanks = Collections.nCopies(A_QTY_OF_GRADE_A_TANKS, createAttributedGradeATank());
        oxygenTankRepositoryInMemory.addOxygenTanks(listOfAttributedTanks);

        // when
        List<OxygenTank> foundTanks = oxygenTankRepositoryInMemory.findAvailableTanksByGrade(OxygenGrade.GRADE_A);

        // then
        assertThat(foundTanks.isEmpty()).isTrue();
    }

    @Test
    public void givenTankNotInRepository_whenUpdate_thenTankIsAddedToRepository() {
        // given
        OxygenTank oxygenTank = createAttributedGradeATank();

        // when
        oxygenTankRepositoryInMemory.update(oxygenTank);
        List<OxygenTank> foundTanks = oxygenTankRepositoryInMemory.getAllTanks();

        // then
        assertThat(foundTanks).contains(oxygenTank);
    }

    @Test
    public void givenTankInRepository_whenUpdate_thenTankIsUpdated() {
        // given
        OxygenTank oxygenTank = createUnattributedGradeBTank();
        oxygenTankRepositoryInMemory.addOxygenTanks(Collections.singletonList(oxygenTank));

        // when
        oxygenTank.markAsAttributed();
        oxygenTankRepositoryInMemory.update(oxygenTank);
        List<OxygenTank> foundTanks = oxygenTankRepositoryInMemory.findByGrade(OxygenGrade.GRADE_B);

        // then
        assertThat(foundTanks).contains(oxygenTank);
        assertThat(foundTanks.get(foundTanks.indexOf(oxygenTank)).isAttributed()).isTrue();
    }

    private OxygenTank createAttributedGradeATank() {
        OxygenTank oxygenTank = new OxygenTank(OxygenGrade.GRADE_A);
        oxygenTank.markAsAttributed();
        return oxygenTank;
    }

    private OxygenTank createUnattributedGradeBTank() {
        return new OxygenTank(OxygenGrade.GRADE_B);
    }
}