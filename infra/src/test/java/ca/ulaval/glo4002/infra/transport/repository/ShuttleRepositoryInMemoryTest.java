package ca.ulaval.glo4002.infra.transport.repository;

import static com.google.common.truth.Truth.assertThat;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ca.ulaval.glo4002.domain.transport.Shuttle;
import ca.ulaval.glo4002.domain.transport.ShuttleDirection;
import ca.ulaval.glo4002.domain.transport.ShuttleType;

class ShuttleRepositoryInMemoryTest {

    private static final LocalDate AN_EVENT_DATE = LocalDate.of(2050, 7, 12);
    private static final LocalDate ANOTHER_EVENT_DATE = LocalDate.of(2050, 7, 20);
    private static final ShuttleType A_SHUTTLE_TYPE = ShuttleType.MILLENIUM_FALCON;
    private static final ShuttleDirection A_SHUTTLE_DIRECTION = ShuttleDirection.ARRIVAL;
    private static final ShuttleDirection A_DIFFERENT_SHUTTLE_DIRECTION = ShuttleDirection.DEPARTURE;
    private static final ShuttleType A_DIFFERENT_SHUTTLE_TYPE = ShuttleType.ET_SPACESHIP;

    private ShuttleRepositoryInMemory shuttleRepository;

    @BeforeEach
    public void setUp() {
        shuttleRepository = new ShuttleRepositoryInMemory();
    }

    @Test
    public void givenShuttleIsNotAlreadyPersisted_whenUpsertShuttle_thenShuttleIsPersisted() {
        // given
        Shuttle shuttle = createShuttle(AN_EVENT_DATE);

        // when
        shuttleRepository.upsertShuttle(shuttle);

        //then
        assertThat(shuttleRepository.findByEventDate(AN_EVENT_DATE)).containsExactly(shuttle);
    }

    @Test
    public void givenShuttleIsAlreadyPersisted_whenUpsertShuttleWithNewShuttleWithSameSpecifications_thenReturnBothShuttlesForLocalDate() {
        // given
        Shuttle shuttle = createShuttle();
        shuttleRepository.upsertShuttle(shuttle);

        // when
        Shuttle updatedShuttle = createShuttle();
        shuttleRepository.upsertShuttle(updatedShuttle);

        // then
        assertThat(shuttleRepository.findByEventDate(AN_EVENT_DATE)).containsExactly(updatedShuttle, shuttle);
    }

    @Test
    public void givenNoAssociatedShuttlesInMemoryForADate_whenFindByEventDate_thenEmptyShuttleListIsReturned() {
        // when
        List<Shuttle> shuttles = shuttleRepository.findByEventDate(AN_EVENT_DATE);
        
        // then
        assertThat(shuttles).isEmpty();
    }

    @Test
    public void givenOnlyShuttlesWithDifferentDates_whenFindByEventDate_thenEmptyListIsReturned() {
        // given
        Shuttle shuttle = createShuttle(AN_EVENT_DATE);
        shuttleRepository.upsertShuttle(shuttle);

        // when
        List<Shuttle> shuttles = shuttleRepository.findByEventDate(ANOTHER_EVENT_DATE);

        // then
        assertThat(shuttles).isEmpty();
    }

    @Test
    public void givenMultipleShuttlesWithDifferentDates_whenFindByEventDate_thenOnlyShuttlesAssociatedToDateAreReturned() {
        // given
        Shuttle shuttle = createShuttle(AN_EVENT_DATE);
        Shuttle anotherShuttle = createShuttle(ANOTHER_EVENT_DATE);
        
        shuttleRepository.upsertShuttle(shuttle);
        shuttleRepository.upsertShuttle(anotherShuttle);

        // when
        List<Shuttle> shuttles = shuttleRepository.findByEventDate(ANOTHER_EVENT_DATE);

        // then
        assertThat(shuttles).containsExactly(anotherShuttle);
    }

    @Test
    public void givenMultipleShuttlesWithSameDates_whenFindByDate_thenAllShuttlesOfDateAreReturned() {
        // given
        Shuttle shuttle = createShuttle(AN_EVENT_DATE);
        Shuttle anotherShuttle = createShuttle(AN_EVENT_DATE);

        shuttleRepository.upsertShuttle(shuttle);
        shuttleRepository.upsertShuttle(anotherShuttle);

        // when
        List<Shuttle> shuttles = shuttleRepository.findByEventDate(AN_EVENT_DATE);

        // then
        assertThat(shuttles).containsExactly(shuttle, anotherShuttle);
    }

    @Test
    public void givenNoShuttlesWithMatchingDate_whenFindByEventDateDirectionAndShuttleType_thenNoShuttlesAreReturned() {
        // given
        Shuttle shuttle = new Shuttle(A_SHUTTLE_TYPE, AN_EVENT_DATE, A_SHUTTLE_DIRECTION);
        shuttleRepository.upsertShuttle(shuttle);

        // when
        List<Shuttle> shuttles = shuttleRepository.findByEventDateDirectionAndShuttleType(ANOTHER_EVENT_DATE,
                                                                                          A_SHUTTLE_DIRECTION,
                                                                                          A_SHUTTLE_TYPE);

        //then
        assertThat(shuttles).isEmpty();
    }

    @Test
    public void givenNoShuttlesWithMatchingDirection_whenFindByEventDateDirectionAndShuttleType_thenNoShuttlesAreReturned() {
        // given
        Shuttle shuttle = new Shuttle(A_SHUTTLE_TYPE, AN_EVENT_DATE, A_SHUTTLE_DIRECTION);
        shuttleRepository.upsertShuttle(shuttle);

        // when
        List<Shuttle> shuttles = shuttleRepository.findByEventDateDirectionAndShuttleType(AN_EVENT_DATE,
                                                                                          A_DIFFERENT_SHUTTLE_DIRECTION,
                                                                                          A_SHUTTLE_TYPE);

        // then
        assertThat(shuttles).isEmpty();
    }

    @Test
    public void givenNoShuttlesWithMatchingType_whenFindByEventDateDirectionAndShuttleType_thenNoShuttlesAreReturned() {
        // given
        Shuttle shuttle = new Shuttle(A_SHUTTLE_TYPE, AN_EVENT_DATE, A_SHUTTLE_DIRECTION);
        shuttleRepository.upsertShuttle(shuttle);

        // when
        List<Shuttle> shuttles = shuttleRepository.findByEventDateDirectionAndShuttleType(AN_EVENT_DATE,
                                                                                          A_SHUTTLE_DIRECTION,
                                                                                          A_DIFFERENT_SHUTTLE_TYPE);

        // then
        assertThat(shuttles).isEmpty();
    }

    @Test
    public void givenAShuttleWithMatchingAttributes_whenFindByEventDateDirectionAndShuttleType_thenMatchingShuttleIsReturned() {
        // given
        Shuttle shuttle = new Shuttle(A_SHUTTLE_TYPE, AN_EVENT_DATE, A_SHUTTLE_DIRECTION);
        shuttleRepository.upsertShuttle(shuttle);

        // when
        List<Shuttle> shuttles = shuttleRepository.findByEventDateDirectionAndShuttleType(AN_EVENT_DATE,
                                                                                          A_SHUTTLE_DIRECTION,
                                                                                          A_SHUTTLE_TYPE);

        // then
        assertThat(shuttles).containsExactly(shuttle);
    }

    @Test
    public void givenMultipleShuttlesWithMatchingAttributes_whenFindByEventDateDirectionAndShuttleType_thenMatchingShuttlesAreReturned() {
        // given
        Shuttle shuttle = new Shuttle(A_SHUTTLE_TYPE, AN_EVENT_DATE, A_SHUTTLE_DIRECTION);
        Shuttle anotherShuttle = new Shuttle(A_SHUTTLE_TYPE, AN_EVENT_DATE, A_SHUTTLE_DIRECTION);
        shuttleRepository.upsertShuttle(anotherShuttle);
        shuttleRepository.upsertShuttle(shuttle);

        // when
        List<Shuttle> shuttles = shuttleRepository.findByEventDateDirectionAndShuttleType(AN_EVENT_DATE,
                                                                                          A_SHUTTLE_DIRECTION,
                                                                                          A_SHUTTLE_TYPE);

        // then
        assertThat(shuttles).containsExactly(shuttle, anotherShuttle);
    }

    private Shuttle createShuttle(LocalDate eventDate) {
        return new Shuttle(A_SHUTTLE_TYPE, eventDate, A_SHUTTLE_DIRECTION);
    }

    private Shuttle createShuttle() {
        return new Shuttle(A_SHUTTLE_TYPE, AN_EVENT_DATE, A_SHUTTLE_DIRECTION);
    }
}
