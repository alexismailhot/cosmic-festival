package ca.ulaval.glo4002.infra.order.repository;

import ca.ulaval.glo4002.domain.discount.OrderDiscount;
import ca.ulaval.glo4002.domain.discount.PassDiscount;
import ca.ulaval.glo4002.domain.order.Order;
import ca.ulaval.glo4002.domain.order.OrderNumber;
import ca.ulaval.glo4002.domain.pass.EventPass;
import ca.ulaval.glo4002.domain.pass.PassCategory;
import ca.ulaval.glo4002.domain.pass.PassNumber;
import ca.ulaval.glo4002.domain.price.Price;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.google.common.truth.Truth.assertThat;

class OrderRepositoryInMemoryTest {

    private static final OrderNumber AN_ORDER_NUMBER = new OrderNumber("an order number");
    private static final PassCategory A_PASS_CATEGORY = PassCategory.NEBULA;
    private static final PassNumber A_PASS_NUMBER = new PassNumber(0L);
    private static final Price A_PRICE = Price.of(100);
    private static final EventPass AN_EVENT_PASS = new EventPass(A_PASS_NUMBER, A_PASS_CATEGORY, A_PRICE);
    private static final List<EventPass> EVENT_PASSES = Collections.singletonList(AN_EVENT_PASS);
    private static final List<OrderDiscount> SOME_ORDER_DISCOUNTS = Collections.emptyList();
    private static final List<PassDiscount> SOME_PASS_DISCOUNTS = Collections.emptyList();

    private OrderRepositoryInMemory orderRepository;
    private Order order;

    @BeforeEach
    public void setUp() {
        orderRepository = new OrderRepositoryInMemory();
        order = createOrder();
    }

    @Test
    public void whenAdd_thenProcessedOrderIsPersisted() {
        // when
        orderRepository.add(order);

        // then
        assertThat(orderRepository.findByOrderNumber(AN_ORDER_NUMBER)).isEqualTo(Optional.of(order));
    }

    @Test
    public void givenNoAssociatedOrderInMemory_whenFindByOrderNumber_thenEmptyOptionalIsReturned() {
        // when
        Optional<Order> order = orderRepository.findByOrderNumber(AN_ORDER_NUMBER);

        // then
        assertThat(order).isEqualTo(Optional.empty());
    }

    @Test
    public void givenAssociatedOrderInMemory_whenFindByOrderNumber_thenOrderIsReturned() {
        // given
        orderRepository.add(order);

        // when
        Optional<Order> foundOrder = orderRepository.findByOrderNumber(AN_ORDER_NUMBER);

        // then
        assertThat(foundOrder.get()).isEqualTo(order);
    }

    private Order createOrder() {
        return new Order(AN_ORDER_NUMBER, EVENT_PASSES, SOME_ORDER_DISCOUNTS, SOME_PASS_DISCOUNTS);
    }
}
