package ca.ulaval.glo4002.infra.discount.configuration;

import ca.ulaval.glo4002.domain.discount.OrderDiscount;
import ca.ulaval.glo4002.domain.discount.PassDiscount;
import ca.ulaval.glo4002.domain.discount.configuration.DiscountsConfiguration;

import java.util.List;

public class GLOW4002DiscountsConfiguration implements DiscountsConfiguration {

    private final List<OrderDiscount> orderDiscounts;
    private final List<PassDiscount> passDiscounts;

    public GLOW4002DiscountsConfiguration(List<OrderDiscount> orderDiscounts, List<PassDiscount> passDiscounts) {
        this.orderDiscounts = orderDiscounts;
        this.passDiscounts = passDiscounts;
    }

    @Override
    public List<OrderDiscount> getOrderDiscounts() {
        return orderDiscounts;
    }

    @Override
    public List<PassDiscount> getPassDiscounts() {
        return passDiscounts;
    }
}
