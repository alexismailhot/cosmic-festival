package ca.ulaval.glo4002.infra.pass;

import java.util.EnumMap;

import ca.ulaval.glo4002.domain.pass.PassCategory;
import ca.ulaval.glo4002.domain.pass.configuration.PassPricesConfiguration;
import ca.ulaval.glo4002.domain.price.Price;

public class GLOW4002PassPricesConfiguration implements PassPricesConfiguration {

    private final EnumMap<PassCategory, Price> eventPassPrices;
    private final EnumMap<PassCategory, Price> dailyEventPassPrices;

    public GLOW4002PassPricesConfiguration(EnumMap<PassCategory, Price> eventPassPrices,
                                           EnumMap<PassCategory, Price> dailyEventPassPrices) {
        this.eventPassPrices = eventPassPrices;
        this.dailyEventPassPrices = dailyEventPassPrices;
    }

    @Override
    public final EnumMap<PassCategory, Price> getEventPassPrices() {
        return eventPassPrices;
    }

    @Override
    public final EnumMap<PassCategory, Price> getDailyEventPassPrices() {
        return dailyEventPassPrices;
    }
}
