package ca.ulaval.glo4002.infra.artist;

import ca.ulaval.glo4002.domain.artist.configuration.ArtistOxygenNeedsConfiguration;
import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;

public class GLOW4002ArtistOxygenNeedsConfiguration implements ArtistOxygenNeedsConfiguration {

    private final OxygenGrade minimumOxygenGradeNeeded;
    private final int numberOfOxygenTanksNeededPerGroupMember;

    public GLOW4002ArtistOxygenNeedsConfiguration(OxygenGrade minimumOxygenGradeNeeded, int numberOfOxygenTanksNeededPerGroupMember) {
        this.minimumOxygenGradeNeeded = minimumOxygenGradeNeeded;
        this.numberOfOxygenTanksNeededPerGroupMember = numberOfOxygenTanksNeededPerGroupMember;
    }

    @Override
    public OxygenGrade getMinimumOxygenGradeNeeded() {
        return minimumOxygenGradeNeeded;
    }

    @Override
    public int getNumberOfOxygenTanksNeededPerGroupMember() {
        return numberOfOxygenTanksNeededPerGroupMember;
    }
}
