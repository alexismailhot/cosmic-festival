package ca.ulaval.glo4002.infra.pass.repository;

import ca.ulaval.glo4002.domain.pass.EventPass;
import ca.ulaval.glo4002.domain.pass.PassNumber;
import ca.ulaval.glo4002.domain.pass.repository.EventPassRepository;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class EventPassRepositoryInMemory implements EventPassRepository {

    private final Map<PassNumber, EventPass> passes = new HashMap<>();

    @Override
    public void add(EventPass eventPass) {
        passes.put(eventPass.getPassNumber(), eventPass);
    }

    @Override
    public int getCount() {
        return passes.size();
    }

    @Override
    public int getCountByDate(LocalDate eventDate) {
        return Math.toIntExact(passes.values().stream()
                .filter(eventPass -> eventPass.isValidOn(eventDate))
                .count());
    }
}
