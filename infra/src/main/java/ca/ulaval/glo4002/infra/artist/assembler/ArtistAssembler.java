package ca.ulaval.glo4002.infra.artist.assembler;

import ca.ulaval.glo4002.domain.artist.Artist;
import ca.ulaval.glo4002.domain.artist.ArtistNumber;
import ca.ulaval.glo4002.domain.price.Price;
import ca.ulaval.glo4002.infra.artist.dto.ArtistDto;

public class ArtistAssembler {

    private final ArtistNumberAssembler artistNumberAssembler;

    public ArtistAssembler(ArtistNumberAssembler artistNumberAssembler) {
        this.artistNumberAssembler = artistNumberAssembler;
    }

    public Artist assemble(ArtistDto artistDto) {
        ArtistNumber artistNumber = artistNumberAssembler.create(artistDto.getId());
        return new Artist(artistNumber,
                          artistDto.getName(),
                          Price.of(artistDto.getPrice()),
                          artistDto.getPopularityRank(),
                          artistDto.getNbPeople());
    }
}
