package ca.ulaval.glo4002.infra.order.configuration;

import ca.ulaval.glo4002.domain.order.configuration.OrderingPeriodConfiguration;

import java.time.LocalDate;

public class GLOW4002OrderingPeriodConfiguration implements OrderingPeriodConfiguration {

    private LocalDate orderPeriodStart;
    private LocalDate orderPeriodEnd;

    public GLOW4002OrderingPeriodConfiguration(LocalDate orderPeriodStart, LocalDate orderPeriodEnd) {
        this.orderPeriodStart = orderPeriodStart;
        this.orderPeriodEnd = orderPeriodEnd;
    }

    @Override
    public LocalDate getOrderPeriodStart() {
        return orderPeriodStart;
    }

    @Override
    public LocalDate getOrderPeriodEnd() {
        return orderPeriodEnd;
    }

    @Override
    public void updateOrderingPeriodStartDate(LocalDate startDate) {
        this.orderPeriodStart = startDate;
    }

    @Override
    public void updateOrderingPeriodEndDate(LocalDate endDate) {
        this.orderPeriodEnd = endDate;
    }
}
