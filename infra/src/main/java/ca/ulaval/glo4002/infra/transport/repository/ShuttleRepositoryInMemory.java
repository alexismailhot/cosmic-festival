package ca.ulaval.glo4002.infra.transport.repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import ca.ulaval.glo4002.domain.transport.Shuttle;
import ca.ulaval.glo4002.domain.transport.ShuttleDirection;
import ca.ulaval.glo4002.domain.transport.ShuttleType;
import ca.ulaval.glo4002.domain.transport.repository.ShuttleRepository;

public class ShuttleRepositoryInMemory implements ShuttleRepository {

    private final Map<LocalDate, List<Shuttle>> shuttles = new HashMap<>();

    @Override
    public List<Shuttle> getAllShuttles() {
        return shuttles.isEmpty() ? Collections.emptyList() : shuttles.values()
                .stream()
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    @Override
    public List<Shuttle> findByEventDate(LocalDate eventDate) {
        return shuttles.get(eventDate) != null ? shuttles.get(eventDate) : new ArrayList<>();
    }

    @Override
    public List<Shuttle> findByEventDateDirectionAndShuttleType(LocalDate eventDate,
                                                                ShuttleDirection shuttleDirection,
                                                                ShuttleType shuttleType) {
        List<Shuttle> eventDateShuttles = shuttles.get(eventDate) != null ? shuttles.get(eventDate) : new ArrayList<>();
        return eventDateShuttles
                .stream()
                .filter((shuttle) -> shuttle.isSameType(shuttleType))
                .filter((shuttle) -> shuttle.isSameDirection(shuttleDirection))
                .collect(Collectors.toList());
    }

    @Override
    public void upsertShuttle(Shuttle shuttle) {
        LocalDate eventDate = shuttle.getEventDate();
        List<Shuttle> eventDateShuttles = shuttles.get(eventDate) != null ? shuttles.get(eventDate) : new ArrayList<>();
        eventDateShuttles.remove(shuttle);
        eventDateShuttles.add(shuttle);
        shuttles.put(eventDate, eventDateShuttles);
    }
}
