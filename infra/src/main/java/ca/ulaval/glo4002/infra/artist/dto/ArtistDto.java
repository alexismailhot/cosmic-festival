package ca.ulaval.glo4002.infra.artist.dto;
import java.util.List;

public class ArtistDto {

    private int id;
    private String name;
    private int nbPeople;
    private String musicStyle;
    private double price;
    private int popularityRank;
    private List<AvailabilityDto> availabilities;

    public ArtistDto() {

    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getPopularityRank() {
        return popularityRank;
    }

    public int getId() {
        return id;
    }

    public int getNbPeople() {
        return nbPeople;
    }

    public String getMusicStyle() {
        return musicStyle;
    }

    public List<AvailabilityDto> getAvailabilities() {
        return availabilities;
    }
}
