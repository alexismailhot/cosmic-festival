package ca.ulaval.glo4002.infra.oxygen.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;
import ca.ulaval.glo4002.domain.oxygen.OxygenTank;
import ca.ulaval.glo4002.domain.oxygen.repository.OxygenTankRepository;

public class OxygenTankRepositoryInMemory implements OxygenTankRepository {

    private final List<OxygenTank> oxygenTanks;

    public OxygenTankRepositoryInMemory() {
        this.oxygenTanks = new ArrayList<>();
    }

    public void addOxygenTanks(List<OxygenTank> newTanks) {
        oxygenTanks.addAll(newTanks);
    }

    public List<OxygenTank> getAllTanks() {
        return oxygenTanks;
    }

    public List<OxygenTank> findByGrade(OxygenGrade oxygenGrade) {
        return filterByGrade(oxygenTanks, oxygenGrade);
    }

    @Override
    public void update(OxygenTank oxygenTank) {
        int oxygenTankIndex = oxygenTanks.indexOf(oxygenTank);
        if (oxygenTankIndex != -1) {
            oxygenTanks.set(oxygenTankIndex, oxygenTank);
        } else {
            oxygenTanks.add(oxygenTank);
        }
    }

    public List<OxygenTank> findAvailableTanksByGrade(OxygenGrade oxygenGrade) {
        List<OxygenTank> availableTanks = findUnattributedTanks();
        return filterByGrade(availableTanks, oxygenGrade);
    }

    private List<OxygenTank> filterByGrade(List<OxygenTank> tanks, OxygenGrade oxygenGrade) {
        return tanks.stream()
                    .filter(oxygenTank -> oxygenTank.getOxygenGrade().equals(oxygenGrade))
                    .collect(Collectors.toList());
    }
    
    private List<OxygenTank> findUnattributedTanks() {
        return oxygenTanks.stream()
                          .filter(oxygenTank -> !oxygenTank.isAttributed())
                          .collect(Collectors.toList());
    }
}
