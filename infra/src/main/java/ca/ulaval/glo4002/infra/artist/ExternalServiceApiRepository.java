package ca.ulaval.glo4002.infra.artist;

import ca.ulaval.glo4002.domain.artist.Artist;
import ca.ulaval.glo4002.infra.artist.assembler.ArtistAssembler;
import ca.ulaval.glo4002.infra.artist.dto.ArtistDto;
import ca.ulaval.glo4002.service.artist.ArtistRepository;
import kong.unirest.GenericType;
import kong.unirest.Unirest;

import java.util.List;
import java.util.stream.Collectors;

public class ExternalServiceApiRepository implements ArtistRepository {

    private final ArtistAssembler artistAssembler;
    private final String externalServiceApiUrl;

    public ExternalServiceApiRepository(ArtistAssembler artistAssembler, String externalServiceApiUrl) {
        this.artistAssembler = artistAssembler;
        this.externalServiceApiUrl = externalServiceApiUrl;
    }

    @Override
    public List<Artist> getArtists() {
        List<ArtistDto> artistDtos = Unirest.get(externalServiceApiUrl).asObject(new GenericType<List<ArtistDto>>(){}).getBody();
        return artistDtos
                .stream()
                .map(artistAssembler::assemble)
                .collect(Collectors.toList());
    }
}
