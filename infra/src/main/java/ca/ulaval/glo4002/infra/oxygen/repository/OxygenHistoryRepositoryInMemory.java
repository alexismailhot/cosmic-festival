package ca.ulaval.glo4002.infra.oxygen.repository;

import ca.ulaval.glo4002.domain.oxygen.OxygenHistory;
import ca.ulaval.glo4002.domain.oxygen.repository.OxygenHistoryRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public class OxygenHistoryRepositoryInMemory implements OxygenHistoryRepository {

    private final HashMap<LocalDate, OxygenHistory> histories = new HashMap<>();

    @Override
    public Optional<OxygenHistory> findByDate(LocalDate date) {
        return Optional.ofNullable(histories.get(date));
    }

    @Override
    public void updateHistory(OxygenHistory history) {
        histories.put(history.getDate(), history);
    }

    @Override
    public List<OxygenHistory> getAll() {
        return new ArrayList<>(histories.values());
    }
}
