package ca.ulaval.glo4002.infra.order.repository;

import ca.ulaval.glo4002.domain.order.Order;
import ca.ulaval.glo4002.domain.order.OrderNumber;
import ca.ulaval.glo4002.domain.order.repository.OrderRepository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class OrderRepositoryInMemory implements OrderRepository {

    private final Map<OrderNumber, Order> orders = new HashMap<>();

    @Override
    public void add(Order order) {
        orders.put(order.getNumber(), order);
    }

    @Override
    public Optional<Order> findByOrderNumber(OrderNumber orderNumber) {
        return Optional.ofNullable(orders.get(orderNumber));
    }

    @Override
    public int getCount() {
        return orders.size();
    }
}
