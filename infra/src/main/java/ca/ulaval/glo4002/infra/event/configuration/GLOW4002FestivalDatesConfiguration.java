package ca.ulaval.glo4002.infra.event.configuration;

import java.time.LocalDate;

import ca.ulaval.glo4002.domain.event.configuration.FestivalDatesConfiguration;

public class GLOW4002FestivalDatesConfiguration implements FestivalDatesConfiguration {

    private LocalDate festivalStartDate;
    private LocalDate festivalEndDate;
    private final LocalDate festivalProgramUnveilingDate;

    public GLOW4002FestivalDatesConfiguration(LocalDate festivalStartDate,
                                              LocalDate festivalEndDate,
                                              LocalDate festivalProgramUnveilingDate) {
        this.festivalStartDate = festivalStartDate;
        this.festivalEndDate = festivalEndDate;
        this.festivalProgramUnveilingDate = festivalProgramUnveilingDate;
    }

    @Override
    public LocalDate getFestivalStartDate() {
        return festivalStartDate;
    }

    @Override
    public LocalDate getFestivalEndDate() {
        return festivalEndDate;
    }

    @Override
    public LocalDate getFestivalProgramUnveilingDate() {
        return festivalProgramUnveilingDate;
    }

    @Override
    public void updateFestivalStartDate(LocalDate startDate) {
        this.festivalStartDate = startDate;
    }

    @Override
    public void updateFestivalEndDate(LocalDate endDate) {
        this.festivalEndDate = endDate;
    }
}
