package ca.ulaval.glo4002.infra.profit.repository;

import ca.ulaval.glo4002.domain.profit.Transaction;
import ca.ulaval.glo4002.domain.profit.repository.TransactionRepository;

import java.util.ArrayList;
import java.util.List;

public class TransactionRepositoryInMemory implements TransactionRepository {

    private final List<Transaction> transactions;

    public TransactionRepositoryInMemory() {
        this.transactions = new ArrayList<>();
    }

    @Override
    public void add(Transaction transaction) {
        transactions.add(transaction);
    }

    @Override
    public List<Transaction> getTransactions() {
        return transactions;
    }
}
