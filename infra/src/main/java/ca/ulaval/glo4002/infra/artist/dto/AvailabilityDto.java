package ca.ulaval.glo4002.infra.artist.dto;

public class AvailabilityDto {

    private String availability;

    public AvailabilityDto() {

    }

    public String getAvailability() {
        return availability;
    }
}
