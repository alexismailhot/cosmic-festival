package ca.ulaval.glo4002.infra.artist;

import ca.ulaval.glo4002.domain.artist.configuration.ArtistShuttleTypeConfiguration;
import ca.ulaval.glo4002.domain.transport.ShuttleType;

public class GLOW4002ArtistShuttleTypeConfiguration implements ArtistShuttleTypeConfiguration {

    private final ShuttleType shuttleTypeForSoloArtists;
    private final ShuttleType shuttleTypeForGroupArtists;

    public GLOW4002ArtistShuttleTypeConfiguration(ShuttleType shuttleTypeForSoloArtists, ShuttleType shuttleTypeForGroupArtists) {
        this.shuttleTypeForSoloArtists = shuttleTypeForSoloArtists;
        this.shuttleTypeForGroupArtists = shuttleTypeForGroupArtists;
    }

    @Override
    public ShuttleType getShuttleTypeForSoloArtists() {
        return shuttleTypeForSoloArtists;
    }

    @Override
    public ShuttleType getShuttleTypeForGroupArtists() {
        return shuttleTypeForGroupArtists;
    }
}
