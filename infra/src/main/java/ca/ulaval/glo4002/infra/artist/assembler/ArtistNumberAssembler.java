package ca.ulaval.glo4002.infra.artist.assembler;

import ca.ulaval.glo4002.domain.artist.ArtistNumber;

public class ArtistNumberAssembler {

    public ArtistNumber create(int artistId) {
        return new ArtistNumber(artistId);
    }
}
