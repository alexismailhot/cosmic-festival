package ca.ulaval.glo4002.domain.artist;

import ca.ulaval.glo4002.domain.artist.configuration.ArtistShuttleTypeConfiguration;
import ca.ulaval.glo4002.domain.transport.ShuttleType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ArtistShuttleTypeTest {

    private static final ShuttleType SHUTTLE_TYPE_FOR_SOLO_ARTISTS = ShuttleType.ET_SPACESHIP;
    private static final ShuttleType SHUTTLE_TYPE_FOR_GROUP_ARTISTS = ShuttleType.MILLENIUM_FALCON;
    private static final int MORE_THAN_ONE_GROUP_MEMBERS = 56;
    private static final int ONE_GROUP_MEMBER = 1;

    @Mock
    private ArtistShuttleTypeConfiguration shuttleTypeConfiguration;

    private ArtistShuttleType artistShuttleType;

    @BeforeEach
    public void setUp() {
        artistShuttleType = new ArtistShuttleType(shuttleTypeConfiguration);
    }

    @Test
    public void whenGetShuttleTypeForOneGroupMember_thenIsTheShuttleTypeForSoloArtists() {
        // when
        when(shuttleTypeConfiguration.getShuttleTypeForSoloArtists()).thenReturn(SHUTTLE_TYPE_FOR_SOLO_ARTISTS);
        ShuttleType shuttleType = artistShuttleType.getShuttleTypeFor(ONE_GROUP_MEMBER);

        // then
        assertThat(shuttleType).isEqualTo(SHUTTLE_TYPE_FOR_SOLO_ARTISTS);
    }

    @Test
    public void whenGetShuttleTypeForMoreThanOneGroupMembers_thenIsTheShuttleTypeForGroupArtists() {
        // when
        when(shuttleTypeConfiguration.getShuttleTypeForGroupArtists()).thenReturn(SHUTTLE_TYPE_FOR_GROUP_ARTISTS);
        ShuttleType shuttleType = artistShuttleType.getShuttleTypeFor(MORE_THAN_ONE_GROUP_MEMBERS);

        // then
        assertThat(shuttleType).isEqualTo(SHUTTLE_TYPE_FOR_GROUP_ARTISTS);
    }
}
