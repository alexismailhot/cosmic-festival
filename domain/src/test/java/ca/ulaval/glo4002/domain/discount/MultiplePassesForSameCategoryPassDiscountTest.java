package ca.ulaval.glo4002.domain.discount;

import ca.ulaval.glo4002.domain.pass.PassCategory;
import ca.ulaval.glo4002.domain.price.Price;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;

class MultiplePassesForSameCategoryPassDiscountTest {

    private static final int NUMBER_OF_PASSES_REQUIRED = 5;
    private static final int AN_INSUFFICIENT_NUMBER_OF_PASSES = 3;
    private static final PassCategory APPLICABLE_CATEGORY = PassCategory.NEBULA;
    private static final PassCategory A_NON_APPLICABLE_CATEGORY = PassCategory.SUPERGIANT;
    private static final double PRICE_AMOUNT = 100;
    private static final Price AN_INITIAL_PRICE = Price.of(PRICE_AMOUNT);
    private static final Price A_PRICE_DISCOUNT = Price.of(100);

    private MultiplePassesForSameCategoryPassDiscount offer;

    @BeforeEach
    public void setUp() {
        offer = new MultiplePassesForSameCategoryPassDiscount(APPLICABLE_CATEGORY, NUMBER_OF_PASSES_REQUIRED, A_PRICE_DISCOUNT);
    }

    @Test
    public void whenIsDiscountApplicableWithApplicableCategoryAndNumberOfPasses_thenReturnTrue() {
        // when
        boolean isApplicable = offer.isDiscountApplicable(createCategories(APPLICABLE_CATEGORY, NUMBER_OF_PASSES_REQUIRED));

        // then
        assertThat(isApplicable).isTrue();
    }

    @Test
    public void whenIsDiscountApplicableWithNonApplicableCategoryAndNumberOfPasses_thenReturnFalse() {
        // when
        boolean isApplicable = offer.isDiscountApplicable(createCategories(A_NON_APPLICABLE_CATEGORY, NUMBER_OF_PASSES_REQUIRED));

        // then
        assertThat(isApplicable).isFalse();
    }

    @Test
    public void whenIsDiscountApplicableWithApplicableCategoryAndInsufficientNumberOfPasses_thenReturnFalse() {
        // when
        boolean isApplicable = offer.isDiscountApplicable(createCategories(APPLICABLE_CATEGORY, AN_INSUFFICIENT_NUMBER_OF_PASSES));

        // then
        assertThat(isApplicable).isFalse();
    }

    @Test
    public void whenIsDiscountApplicableWithNonApplicableCategoryAndInsufficientNumberOfPasses_thenReturnFalse() {
        // when
        boolean isApplicable = offer.isDiscountApplicable(createCategories(A_NON_APPLICABLE_CATEGORY, AN_INSUFFICIENT_NUMBER_OF_PASSES));

        // then
        assertThat(isApplicable).isFalse();
    }

    @Test
    public void whenApplyDiscountWithApplicableCategory_thenReturnPriceWithAppliedDiscount() {
        // when
        Price price = offer.applyDiscountIfApplicable(AN_INITIAL_PRICE, APPLICABLE_CATEGORY);

        // then
        Price expectedDiscountedPrice = AN_INITIAL_PRICE.subtract(A_PRICE_DISCOUNT);
        assertThat(price).isEquivalentAccordingToCompareTo(expectedDiscountedPrice);
    }

    @Test
    public void whenApplyDiscountWithApplicableCategory_thenReturnInitialPrice() {
        // when
        Price price = offer.applyDiscountIfApplicable(AN_INITIAL_PRICE, A_NON_APPLICABLE_CATEGORY);

        // then
        assertThat(price).isEquivalentAccordingToCompareTo(AN_INITIAL_PRICE);
    }

    private List<PassCategory> createCategories(PassCategory passCategory, int numberOfPasses) {
        List<PassCategory> categories = new ArrayList<>();
        for (int i = 0; i < numberOfPasses; i++) {
            categories.add(passCategory);
        }
        return categories;
    }
}
