package ca.ulaval.glo4002.domain.order;

import ca.ulaval.glo4002.domain.discount.OrderDiscount;
import ca.ulaval.glo4002.domain.discount.PassDiscount;
import ca.ulaval.glo4002.domain.pass.EventPass;
import ca.ulaval.glo4002.domain.pass.PassCategory;
import ca.ulaval.glo4002.domain.pass.PassNumber;
import ca.ulaval.glo4002.domain.price.Price;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OrderTest {

    private static final OrderNumber AN_ORDER_NUMBER = new OrderNumber("an order number");
    private static final List<OrderDiscount> SOME_ORDER_DISCOUNTS = Collections.emptyList();
    private static final PassNumber A_PASS_NUMBER = new PassNumber(0L);
    private static final PassCategory A_PASS_CATEGORY = PassCategory.NEBULA;
    private static final List<PassDiscount> SOME_PASS_DISCOUNTS = Collections.emptyList();
    private static final Price A_PRICE = Price.of(100);
    private static final Price A_PRICE_GREATER_THAN_ZERO = Price.of(100);

    private Order order;

    @Mock
    private OrderDiscount orderDiscount;

    @Mock
    private PassDiscount passDiscount;

    @Test
    public void whenCalculateTotalPrice_thenEachOrderDiscountIsApplied() {
        // when
        List<OrderDiscount> orderDiscounts = Arrays.asList(orderDiscount, orderDiscount);
        List<PassDiscount> passDiscounts = Arrays.asList(passDiscount, passDiscount);
        when(passDiscount.applyDiscountIfApplicable(any(), any())).thenReturn(A_PRICE);

        order = new Order(AN_ORDER_NUMBER, createEventPasses(), orderDiscounts, passDiscounts);

        order.calculateTotalPrice();

        // then
        verify(orderDiscount, times(orderDiscounts.size())).applyDiscount(any());
    }

    @Test
    public void whenCalculateAndNoOrderDiscounts_thenAddedPassesPriceIsReturned() {
        // when
        List<EventPass> eventPasses = createEventPasses();
        order = new Order(AN_ORDER_NUMBER, eventPasses, Collections.emptyList(), Collections.emptyList());

        Price price = order.calculateTotalPrice();

        // then
        Price expectedPrice = A_PRICE_GREATER_THAN_ZERO.add(A_PRICE_GREATER_THAN_ZERO);
        assertThat(price).isEquivalentAccordingToCompareTo(expectedPrice);
    }

    @Test
    public void whenCalculateAndNoPasses_thenThrowIllegalStateException() {
        // then
        Assertions.assertThrows(IllegalStateException.class, () -> {
            // when
            order = new Order(AN_ORDER_NUMBER, Collections.emptyList(), SOME_ORDER_DISCOUNTS, SOME_PASS_DISCOUNTS);

            order.calculateTotalPrice();
        });
    }

    private List<EventPass> createEventPasses() {
        EventPass eventPass = new EventPass(A_PASS_NUMBER, A_PASS_CATEGORY, A_PRICE);
        return Arrays.asList(eventPass, eventPass);
    }
}
