package ca.ulaval.glo4002.domain.transport;

import static com.google.common.truth.Truth.assertThat;

import java.time.LocalDate;
import java.util.Collections;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import ca.ulaval.glo4002.domain.pass.PassNumber;
import ca.ulaval.glo4002.domain.transport.exception.CannotAddGroupOfPassengersToShuttleException;
import ca.ulaval.glo4002.domain.transport.exception.CannotAddPassengerToShuttleException;

@ExtendWith(MockitoExtension.class)
class ShuttleTest {

    private static final PassengerNumber A_PASSENGER_NUMBER = new PassNumber(0L);
    private static final LocalDate AN_EVENT_DATE = LocalDate.of(2050, 11, 20);
    private static final ShuttleDirection A_SHUTTLE_DIRECTION = ShuttleDirection.ARRIVAL;
    private static final ShuttleType SHUTTLE_TYPE = ShuttleType.MILLENIUM_FALCON;
    private static final int SHUTTLE_CAPACITY = 20;
    private static final int A_NUMBER_OF_PASSENGERS = 5;

    private Shuttle shuttle;

    @BeforeEach
    public void setUp() {
        shuttle = new Shuttle(SHUTTLE_TYPE, AN_EVENT_DATE, A_SHUTTLE_DIRECTION);
    }

    @Test
    public void givenShuttleIsNotFull_whenAddPassenger_thenPassengerIsAdded() {
        // when
        shuttle.addPassenger(A_PASSENGER_NUMBER);

        // then
        assertThat(shuttle.getPassengers()).contains(A_PASSENGER_NUMBER);
    }

    @Test
    public void givenShuttleIsFull_whenAddPassenger_thenThrowCannotAddPassengerToShuttleException() {
        // given
        shuttle.addGroupOfPassengers(Collections.nCopies(SHUTTLE_CAPACITY, A_PASSENGER_NUMBER));

        // then
        Assertions.assertThrows(CannotAddPassengerToShuttleException.class, () -> {
            // when
            shuttle.addPassenger(A_PASSENGER_NUMBER);
        });
    }

    @Test
    public void givenShuttleIsNotFull_whenAddGroupOfPassengers_thenPassengersAreAdded() {
        // when
        shuttle.addGroupOfPassengers(Collections.nCopies(A_NUMBER_OF_PASSENGERS, A_PASSENGER_NUMBER));

        // then
        long numberOfPassengersAdded = shuttle.getPassengers().stream()
                .filter(passengerNumber -> passengerNumber.getValue() == A_PASSENGER_NUMBER.getValue())
                .count();
        assertThat(numberOfPassengersAdded).isEqualTo(A_NUMBER_OF_PASSENGERS);
    }

    @Test
    public void givenShuttleIsFull_whenAddGroupOfPassengers_thenThrowCannotAddPassengerToShuttleException() {
        // given
        shuttle.addGroupOfPassengers(Collections.nCopies(SHUTTLE_CAPACITY, A_PASSENGER_NUMBER));

        // then
        Assertions.assertThrows(CannotAddGroupOfPassengersToShuttleException.class, () -> {
            // when
            shuttle.addGroupOfPassengers(Collections.singletonList(A_PASSENGER_NUMBER));
        });
    }

    @Test
    public void givenLessPassengersInShuttleThanCapacity_whenHasEnoughPlacesForANumberOfPassengersEqualToRemainingCapacity_thenReturnTrue() {
        // given
        ShuttleType shuttleType = ShuttleType.MILLENIUM_FALCON;
        Shuttle shuttle = new Shuttle(shuttleType, AN_EVENT_DATE, A_SHUTTLE_DIRECTION);
        shuttle.addGroupOfPassengers(Collections.nCopies(A_NUMBER_OF_PASSENGERS, A_PASSENGER_NUMBER));

        // when
        int numberOfPassengersEqualToRemainingCapacity = SHUTTLE_CAPACITY - A_NUMBER_OF_PASSENGERS;
        boolean hasEnoughPlacesFor = shuttle.hasEnoughPlacesFor(numberOfPassengersEqualToRemainingCapacity);

        // then
        assertThat(hasEnoughPlacesFor).isTrue();
    }

    @Test
    public void givenLessPassengersInShuttleThanCapacity_whenHasEnoughPlacesForLessPassengersThanRemainingCapacity_thenReturnTrue() {
        // given
        ShuttleType shuttleType = ShuttleType.MILLENIUM_FALCON;
        Shuttle shuttle = new Shuttle(shuttleType, AN_EVENT_DATE, A_SHUTTLE_DIRECTION);
        shuttle.addGroupOfPassengers(Collections.nCopies(A_NUMBER_OF_PASSENGERS, A_PASSENGER_NUMBER));

        // when
        int numberOfPassengersLessThanRemainingCapacity = SHUTTLE_CAPACITY - A_NUMBER_OF_PASSENGERS - 1;
        boolean hasEnoughPlacesFor = shuttle.hasEnoughPlacesFor(numberOfPassengersLessThanRemainingCapacity);

        // then
        assertThat(hasEnoughPlacesFor).isTrue();
    }

    @Test
    public void givenLessPassengersInShuttleThanCapacity_whenHasEnoughPlacesForMorePassengersThanRemainingCapacity_thenReturnFalse() {
        // given
        ShuttleType shuttleType = ShuttleType.MILLENIUM_FALCON;
        Shuttle shuttle = new Shuttle(shuttleType, AN_EVENT_DATE, A_SHUTTLE_DIRECTION);
        shuttle.addGroupOfPassengers(Collections.nCopies(A_NUMBER_OF_PASSENGERS, A_PASSENGER_NUMBER));

        // when
        int numberOfPassengersGreaterThanRemainingCapacity = SHUTTLE_CAPACITY - A_NUMBER_OF_PASSENGERS + 1;
        boolean hasEnoughPlacesFor = shuttle.hasEnoughPlacesFor(numberOfPassengersGreaterThanRemainingCapacity);

        // then
        assertThat(hasEnoughPlacesFor).isFalse();
    }
}
