package ca.ulaval.glo4002.domain.pass;

import ca.ulaval.glo4002.domain.pass.configuration.PassPricesConfiguration;
import ca.ulaval.glo4002.domain.price.Price;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.EnumMap;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PassPricesTest {

    private static final PassCategory A_PASS_CATEGORY = PassCategory.NEBULA;
    private static final Price A_CHEAP_EVENT_PRICE = Price.of(70);
    private static final Price A_MODERATE_EVENT_PRICE = Price.of(700);
    private static final Price AN_EXPENSIVE_EVENT_PRICE = Price.of(7000);
    private static final Price A_CHEAP_DAILY_PRICE = Price.of(10);
    private static final Price A_MODERATE_DAILY_PRICE = Price.of(100);
    private static final Price AN_EXPENSIVE_DAILY_PRICE = Price.of(1000);

    private EnumMap<PassCategory, Price> eventPassPrices;

    private EnumMap<PassCategory, Price> dailyEventPassPrices;

    private PassPrices passPrices;

    @Mock
    PassPricesConfiguration passPriceConfiguration;

    @BeforeEach
    public void setUpPassPrices() {
        setUpEventPassPrices();
        setUpDailyEventPassPrices();

        passPrices = new PassPrices(passPriceConfiguration);
    }

    private void setUpEventPassPrices() {
        eventPassPrices = new EnumMap<>(PassCategory.class);

        eventPassPrices.put(PassCategory.NEBULA, A_CHEAP_EVENT_PRICE);
        eventPassPrices.put(PassCategory.SUPERGIANT, A_MODERATE_EVENT_PRICE);
        eventPassPrices.put(PassCategory.SUPERNOVA, AN_EXPENSIVE_EVENT_PRICE);
    }

    private void setUpDailyEventPassPrices() {
        dailyEventPassPrices = new EnumMap<>(PassCategory.class);

        dailyEventPassPrices.put(PassCategory.NEBULA, A_CHEAP_DAILY_PRICE);
        dailyEventPassPrices.put(PassCategory.SUPERGIANT, A_MODERATE_DAILY_PRICE);
        dailyEventPassPrices.put(PassCategory.SUPERNOVA, AN_EXPENSIVE_DAILY_PRICE);
    }

    @Test
    public void whenGetEventPassPrice_thenReturnPriceCorrespondingToCategory() {
        // when
        when(passPriceConfiguration.getEventPassPrices()).thenReturn(eventPassPrices);
        Price dailyEventPassPrice = passPrices.getEventPassPrice(A_PASS_CATEGORY);

        // then
        Price categoryEventPrice = eventPassPrices.get(A_PASS_CATEGORY);
        assertThat(dailyEventPassPrice).isEqualTo(categoryEventPrice);

    }

    @Test
    public void whenGetDailyEventPassPrice_thenReturnPriceCorrespondingToCategory() {
        // when
        when(passPriceConfiguration.getDailyEventPassPrices()).thenReturn(dailyEventPassPrices);
        Price dailyEventPassPrice = passPrices.getDailyEventPassPrice(A_PASS_CATEGORY);

        // then
        Price categoryDailyPrice = dailyEventPassPrices.get(A_PASS_CATEGORY);
        assertThat(dailyEventPassPrice).isEqualTo(categoryDailyPrice);
    }

    @Test
    public void givenEventPassPricesAreMissingACategory_whenGetEventPassPriceWithMissingCategory_thenOperationFails() {
        // given
        eventPassPrices.remove(A_PASS_CATEGORY);

        // then
        Assertions.assertThrows(IllegalAccessError.class, () -> {
            // when
            when(passPriceConfiguration.getEventPassPrices()).thenReturn(eventPassPrices);
            passPrices.getEventPassPrice(A_PASS_CATEGORY);
        });
    }

    @Test
    public void givenDailyEventPassPricesAreMissingACategory_whenGetDailyEventPassPriceWithMissingCategory_thenOperationFails() {
        // given
        dailyEventPassPrices.remove(A_PASS_CATEGORY);

        // then
        Assertions.assertThrows(IllegalAccessError.class, () -> {
            // when
            when(passPriceConfiguration.getDailyEventPassPrices()).thenReturn(dailyEventPassPrices);
            passPrices.getDailyEventPassPrice(A_PASS_CATEGORY);
        });
    }
}
