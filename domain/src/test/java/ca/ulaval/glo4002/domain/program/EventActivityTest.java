package ca.ulaval.glo4002.domain.program;

import ca.ulaval.glo4002.domain.program.exception.InvalidEventActivityException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class EventActivityTest {

    private static final String AN_INVALID_ACTIVITY_NAME = "an invalid activity";

    @Test
    public void whenFromStringWithInvalidActivityName_thenThrowInvalidActivityException() {
        // then
        Assertions.assertThrows(InvalidEventActivityException.class, () -> {
            // when
            EventActivity.fromString(AN_INVALID_ACTIVITY_NAME);
        });
    }
}
