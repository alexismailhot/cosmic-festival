package ca.ulaval.glo4002.domain.order;

import ca.ulaval.glo4002.domain.order.configuration.OrderingPeriodConfiguration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OrderingPeriodTest {

    private static final LocalDate FESTIVAL_START_DATE = LocalDate.of(2050, 07, 17);
    private final static int MAXIMUM_NUMBER_OF_DAYS_ALLOWED_TO_ORDER_BEFORE_FESTIVAL_START_DATE = 180;
    private static final int MINIMUM_NUMBER_OF_DAYS_ALLOWED_TO_ORDER_BEFORE_FESTIVAL_START_DATE = 1;
    private static final LocalDate ORDERING_PERIOD_START_DATE = LocalDate.of(2050, 4, 1);
    private static final LocalDate ORDERING_PERIOD_END_DATE = LocalDate.of(2050, 8, 1);
    private static final LocalDate DATE_IN_ORDERING_PERIOD = LocalDate.of(2050, 4, 20);
    private static final LocalDate DATE_BEFORE_ORDERING_PERIOD = LocalDate.of(2050, 1, 1);
    private static final LocalDate DATE_AFTER_ORDERING_PERIOD = (LocalDate.of(2050, 11, 1));

    @Mock
    private OrderingPeriodConfiguration orderingPeriodConfiguration;

    private OrderingPeriod orderingPeriod;

    @BeforeEach
    public void setUp() {
        orderingPeriod = new OrderingPeriod(orderingPeriodConfiguration);
    }

    @Test
    public void whenIsDateInOrderingPeriodAndDateIsInOrderingPeriod_thenReturnTrue() {
        // when
        whenOrderingPeriodConfigurationIsCalledToGetOrderingPeriodDates();
        boolean result = orderingPeriod.isDateInOrderingPeriod(DATE_IN_ORDERING_PERIOD);

        // then
        assertThat(result).isTrue();
    }

    @Test
    public void whenIsDateInOrderingPeriodAndDateIsBeforeOrderingPeriod_thenReturnFalse() {
        // when
        whenOrderingPeriodConfigurationIsCalledToGetOrderingPeriodDates();
        boolean result = orderingPeriod.isDateInOrderingPeriod(DATE_BEFORE_ORDERING_PERIOD);

        // then
        assertThat(result).isFalse();
    }

    @Test
    public void whenIsDateInOrderingPeriodAndDateIsAfterOrderingPeriod_thenReturnFalse() {
        // when
        whenOrderingPeriodConfigurationIsCalledToGetOrderingPeriodDates();
        boolean result = orderingPeriod.isDateInOrderingPeriod(DATE_AFTER_ORDERING_PERIOD);

        // then
        assertThat(result).isFalse();
    }

    @Test
    public void whenUpdateOrderingPeriodDates_thenOrderingPeriodConfigurationIsCalledToUpdateOrderingPeriodStartDate() {
        // when
        orderingPeriod.updateOrderingPeriodDates(FESTIVAL_START_DATE);
        LocalDate newOrderingPeriodStartDate = FESTIVAL_START_DATE.minusDays(MAXIMUM_NUMBER_OF_DAYS_ALLOWED_TO_ORDER_BEFORE_FESTIVAL_START_DATE);

        // then
        verify(orderingPeriodConfiguration).updateOrderingPeriodStartDate(newOrderingPeriodStartDate);
    }

    @Test
    public void whenUpdateOrderingPeriodDates_thenOrderingPeriodConfigurationIsCalledToUpdateOrderingPeriodEndDate() {
        // when
        orderingPeriod.updateOrderingPeriodDates(FESTIVAL_START_DATE);
        LocalDate newOrderingPeriodEndDate = FESTIVAL_START_DATE.minusDays(MINIMUM_NUMBER_OF_DAYS_ALLOWED_TO_ORDER_BEFORE_FESTIVAL_START_DATE);

        // then
        verify(orderingPeriodConfiguration).updateOrderingPeriodEndDate(newOrderingPeriodEndDate);
    }

    private void whenOrderingPeriodConfigurationIsCalledToGetOrderingPeriodDates() {
        when(orderingPeriodConfiguration.getOrderPeriodStart()).thenReturn(ORDERING_PERIOD_START_DATE);
        when(orderingPeriodConfiguration.getOrderPeriodEnd()).thenReturn(ORDERING_PERIOD_END_DATE);
    }
}
