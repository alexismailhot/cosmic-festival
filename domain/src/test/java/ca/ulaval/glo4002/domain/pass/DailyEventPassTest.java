package ca.ulaval.glo4002.domain.pass;

import ca.ulaval.glo4002.domain.discount.PassDiscount;
import ca.ulaval.glo4002.domain.price.Price;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class DailyEventPassTest {

    private static final LocalDate AN_EVENT_DATE = LocalDate.of(1, 2, 3);
    private static final LocalDate ANOTHER_EVENT_DATE = LocalDate.of(1, 2, 9);
    private static final PassCategory A_PASS_CATEGORY = PassCategory.NEBULA;
    private static final PassNumber A_PASS_NUMBER = new PassNumber(0L);
    private static final Price A_PRICE = Price.of(100);

    @Mock
    private PassDiscount passDiscount;

    @Test
    public void whenIsValidOnAndEventDateIsPassEventDate_ReturnTrue() {
        // when
        DailyEventPass dailyEventPass = new DailyEventPass(A_PASS_NUMBER, A_PASS_CATEGORY, AN_EVENT_DATE, A_PRICE);
        boolean isValidOn = dailyEventPass.isValidOn(AN_EVENT_DATE);

        // then
        assertThat(isValidOn).isTrue();
    }

    @Test
    public void whenIsValidOnAndEventDateIsNotPassEventDate_ReturnTrue() {
        // when
        DailyEventPass dailyEventPass = new DailyEventPass(A_PASS_NUMBER, A_PASS_CATEGORY, AN_EVENT_DATE, A_PRICE);
        boolean isValidOn = dailyEventPass.isValidOn(ANOTHER_EVENT_DATE);

        // then
        assertThat(isValidOn).isFalse();
    }

    @Test
    public void whenCalculatePrice_thenEachPassDiscountIsAppliedIfApplicable() {
        // when
        DailyEventPass dailyEventPass = new DailyEventPass(A_PASS_NUMBER, A_PASS_CATEGORY, AN_EVENT_DATE, A_PRICE);
        List<PassDiscount> passDiscounts = Arrays.asList(passDiscount, passDiscount);
        dailyEventPass.calculatePrice(passDiscounts);

        // then
        verify(passDiscount, times(passDiscounts.size())).applyDiscountIfApplicable(any(), any());
    }
}
