package ca.ulaval.glo4002.domain.event;

import ca.ulaval.glo4002.domain.event.configuration.FestivalDatesConfiguration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FestivalDatesTest {

    private static final LocalDate EVENT_START_DATE = LocalDate.of(2050, 7, 17);
    private static final LocalDate MIDDLE_OF_FESTIVAL_DATE = LocalDate.of(2050, 7, 18);
    private static final LocalDate EVENT_END_DATE = LocalDate.of(2050, 7, 19);
    private static final LocalDate A_DATE_BEFORE_FESTIVAL_DATES = LocalDate.of(2050, 7, 10);
    private static final LocalDate A_DATE_AFTER_FESTIVAL_DATES = LocalDate.of(2050, 7, 29);

    private FestivalDates festivalDates;
    private List<LocalDate> allFestivalDates;

    @Mock
    private FestivalDatesConfiguration festivalDatesConfiguration;

    @BeforeEach
    public void setUp() {
        festivalDates = new FestivalDates(festivalDatesConfiguration);
        allFestivalDates = Arrays.asList(EVENT_START_DATE, MIDDLE_OF_FESTIVAL_DATE, EVENT_END_DATE);
    }

    @Test
    public void whenIsDateInFestivalDatesAndDateIsInFestivalDates_thenReturnTrue() {
        // when
        whenFestivalDatesConfigurationIsCalledToGetFestivalDates();
        boolean result = festivalDates.isDateInFestivalDates(MIDDLE_OF_FESTIVAL_DATE);

        // then
        assertThat(result).isTrue();
    }

    @Test
    public void whenIsDateInFestivalDatesAndDateIsBeforeFestivalDates_thenReturnFalse() {
        // when
        whenFestivalDatesConfigurationIsCalledToGetFestivalDates();
        boolean result = festivalDates.isDateInFestivalDates(A_DATE_BEFORE_FESTIVAL_DATES);

        // then
        assertThat(result).isFalse();
    }

    @Test
    public void whenIsDateInFestivalDatesAndDateIsAfterFestivalDates_thenReturnFalse() {
        // when
        whenFestivalDatesConfigurationIsCalledToGetFestivalDates();
        boolean result = festivalDates.isDateInFestivalDates(A_DATE_AFTER_FESTIVAL_DATES);

        // then
        assertThat(result).isFalse();
    }

    @Test
    public void whenDoDatesContainExactlyAllFestivalDatesAndAllFestivalDatesArePresentWithoutDuplicate_thenReturnTrue() {
        // when
        whenFestivalDatesConfigurationIsCalledToGetFestivalDates();
        List<LocalDate> dates = Arrays.asList(EVENT_START_DATE, MIDDLE_OF_FESTIVAL_DATE, EVENT_END_DATE);
        boolean result = festivalDates.doDatesContainExactlyAllFestivalDates(dates);

        // then
        assertThat(result).isTrue();
    }

    @Test
    public void whenDoDatesContainExactlyAllFestivalDatesAndThisIsADuplicateDate_thenReturnFalse() {
        // when
        whenFestivalDatesConfigurationIsCalledToGetFestivalDates();
        List<LocalDate> dates = Arrays.asList(EVENT_START_DATE, MIDDLE_OF_FESTIVAL_DATE, EVENT_END_DATE, EVENT_END_DATE);
        boolean result = festivalDates.doDatesContainExactlyAllFestivalDates(dates);

        // then
        assertThat(result).isFalse();
    }

    @Test
    public void whenDoDatesContainExactlyAllFestivalDatesAndThisIsAFestivalDateMissing_thenReturnFalse() {
        // when
        whenFestivalDatesConfigurationIsCalledToGetFestivalDates();
        List<LocalDate> dates = Arrays.asList(EVENT_START_DATE, EVENT_END_DATE);
        boolean result = festivalDates.doDatesContainExactlyAllFestivalDates(dates);

        // then
        assertThat(result).isFalse();
    }

    @Test
    public void whenGetFestivalDurationInDays_thenReturnNumberOfDaysInFestival() {
        // when
        whenFestivalDatesConfigurationIsCalledToGetFestivalDates();
        int numberOfFestivalDays = festivalDates.getFestivalDurationInDays();

        // then
        assertThat(numberOfFestivalDays).isEqualTo(allFestivalDates.size());
    }

    @Test
    public void whenUpdateFestivalStartDate_thenFestivalDatesConfigurationIsCalledToUpdateStartDate() {
        // when
        festivalDates.updateFestivalStartDate(EVENT_START_DATE);

        // then
        verify(festivalDatesConfiguration).updateFestivalStartDate(EVENT_START_DATE);
    }

    @Test
    public void whenUpdateFestivalEndDate_thenFestivalDatesConfigurationIsCalledToUpdateEndDate() {
        // when
        festivalDates.updateFestivalEndDate(EVENT_END_DATE);

        // then
        verify(festivalDatesConfiguration).updateFestivalEndDate(EVENT_END_DATE);
    }

    private void whenFestivalDatesConfigurationIsCalledToGetFestivalDates() {
        when(festivalDatesConfiguration.getFestivalStartDate()).thenReturn(EVENT_START_DATE);
        when(festivalDatesConfiguration.getFestivalEndDate()).thenReturn(EVENT_END_DATE);
    }
}
