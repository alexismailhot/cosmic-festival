package ca.ulaval.glo4002.domain.oxygen;

import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;

class OxygenGradeComparatorTest {

    @Test
    public void whenCompareAndGradeCharacterIsGreaterThanOther_thenResultIsGreaterThanZero() {
        // when
        int result = OxygenGradeComparator.compare(OxygenGrade.GRADE_E, OxygenGrade.GRADE_B);

        // then
        assertThat(result).isGreaterThan(0);
    }

    @Test
    public void whenCompareAndGradeCharacterIsEqualOther_thenResultIsZero() {
        // when
        int result = OxygenGradeComparator.compare(OxygenGrade.GRADE_E, OxygenGrade.GRADE_E);

        // then
        assertThat(result).isEqualTo(0);
    }

    @Test
    public void whenCompareAndGradeCharacterIsLessThanOther_thenResultIsLessThanZero() {
        // when
        int result = OxygenGradeComparator.compare(OxygenGrade.GRADE_A, OxygenGrade.GRADE_B);

        // then
        assertThat(result).isLessThan(0);
    }
}
