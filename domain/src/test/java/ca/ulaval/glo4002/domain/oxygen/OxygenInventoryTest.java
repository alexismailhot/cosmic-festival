package ca.ulaval.glo4002.domain.oxygen;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ca.ulaval.glo4002.domain.oxygen.repository.OxygenTankRepository;

@ExtendWith(MockitoExtension.class)
class OxygenInventoryTest {

    private static final OxygenGrade METHOD_OXYGEN_GRADE = OxygenGrade.GRADE_B;
    private static final OxygenGrade MINIMUM_OXYGEN_GRADE = OxygenGrade.GRADE_A;
    private static final int A_NUMBER_OF_TANKS_NEEDED = 3;
    private static final int A_NUMBER_OF_TANKS_NEEDED_GREATER_THAN_NUMBER_PRODUCED = 7;
    private static final int A_NUMBER_OF_TANKS_NEEDED_LESS_THAN_NUMBER_PRODUCED = 1;
    private static final int A_NUMBER_OF_TANKS_PRODUCED = 4;
    private static final List<OxygenTank> A_SINGLE_AVAILABLE_GRADE_A_TANK_LIST = Collections.singletonList(new OxygenTank(MINIMUM_OXYGEN_GRADE));
    private static final List<OxygenTank> A_SINGLE_AVAILABLE_GRADE_B_TANK_LIST = Collections.singletonList(new OxygenTank(METHOD_OXYGEN_GRADE));
    private static final List<OxygenTank> A_MULTIPLE_AVAILABLE_GRADE_A_TANK_LIST = Collections.nCopies(A_NUMBER_OF_TANKS_PRODUCED,
                                                                                             new OxygenTank(MINIMUM_OXYGEN_GRADE));

    private OxygenInventory oxygenInventory;

    @Mock
    private OxygenTankRepository oxygenTankRepository;

    @BeforeEach
    public void setUp() {
        oxygenInventory = new OxygenInventory(oxygenTankRepository);
    }

    @Test
    public void whenAddOxygenTanks_thenTanksProducedAreAddedToNumberOfTanks() {
        // when
        when(oxygenTankRepository.getAllTanks()).thenReturn(A_SINGLE_AVAILABLE_GRADE_A_TANK_LIST);

        oxygenInventory.addOxygenTanks(A_SINGLE_AVAILABLE_GRADE_A_TANK_LIST);

        // then
        assertThat(oxygenInventory.getNumberOfTanks()).isEqualTo(A_SINGLE_AVAILABLE_GRADE_A_TANK_LIST.size());
    }

    @Test
    public void whenAttributeOxygenWithInsufficientSurplusToMatchTanksNeeded_thenOperationFails() {
        // then
        assertThrows(IllegalArgumentException.class, () -> {
            // when
            when(oxygenTankRepository.findAvailableTanksByGrade(MINIMUM_OXYGEN_GRADE)).thenReturn(A_SINGLE_AVAILABLE_GRADE_A_TANK_LIST);

            oxygenInventory.attributeOxygen(A_NUMBER_OF_TANKS_NEEDED, MINIMUM_OXYGEN_GRADE);
        });
    }

    @Test
    public void givenInventorySurplusIsSmallerThanNumberOfTanksNeeded_whenUseAvailableTanks_thenReturnNumberOfNewTanksNeeded() {
        // given
        oxygenInventory.addOxygenTanks(A_SINGLE_AVAILABLE_GRADE_A_TANK_LIST);

        // when
        when(oxygenTankRepository.findAvailableTanksByGrade(MINIMUM_OXYGEN_GRADE))
            .thenReturn(A_SINGLE_AVAILABLE_GRADE_A_TANK_LIST)
            .thenReturn(Collections.emptyList());

        int newTanksNeeded = oxygenInventory.useAvailableTanks(A_NUMBER_OF_TANKS_NEEDED_GREATER_THAN_NUMBER_PRODUCED,
                                                                             MINIMUM_OXYGEN_GRADE, MINIMUM_OXYGEN_GRADE);

        // then
        assertThat(newTanksNeeded).isEqualTo(A_NUMBER_OF_TANKS_NEEDED_GREATER_THAN_NUMBER_PRODUCED - A_SINGLE_AVAILABLE_GRADE_A_TANK_LIST.size());
    }

    @Test
    public void givenInventorySurplusIsBiggerThanNumberOfTanksNeeded_whenUseAvailableTanks_thenReturnZero() {
        // given
        oxygenInventory.addOxygenTanks(A_MULTIPLE_AVAILABLE_GRADE_A_TANK_LIST);

        // when
        when(oxygenTankRepository.findAvailableTanksByGrade(MINIMUM_OXYGEN_GRADE)).thenReturn(A_MULTIPLE_AVAILABLE_GRADE_A_TANK_LIST);

        int newTanksNeeded = oxygenInventory.useAvailableTanks(A_NUMBER_OF_TANKS_NEEDED_LESS_THAN_NUMBER_PRODUCED,
                                                                             MINIMUM_OXYGEN_GRADE, MINIMUM_OXYGEN_GRADE);

        // then
        assertThat(newTanksNeeded).isEqualTo(0);
    }

    @Test
    public void givenInventorySurplusIsEqualToNumberOfTanksNeeded_whenUseAvailableTanks_thenReturnZero() {
        // given
        oxygenInventory.addOxygenTanks(A_MULTIPLE_AVAILABLE_GRADE_A_TANK_LIST);

        // when
        when(oxygenTankRepository.findAvailableTanksByGrade(MINIMUM_OXYGEN_GRADE)).thenReturn(A_MULTIPLE_AVAILABLE_GRADE_A_TANK_LIST);

        int newTanksNeeded = oxygenInventory.useAvailableTanks(A_NUMBER_OF_TANKS_PRODUCED, MINIMUM_OXYGEN_GRADE, MINIMUM_OXYGEN_GRADE);

        // then
        assertThat(newTanksNeeded).isEqualTo(0);
    }

    @Test
    public void whenUseAvailableTanksAndMethodGradeIsGreaterThanMinimumGrade_thenResultConsidersAllGradeIncludedBetweenMinimumGradeAndMethodGrade() {
        // when
        when(oxygenTankRepository.findAvailableTanksByGrade(METHOD_OXYGEN_GRADE))
            .thenReturn(A_SINGLE_AVAILABLE_GRADE_B_TANK_LIST)
            .thenReturn(Collections.emptyList());
        when(oxygenTankRepository.findAvailableTanksByGrade(MINIMUM_OXYGEN_GRADE))
            .thenReturn(A_SINGLE_AVAILABLE_GRADE_A_TANK_LIST)
            .thenReturn(Collections.emptyList());

        int newTanksNeeded = oxygenInventory.useAvailableTanks(A_NUMBER_OF_TANKS_NEEDED, MINIMUM_OXYGEN_GRADE, METHOD_OXYGEN_GRADE);

        // then
        assertThat(newTanksNeeded).isEqualTo(A_NUMBER_OF_TANKS_NEEDED - (A_SINGLE_AVAILABLE_GRADE_A_TANK_LIST.size() * 2));
    }

    @Test
    public void whenUseAvailableTanksAndMinimumGradeIsGreaterThanMethodGrade_thenThrowIllegalArgumentException() {
        // then
        assertThrows(IllegalArgumentException.class, () -> {
            // when
            OxygenGrade methodOxygenGrade = OxygenGrade.GRADE_A;
            OxygenGrade minimumOxygenGrade = OxygenGrade.GRADE_E;

            oxygenInventory.useAvailableTanks(A_NUMBER_OF_TANKS_NEEDED, minimumOxygenGrade, methodOxygenGrade);
        });
    }
}
