package ca.ulaval.glo4002.domain.oxygen.method;

import static com.google.common.truth.Truth.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;
import ca.ulaval.glo4002.domain.oxygen.factory.OxygenTankFactory;
import ca.ulaval.glo4002.domain.price.Price;

class OxygenSupplyMethodTest {

    private static final int A_NUMBER_OF_TANKS_PRODUCED_PER_BATCH = 10;
    private static final int A_GREATER_THAN_NUMBER_OF_DAYS_TO_FESTIVAL = 100;
    private static final int A_LESS_THAN_NUMBER_OF_DAYS_TO_FESTIVAL = 1;
    private static final int A_FABRICATION_TIME = 10;
    private static final OxygenGrade AN_OXYGEN_GRADE = OxygenGrade.GRADE_B;
    private static final OxygenGrade A_GREATER_THAN_OXYGEN_GRADE = OxygenGrade.GRADE_E;
    private static final OxygenGrade A_LESS_THAN_OXYGEN_GRADE = OxygenGrade.GRADE_A;
    private static final int A_NUMBER_OF_TANKS_BOUGHT = 2;
    private static final Price A_PURCHASE_COST = Price.of(1500);

    private OxygenSupplyMethod oxygenSupplyMethod;

    @BeforeEach
    void setUp() {
        oxygenSupplyMethod = new ElectrolysisOxygenSupplyMethod(new OxygenTankFactory(),
                                                                AN_OXYGEN_GRADE,
                                                                A_NUMBER_OF_TANKS_PRODUCED_PER_BATCH,
                                                                A_FABRICATION_TIME,
                                                                A_NUMBER_OF_TANKS_BOUGHT,
                                                                A_PURCHASE_COST);
    }

    @Test
    public void whenCanProduceInTimeAndNumberOfDaysToFestivalIsGreaterThanFabricationTime_thanReturnTrue() {
        // when
        boolean canProduce = oxygenSupplyMethod.canProduceInTime(A_GREATER_THAN_NUMBER_OF_DAYS_TO_FESTIVAL);

        // then
        assertThat(canProduce).isTrue();
    }

    @Test
    public void whenCanProduceInTimeAndNumberOfDaysToFestivalIsEqualToFabricationTime_thanReturnTrue() {
        // when
        boolean canProduce = oxygenSupplyMethod.canProduceInTime(A_FABRICATION_TIME);

        // then
        assertThat(canProduce).isTrue();
    }

    @Test
    public void whenCanProduceInTimeAndNumberOfDaysToFestivalIsLessThanFabricationTime_thanReturnFalse() {
        // when
        boolean canProduce = oxygenSupplyMethod.canProduceInTime(A_LESS_THAN_NUMBER_OF_DAYS_TO_FESTIVAL);

        // then
        assertThat(canProduce).isFalse();
    }

    @Test
    public void whenMeetsGradeRequirementAndMinimumGradeIsEqualToMethodGrade_thenReturnTrue() {
        // when
        boolean meetsGradeRequirement = oxygenSupplyMethod.meetsGradeRequirement(AN_OXYGEN_GRADE);

        // then
        assertThat(meetsGradeRequirement).isTrue();
    }

    @Test
    public void whenMeetsGradeRequirementAndMinimumGradeIsLessThanMethodGrade_thenReturnTrue() {
        // when
        boolean meetsGradeRequirement = oxygenSupplyMethod.meetsGradeRequirement(A_LESS_THAN_OXYGEN_GRADE);

        // then
        assertThat(meetsGradeRequirement).isTrue();
    }

    @Test
    public void whenMeetsGradeRequirementAndMinimumGradeIsGreaterThanMethodGrade_thenReturnFalse() {
        // when
        boolean meetsGradeRequirement = oxygenSupplyMethod.meetsGradeRequirement(A_GREATER_THAN_OXYGEN_GRADE);

        // then
        assertThat(meetsGradeRequirement).isFalse();
    }
}
