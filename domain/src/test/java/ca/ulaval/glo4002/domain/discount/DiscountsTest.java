package ca.ulaval.glo4002.domain.discount;

import ca.ulaval.glo4002.domain.discount.configuration.DiscountsConfiguration;
import ca.ulaval.glo4002.domain.pass.PassCategory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DiscountsTest {

    private static final List<PassCategory> SOME_PASS_CATEGORIES = Collections.emptyList();

    @Mock
    private DiscountsConfiguration discountsConfiguration;

    @Mock
    private OrderDiscount orderDiscount;

    @Mock
    private PassDiscount passDiscount;

    private Discounts discounts;

    @BeforeEach
    public void setUp() {
        discounts = new Discounts(discountsConfiguration);
    }

    @Test
    public void whenGetApplicableOrderDiscountsAndManyDiscountsAreApplicable_theReturnDiscounts() {
        // when
        when(discountsConfiguration.getOrderDiscounts()).thenReturn(Arrays.asList(orderDiscount, orderDiscount));
        when(orderDiscount.isDiscountApplicable(SOME_PASS_CATEGORIES)).thenReturn(true);
        List<OrderDiscount> orderDiscounts = discounts.getApplicableOrderDiscounts(SOME_PASS_CATEGORIES);

        // then
        assertThat(orderDiscounts).containsExactly(orderDiscount, orderDiscount);
    }

    @Test
    public void whenGetApplicableOrderDiscountsAndNoDiscountIsApplicable_theReturnNoDiscounts() {
        // when
        when(discountsConfiguration.getOrderDiscounts()).thenReturn(Arrays.asList(orderDiscount, orderDiscount));
        when(orderDiscount.isDiscountApplicable(SOME_PASS_CATEGORIES)).thenReturn(false);
        List<OrderDiscount> orderDiscounts = discounts.getApplicableOrderDiscounts(SOME_PASS_CATEGORIES);

        // then
        assertThat(orderDiscounts).isEmpty();
    }

    @Test
    public void whenGetApplicablePassDiscountsAndManyDiscountsAreApplicable_theReturnDiscounts() {
        // when
        when(discountsConfiguration.getPassDiscounts()).thenReturn(Arrays.asList(passDiscount, passDiscount));
        when(passDiscount.isDiscountApplicable(SOME_PASS_CATEGORIES)).thenReturn(true);
        List<PassDiscount> passDiscounts = discounts.getApplicablePassDiscounts(SOME_PASS_CATEGORIES);

        // then
        assertThat(passDiscounts).containsExactly(passDiscount, passDiscount);
    }

    @Test
    public void whenGetApplicablePassDiscountsAndNoDiscountIsApplicable_thenReturnNoDiscounts() {
        // when
        when(discountsConfiguration.getPassDiscounts()).thenReturn(Arrays.asList(passDiscount, passDiscount));
        when(passDiscount.isDiscountApplicable(SOME_PASS_CATEGORIES)).thenReturn(false);
        List<PassDiscount> passDiscounts = discounts.getApplicablePassDiscounts(SOME_PASS_CATEGORIES);

        // then
        assertThat(passDiscounts).isEmpty();
    }
}
