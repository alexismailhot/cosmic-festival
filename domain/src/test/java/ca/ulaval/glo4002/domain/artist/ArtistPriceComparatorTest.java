package ca.ulaval.glo4002.domain.artist;

import ca.ulaval.glo4002.domain.price.Price;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;

class ArtistPriceComparatorTest {

    private static final ArtistNumber AN_ARTIST_NUMBER = new ArtistNumber(8);
    private static final String AN_ARTIST_NAME = "a name";
    private static final int A_NUMBER_OF_GROUP_MEMBERS = 9;
    private static final int A_POPULARITY_RANK = 4;
    private static final int A_SMALLER_POPULARITY_RANK = 2;
    private static final Price A_PRICE = Price.of(1000);
    private static final Price A_SMALLER_PRICE = Price.of(10);

    private ArtistPriceComparator artistPriceComparator;

    @BeforeEach
    public void setUp() {
        artistPriceComparator = new ArtistPriceComparator();
    }

    @Test
    public void givenTwoArtists_whenCompareToAndArtistPriceIsGreaterThanOther_thenResultIsGreaterThanZero() {
        // given
        Artist artist = createArtist(A_PRICE, A_POPULARITY_RANK);
        Artist otherArtist = createArtist(A_SMALLER_PRICE, A_POPULARITY_RANK);

        // when
        int result = artistPriceComparator.compare(artist, otherArtist);

        // then
        assertThat(result).isGreaterThan(0);
    }

    @Test
    public void givenTwoArtists_whenCompareToAndArtistPriceIsLessThanOther_thenResultIsLessThanZero() {
        // given
        Artist artist = createArtist(A_SMALLER_PRICE, A_POPULARITY_RANK);
        Artist otherArtist = createArtist(A_PRICE, A_POPULARITY_RANK);

        // when
        int result = artistPriceComparator.compare(artist, otherArtist);

        // then
        assertThat(result).isLessThan(0);
    }

    @Test
    public void givenTwoArtists_whenCompareToAndArtistPriceIsEqualToOtherAndArtistPopularityRankIsGreaterThanOther_thenResultIsGreaterThanZero() {
        // given
        Artist artist = createArtist(A_PRICE, A_POPULARITY_RANK);
        Artist otherArtist = createArtist(A_PRICE, A_SMALLER_POPULARITY_RANK);

        // when
        int result = artistPriceComparator.compare(artist, otherArtist);

        // then
        assertThat(result).isGreaterThan(0);
    }
    @Test
    public void givenTwoArtists_whenCompareToAndArtistPriceIsEqualToOtherAndArtistPopularityRankIsLessThanOther_thenResultIsLessThanZero() {
        // given
        Artist artist = createArtist(A_PRICE, A_SMALLER_POPULARITY_RANK);
        Artist otherArtist = createArtist(A_PRICE, A_POPULARITY_RANK);

        // when
        int result = artistPriceComparator.compare(artist, otherArtist);

        // then
        assertThat(result).isLessThan(0);
    }

    @Test
    public void givenTwoArtists_whenCompareToAndArtistPriceAndPopularityRankAreEqualsToOther_thenResultIsZero() {
        // given
        Artist artist = createArtist(A_PRICE, A_POPULARITY_RANK);
        Artist otherArtist = createArtist(A_PRICE, A_POPULARITY_RANK);

        // when
        int result = artistPriceComparator.compare(artist, otherArtist);

        // then
        assertThat(result).isEqualTo(0);
    }

    private Artist createArtist(Price price, int popularityRank) {
        return new Artist(AN_ARTIST_NUMBER, AN_ARTIST_NAME, price, popularityRank, A_NUMBER_OF_GROUP_MEMBERS);
    }
}
