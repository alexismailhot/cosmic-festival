package ca.ulaval.glo4002.domain.artist;

import ca.ulaval.glo4002.domain.artist.configuration.ArtistOxygenNeedsConfiguration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ArtistOxygenNeedsTest {

    private static final int A_NUMBER_OF_GROUP_MEMBERS = 42;
    private static final int A_NUMBER_OF_TANKS_NEEDED_PER_GROUP_MEMBER = 6;

    @Mock
    private ArtistOxygenNeedsConfiguration artistOxygenNeedsConfiguration;

    private ArtistOxygenNeeds artistOxygenNeeds;

    @BeforeEach
    public void setUp() {
        artistOxygenNeeds = new ArtistOxygenNeeds(artistOxygenNeedsConfiguration);
    }

    @Test
    public void whenGetNumberOfOxygenTanksNeededForAnArtist_thenReturnTheNumberOfTanksNeededTimesTheNumberOfGroupMembers() {
        // when
        when(artistOxygenNeedsConfiguration.getNumberOfOxygenTanksNeededPerGroupMember()).thenReturn(A_NUMBER_OF_TANKS_NEEDED_PER_GROUP_MEMBER);
        int numberOfOxygenTanksNeeded = artistOxygenNeeds.getNumberOfOxygenTanksNeededFor(A_NUMBER_OF_GROUP_MEMBERS);

        // then
        int expectedNumberOfOxygenTanksNeeded = A_NUMBER_OF_TANKS_NEEDED_PER_GROUP_MEMBER * A_NUMBER_OF_GROUP_MEMBERS;
        assertThat(numberOfOxygenTanksNeeded).isEqualTo(expectedNumberOfOxygenTanksNeeded);
    }
}
