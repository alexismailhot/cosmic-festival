package ca.ulaval.glo4002.domain.price;

import static com.google.common.truth.Truth.assertThat;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PriceTest {

    private static final double PRICE_AMOUNT = 500;
    private static final double OTHER_PRICE_AMOUNT = 100;
    private static final double PERCENT_DISCOUNT = 20;
    private static final double LOWER_THAN_ZERO_PERCENT_DISCOUNT = -10;
    private static final double OVER_ONE_HUNDRED_PERCENT_DISCOUNT = 1000;
    private static final int A_FACTOR = 2;

    private Price price;

    @BeforeEach
    public void setUp() {
        price = Price.of(PRICE_AMOUNT);
    }

    @Test
    public void whenAdd_thenReturnedAdditionOfTwoPrices() {
        // when
        Price otherPrice = Price.of(OTHER_PRICE_AMOUNT);
        Price result = price.add(otherPrice);

        // then
        assertThat(result).isEquivalentAccordingToCompareTo(Price.of(PRICE_AMOUNT + OTHER_PRICE_AMOUNT));
    }

    @Test
    public void whenSubtract_thenResultIsOtherPriceSubtractedToPrice() {
        // when
        Price otherPrice = Price.of(OTHER_PRICE_AMOUNT);
        Price result = price.subtract(otherPrice);

        // then
        assertThat(result).isEquivalentAccordingToCompareTo(Price.of(PRICE_AMOUNT - OTHER_PRICE_AMOUNT));
    }

    @Test
    public void whenApplyPercentDiscount_thenDiscountIsAppliedToReturnedPrice() {
        // when
        Price price = Price.of(PRICE_AMOUNT);
        Price result = price.applyPercentDiscount(PERCENT_DISCOUNT);

        // then
        Price expectedPrice = Price.of(PRICE_AMOUNT * ((100 - PERCENT_DISCOUNT) / 100));
        assertThat(result).isEquivalentAccordingToCompareTo(expectedPrice);
    }

    @Test
    public void whenApplyPercentDiscountWithPercentLowerThanZero_thenThrowIllegalArgumentException() {
        // then
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            // when
            price.applyPercentDiscount(LOWER_THAN_ZERO_PERCENT_DISCOUNT);
        });
    }

    @Test
    public void whenApplyPercentDiscountWithPercentOverOneHundred_thenThrowIllegalArgumentException() {
        // then
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            // when
            price.applyPercentDiscount(OVER_ONE_HUNDRED_PERCENT_DISCOUNT);
        });
    }

    @Test
    void whenMultiplyBy_thenPriceIsMultipliedByFactor() {
        //when
        Price expectedPrice = Price.of(price.getValue().floatValue() * A_FACTOR);
        Price newPrice = price.multiplyBy(A_FACTOR);

        // then
        assertThat(newPrice).isEquivalentAccordingToCompareTo(expectedPrice);
    }
}
