package ca.ulaval.glo4002.domain.oxygen;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static com.google.common.truth.Truth.assertThat;

class OxygenHistoryTest {

    private static final LocalDate A_DATE = LocalDate.of(1, 2, 3);
    private static final LocalDate A_DATE_BEFORE = LocalDate.of(1, 2, 1);
    private static final LocalDate A_DATE_AFTER = LocalDate.of(11, 2, 3);
    private static final int A_QUANTITY_OF_USED_RESOURCES = 2;

    private OxygenHistory oxygenHistory;

    @BeforeEach
    public void setUp() {
        oxygenHistory = new OxygenHistory(A_DATE);
    }

    @Test
    public void whenRecordCandlesUsed_thenAddTheAmountOfCandlesUsedToNumberOfCandlesUsed() {
        //when
        oxygenHistory.recordCandlesUsed(A_QUANTITY_OF_USED_RESOURCES);
        //then
        assertThat(oxygenHistory.getNumberOfCandlesUsed()).isEqualTo(A_QUANTITY_OF_USED_RESOURCES);
    }

    @Test
    public void whenRecordOxygenBought_thenAddTheAmountOfBoughtOxygenTanksToNumberOfTanksBought() {
        //when
        oxygenHistory.recordOxygenTanksBought(A_QUANTITY_OF_USED_RESOURCES);
        //then
        assertThat(oxygenHistory.getNumberOfTanksBought()).isEqualTo(A_QUANTITY_OF_USED_RESOURCES);
    }

    @Test
    public void whenRecordOxygenMade_thenAddTheAmountOfMadeOxygenTanksToNumberOfTanksMade() {
        //when
        oxygenHistory.recordOxygenTanksMade(A_QUANTITY_OF_USED_RESOURCES);
        //then
        assertThat(oxygenHistory.getNumberOfTanksMade()).isEqualTo(A_QUANTITY_OF_USED_RESOURCES);
    }

    @Test
    public void whenRecordWaterUsed_thenAddTheAmountOfLitersOfWatersUsedToLitersOfWaterUsed() {
        //when
        oxygenHistory.recordWaterUsed(A_QUANTITY_OF_USED_RESOURCES);
        //then
        assertThat(oxygenHistory.getLitersOfWaterUsed()).isEqualTo(A_QUANTITY_OF_USED_RESOURCES);
    }

    @Test
    public void whenCompareToOtherHistoryAndDatesAreEqual_thenResultIsZero() {
        // when
        OxygenHistory otherOxygenHistory = new OxygenHistory(A_DATE);

        int result = oxygenHistory.compareTo(otherOxygenHistory);

        // then
        assertThat(result).isEqualTo(0);
    }

    @Test
    public void whenCompareToOtherHistoryAndOtherDateIsBefore_thenResultIsGreaterThanZero() {
        // when
        OxygenHistory otherOxygenHistory = new OxygenHistory(A_DATE_BEFORE);

        int result = oxygenHistory.compareTo(otherOxygenHistory);

        // then
        assertThat(result).isGreaterThan(0);
    }

    @Test
    public void whenCompareToOtherHistoryAndOtherDateIsAfter_thenResultIsLessThanZero() {
        // when
        OxygenHistory otherOxygenHistory = new OxygenHistory(A_DATE_AFTER);

        int result = oxygenHistory.compareTo(otherOxygenHistory);

        // then
        assertThat(result).isLessThan(0);
    }
}
