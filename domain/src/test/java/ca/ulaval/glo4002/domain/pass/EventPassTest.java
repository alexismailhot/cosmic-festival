package ca.ulaval.glo4002.domain.pass;

import ca.ulaval.glo4002.domain.price.Price;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static com.google.common.truth.Truth.assertThat;

class EventPassTest {

    private static final LocalDate AN_EVENT_DATE = LocalDate.of(1, 2, 3);
    private static final PassCategory A_PASS_CATEGORY = PassCategory.NEBULA;
    private static final PassNumber A_PASS_NUMBER = new PassNumber(0L);
    private static final Price A_PRICE = Price.of(100);

    @Test
    public void whenIsValidOn_ReturnTrueForAllFestivalDates() {
        // when
        EventPass eventPass = new EventPass(A_PASS_NUMBER, A_PASS_CATEGORY, A_PRICE);
        boolean isValidOn = eventPass.isValidOn(AN_EVENT_DATE);

        // then
        assertThat(isValidOn).isTrue();
    }
}
