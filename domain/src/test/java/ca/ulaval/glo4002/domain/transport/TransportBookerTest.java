package ca.ulaval.glo4002.domain.transport;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import ca.ulaval.glo4002.domain.profit.repository.TransactionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ca.ulaval.glo4002.domain.event.FestivalDates;
import ca.ulaval.glo4002.domain.pass.PassNumber;
import ca.ulaval.glo4002.domain.profit.Transaction;
import ca.ulaval.glo4002.domain.profit.factory.TransactionFactory;
import ca.ulaval.glo4002.domain.transport.factory.ShuttleFactory;
import ca.ulaval.glo4002.domain.transport.repository.ShuttleRepository;


@ExtendWith(MockitoExtension.class)
class TransportBookerTest {

    private static final LocalDate FESTIVAL_START_DATE = LocalDate.of(2050, 7, 17);
    private static final LocalDate FESTIVAL_END_DATE = LocalDate.of(2050, 7, 24);
    private static final ShuttleDirection DEPARTURE_SHUTTLE_DIRECTION = ShuttleDirection.DEPARTURE;
    private static final ShuttleDirection ARRIVAL_SHUTTLE_DIRECTION = ShuttleDirection.ARRIVAL;
    private static final ShuttleType A_SHUTTLE_TYPE = ShuttleType.MILLENIUM_FALCON;
    private static final PassNumber A_PASS_NUMBER = new PassNumber(42);
    private static final List<PassengerNumber> A_GROUP_OF_20_PEOPLE = Collections.nCopies(20, A_PASS_NUMBER);

    private Shuttle aFestivalStartDateDepartureShuttle;
    private Shuttle aFestivalStartDateArrivalShuttle;
    private Shuttle aFestivalEndDateDepartureShuttle;
    private Shuttle aFestivalEndDateArrivalShuttle;
    private TransportBooker transportBooker;

    @Mock
    private ShuttleFactory shuttleFactory;

    @Mock
    private ShuttleRepository shuttleRepository;

    @Mock
    private FestivalDates festivalDates;

    @Mock
    private TransactionRepository transactionRepository;

    @BeforeEach
    public void setUp() {
        transportBooker = new TransportBooker(festivalDates, shuttleRepository, shuttleFactory, transactionRepository, new TransactionFactory());
        aFestivalStartDateDepartureShuttle = new Shuttle(A_SHUTTLE_TYPE, FESTIVAL_START_DATE, DEPARTURE_SHUTTLE_DIRECTION);
        aFestivalStartDateArrivalShuttle = new Shuttle(A_SHUTTLE_TYPE, FESTIVAL_START_DATE, ARRIVAL_SHUTTLE_DIRECTION);
        aFestivalEndDateDepartureShuttle = new Shuttle(A_SHUTTLE_TYPE, FESTIVAL_END_DATE, DEPARTURE_SHUTTLE_DIRECTION);
        aFestivalEndDateArrivalShuttle = new Shuttle(A_SHUTTLE_TYPE, FESTIVAL_END_DATE, ARRIVAL_SHUTTLE_DIRECTION);
    }

    @Test
    public void whenBookEventTransport_thenPassengerIsAddedToStartDateDepartureShuttleAndEndDateArrivalShuttle() {
        // when
        givenFestivalDates();
        givenStartDateDepartureShuttleAndEndDateArrivalShuttleAreEmpty();
        transportBooker.bookEventTransport(A_PASS_NUMBER, A_SHUTTLE_TYPE);

        // then
        verify(shuttleRepository).upsertShuttle(aFestivalStartDateDepartureShuttle);
        verify(shuttleRepository).upsertShuttle(aFestivalEndDateArrivalShuttle);
    }

    @Test
    public void whenBookDailyEventTransport_thenPassengerIsAddedToDepartureAndArrivalShuttlesInTheSameDay() {
        // when
        givenStartDateDepartureShuttleAndStartDateArrivalShuttleAreEmpty();
        transportBooker.bookDailyEventTransport(FESTIVAL_START_DATE, A_PASS_NUMBER, A_SHUTTLE_TYPE);

        // then
        verify(shuttleRepository).upsertShuttle(aFestivalStartDateDepartureShuttle);
        verify(shuttleRepository).upsertShuttle(aFestivalStartDateArrivalShuttle);
    }

    @Test
    public void whenBookDailyEventTransportForAGroup_thenGroupPassengersAreAddedToTheSameDepartureAndArrivalShuttlesInTheSameDay() {
        // when
        givenStartDateDepartureShuttleAndStartDateArrivalShuttleAreEmpty();
        transportBooker.bookDailyEventTransportForAGroup(FESTIVAL_START_DATE, A_GROUP_OF_20_PEOPLE, A_SHUTTLE_TYPE);

        //then
        verify(shuttleRepository).upsertShuttle(aFestivalStartDateDepartureShuttle);
        verify(shuttleRepository).upsertShuttle(aFestivalStartDateArrivalShuttle);
    }

    @Test
    public void givenShuttlesWithAvailableSpace_whenBookDailyEventPassTransport_thenNoCostIsAddedToExpenses() {
        // given
        givenStartDateDepartureShuttleAndStartDateArrivalShuttleAreEmpty();

        // when
        transportBooker.bookDailyEventTransport(FESTIVAL_START_DATE, A_PASS_NUMBER, A_SHUTTLE_TYPE);

        // then
        verify(transactionRepository, never()).add(any(Transaction.class));
    }

    @Test
    public void givenFullShuttles_whenBookDailyEventPassTransport_thenCreatedShuttlesCostIsAddedToExpenses() {
        // given
        givenStartDateDepartureShuttleAndStartDateArrivalShuttleAreFull();

        // when
        when(shuttleFactory.create(A_SHUTTLE_TYPE, FESTIVAL_START_DATE, DEPARTURE_SHUTTLE_DIRECTION))
            .thenReturn(createShuttle(FESTIVAL_START_DATE, DEPARTURE_SHUTTLE_DIRECTION));
        when(shuttleFactory.create(A_SHUTTLE_TYPE, FESTIVAL_START_DATE, ARRIVAL_SHUTTLE_DIRECTION))
            .thenReturn(createShuttle(FESTIVAL_START_DATE, ARRIVAL_SHUTTLE_DIRECTION));
        transportBooker.bookDailyEventTransport(FESTIVAL_START_DATE, A_PASS_NUMBER, A_SHUTTLE_TYPE);

        // then
        verify(transactionRepository, times(2)).add(any(Transaction.class));
    }

    @Test
    public void givenShuttlesWithAvailableSpace_whenBookEventPassTransport_thenNoCostIsAddedToExpenses() {
        // given
        givenStartDateDepartureShuttleAndEndDateArrivalShuttleAreEmpty();

        // when
        givenFestivalDates();
        transportBooker.bookEventTransport(A_PASS_NUMBER, A_SHUTTLE_TYPE);

        // then
        verify(transactionRepository, never()).add(any(Transaction.class));
    }

    @Test
    public void givenFullShuttles_whenBookEventPassTransport_thenCreatedShuttlesCostIsAddedToExpenses() {
        // given
        givenStartDateDepartureShuttleAndEndDateArrivalShuttleAreFull();

        // when
        givenFestivalDates();
        when(shuttleFactory.create(A_SHUTTLE_TYPE, FESTIVAL_START_DATE, DEPARTURE_SHUTTLE_DIRECTION))
            .thenReturn(createShuttle(FESTIVAL_START_DATE, DEPARTURE_SHUTTLE_DIRECTION));
        when(shuttleFactory.create(A_SHUTTLE_TYPE, FESTIVAL_END_DATE, ARRIVAL_SHUTTLE_DIRECTION))
            .thenReturn(createShuttle(FESTIVAL_END_DATE, ARRIVAL_SHUTTLE_DIRECTION));
        transportBooker.bookEventTransport(A_PASS_NUMBER, A_SHUTTLE_TYPE);

        // then
        verify(transactionRepository, times(2)).add(any(Transaction.class));
    }

    @Test
    public void givenEnoughSpaceInShuttlesForGroup_whenBookDailyEventTransportForAGroup_thenNoCostIsAddedToExpenses() {
        // given
        givenStartDateDepartureShuttleAndStartDateArrivalShuttleAreEmpty();

        // when
        transportBooker.bookDailyEventTransportForAGroup(FESTIVAL_START_DATE, A_GROUP_OF_20_PEOPLE, A_SHUTTLE_TYPE);

        // then
        verify(transactionRepository, never()).add(any(Transaction.class));
    }

    @Test
    public void givenNotEnoughSpaceInShuttlesForGroup_whenBookDailyEventTransportForAGroup_thenCreatedShuttlesCostIsAddedToExpenses() {
        // given
        givenStartDateDepartureShuttleAndStartDateArrivalShuttlesWithoutEnoughSpaceForGroup();

        // when
        when(shuttleFactory.create(A_SHUTTLE_TYPE, FESTIVAL_START_DATE, DEPARTURE_SHUTTLE_DIRECTION))
            .thenReturn(createShuttle(FESTIVAL_START_DATE, DEPARTURE_SHUTTLE_DIRECTION));
        when(shuttleFactory.create(A_SHUTTLE_TYPE, FESTIVAL_START_DATE, ARRIVAL_SHUTTLE_DIRECTION))
            .thenReturn(createShuttle(FESTIVAL_START_DATE, ARRIVAL_SHUTTLE_DIRECTION));
        transportBooker.bookDailyEventTransportForAGroup(FESTIVAL_START_DATE, A_GROUP_OF_20_PEOPLE, A_SHUTTLE_TYPE);

        // then
        verify(transactionRepository, times(2)).add(any(Transaction.class));
    }

    private void givenFestivalDates() {
        when(festivalDates.getFestivalStartDate()).thenReturn(FESTIVAL_START_DATE);
        when(festivalDates.getFestivalEndDate()).thenReturn(FESTIVAL_END_DATE);
    }

    private void givenStartDateDepartureShuttleAndStartDateArrivalShuttleAreEmpty() {
        when(shuttleRepository.findByEventDateDirectionAndShuttleType(FESTIVAL_START_DATE, DEPARTURE_SHUTTLE_DIRECTION, A_SHUTTLE_TYPE))
            .thenReturn(Collections.singletonList(aFestivalStartDateDepartureShuttle));
        when(shuttleRepository.findByEventDateDirectionAndShuttleType(FESTIVAL_START_DATE, ARRIVAL_SHUTTLE_DIRECTION, A_SHUTTLE_TYPE))
            .thenReturn(Collections.singletonList(aFestivalStartDateArrivalShuttle));
    }

    private void givenStartDateDepartureShuttleAndStartDateArrivalShuttleAreFull() {
        when(shuttleRepository.findByEventDateDirectionAndShuttleType(FESTIVAL_START_DATE, DEPARTURE_SHUTTLE_DIRECTION, A_SHUTTLE_TYPE))
            .thenReturn(Collections.singletonList(aFestivalStartDateDepartureShuttle));
        when(shuttleRepository.findByEventDateDirectionAndShuttleType(FESTIVAL_START_DATE, ARRIVAL_SHUTTLE_DIRECTION, A_SHUTTLE_TYPE))
            .thenReturn(Collections.singletonList(aFestivalStartDateArrivalShuttle));
        transportBooker.bookDailyEventTransportForAGroup(FESTIVAL_START_DATE, A_GROUP_OF_20_PEOPLE, A_SHUTTLE_TYPE);
    }

    private void givenStartDateDepartureShuttleAndEndDateArrivalShuttleAreEmpty() {
        when(shuttleRepository.findByEventDateDirectionAndShuttleType(FESTIVAL_START_DATE, DEPARTURE_SHUTTLE_DIRECTION, A_SHUTTLE_TYPE))
            .thenReturn(Collections.singletonList(aFestivalStartDateDepartureShuttle));
        when(shuttleRepository.findByEventDateDirectionAndShuttleType(FESTIVAL_END_DATE, ARRIVAL_SHUTTLE_DIRECTION, A_SHUTTLE_TYPE))
            .thenReturn(Collections.singletonList(aFestivalEndDateArrivalShuttle));
    }

    private void givenStartDateDepartureShuttleAndEndDateArrivalShuttleAreFull() {
        when(shuttleRepository.findByEventDateDirectionAndShuttleType(FESTIVAL_START_DATE, DEPARTURE_SHUTTLE_DIRECTION, A_SHUTTLE_TYPE))
            .thenReturn(Collections.singletonList(aFestivalStartDateDepartureShuttle));
        when(shuttleRepository.findByEventDateDirectionAndShuttleType(FESTIVAL_START_DATE, ARRIVAL_SHUTTLE_DIRECTION, A_SHUTTLE_TYPE))
            .thenReturn(Collections.singletonList(aFestivalStartDateArrivalShuttle));

        when(shuttleRepository.findByEventDateDirectionAndShuttleType(FESTIVAL_END_DATE, DEPARTURE_SHUTTLE_DIRECTION, A_SHUTTLE_TYPE))
            .thenReturn(Collections.singletonList(aFestivalEndDateDepartureShuttle));
        when(shuttleRepository.findByEventDateDirectionAndShuttleType(FESTIVAL_END_DATE, ARRIVAL_SHUTTLE_DIRECTION, A_SHUTTLE_TYPE))
            .thenReturn(Collections.singletonList(aFestivalEndDateArrivalShuttle));

        transportBooker.bookDailyEventTransportForAGroup(FESTIVAL_START_DATE, A_GROUP_OF_20_PEOPLE, A_SHUTTLE_TYPE);
        transportBooker.bookDailyEventTransportForAGroup(FESTIVAL_END_DATE, A_GROUP_OF_20_PEOPLE, A_SHUTTLE_TYPE);
    }

    private void givenStartDateDepartureShuttleAndStartDateArrivalShuttlesWithoutEnoughSpaceForGroup() {
        when(shuttleRepository.findByEventDateDirectionAndShuttleType(FESTIVAL_START_DATE, DEPARTURE_SHUTTLE_DIRECTION, A_SHUTTLE_TYPE))
            .thenReturn(Collections.singletonList(aFestivalStartDateDepartureShuttle));
        when(shuttleRepository.findByEventDateDirectionAndShuttleType(FESTIVAL_START_DATE, ARRIVAL_SHUTTLE_DIRECTION, A_SHUTTLE_TYPE))
            .thenReturn(Collections.singletonList(aFestivalStartDateDepartureShuttle));
        transportBooker.bookDailyEventTransport(FESTIVAL_START_DATE, A_PASS_NUMBER, A_SHUTTLE_TYPE);
    }

    private Shuttle createShuttle(final LocalDate date, final ShuttleDirection shuttleDirection) {
        return new Shuttle(A_SHUTTLE_TYPE, date, shuttleDirection);
    }
}
