package ca.ulaval.glo4002.domain.oxygen.method;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;
import ca.ulaval.glo4002.domain.oxygen.OxygenHistory;
import ca.ulaval.glo4002.domain.oxygen.OxygenInventory;
import ca.ulaval.glo4002.domain.oxygen.factory.OxygenTankFactory;
import ca.ulaval.glo4002.domain.price.Price;

@ExtendWith(MockitoExtension.class)
class ElectrolysisOxygenSupplyMethodTest {

    private static final int A_FABRICATION_TIME = 6;
    private static final int A_NUMBER_OF_LITERS_OF_WATER_USED = 4;
    private static final int A_NUMBER_OF_TANKS_PRODUCED = 9;
    private static final int A_NUMBER_OF_TANKS_NEEDED = 8;
    private static final Price A_FABRICATION_COST = Price.of(1500);

    private ElectrolysisOxygenSupplyMethod oxygenSupplyMethod;

    @Mock
    private OxygenHistory oxygenHistory;

    @Mock
    private OxygenInventory oxygenInventory;

    @BeforeEach
    public void setUp() {
        oxygenSupplyMethod = new ElectrolysisOxygenSupplyMethod(new OxygenTankFactory(),
                                                                OxygenGrade.GRADE_E,
                                                                A_NUMBER_OF_TANKS_PRODUCED,
                                                                A_FABRICATION_TIME,
                                                                A_NUMBER_OF_LITERS_OF_WATER_USED,
                                                                A_FABRICATION_COST);
    }

    @Test
    public void whenSupplyNeededOxygen_thenOxygenHistoryIsUpdatedWithUsedLitersOfWater() {
        // when
        oxygenSupplyMethod.supplyNeededOxygen(oxygenHistory, oxygenInventory, A_NUMBER_OF_TANKS_NEEDED);

        // then
        verify(oxygenHistory).recordWaterUsed(A_NUMBER_OF_LITERS_OF_WATER_USED);
    }

    @Test
    public void whenSupplyNeededOxygen_thenOxygenInventoryIsUpdatedWithProducedTanks() {
        // when
        oxygenSupplyMethod.supplyNeededOxygen(oxygenHistory, oxygenInventory, A_NUMBER_OF_TANKS_NEEDED);

        // then
        verify(oxygenInventory).addOxygenTanks(argThat(list -> list.size() == A_NUMBER_OF_TANKS_PRODUCED));
    }
}
