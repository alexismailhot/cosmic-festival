package ca.ulaval.glo4002.domain.oxygen.factory;

import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;
import ca.ulaval.glo4002.domain.oxygen.OxygenTank;

public class OxygenTankFactory {

    public OxygenTank create(OxygenGrade oxygenGrade) {
        return new OxygenTank(oxygenGrade);
    }
}
