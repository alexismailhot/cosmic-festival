package ca.ulaval.glo4002.domain.oxygen;

import ca.ulaval.glo4002.domain.price.Price;

public class OxygenSupplyReport {
    private final Price supplyCost;
    private final int numberOfTanksProduced;

    public OxygenSupplyReport(Price supplyCost, int numberOfTanksProduced) {
        this.supplyCost = supplyCost;
        this.numberOfTanksProduced = numberOfTanksProduced;
    }

    public static OxygenSupplyReport createNullOxygenSupplyReport() {
        return new OxygenSupplyReport(Price.zero(), 0);
    }

    public Price getSupplyCost() {
        return supplyCost;
    }

    public int getNumberOfTanksProduced() {
        return numberOfTanksProduced;
    }
}
