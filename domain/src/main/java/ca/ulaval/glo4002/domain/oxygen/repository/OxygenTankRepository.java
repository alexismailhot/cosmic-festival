package ca.ulaval.glo4002.domain.oxygen.repository;

import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;
import ca.ulaval.glo4002.domain.oxygen.OxygenTank;

import java.util.List;

public interface OxygenTankRepository {

    void addOxygenTanks(List<OxygenTank> oxygenTanks);

    List<OxygenTank> getAllTanks();

    List<OxygenTank> findByGrade(OxygenGrade oxygenGrade);

    List<OxygenTank> findAvailableTanksByGrade(OxygenGrade oxygenGrade);
    
    void update(OxygenTank oxygenTank);
}