package ca.ulaval.glo4002.domain.artist;

import ca.ulaval.glo4002.domain.transport.PassengerNumber;

public class ArtistNumber implements PassengerNumber {

    private final int id;

    public ArtistNumber(int id) {
        this.id = id;
    }

    @Override
    public long getValue() {
        return id;
    }
}
