package ca.ulaval.glo4002.domain.artist;

import ca.ulaval.glo4002.domain.price.Price;

public class Artist {

    private final ArtistNumber number;
    private final String name;
    private final Price price;
    private final int popularity;
    private final int numberGroupMembers;

    public Artist(ArtistNumber number, String name, Price price, int popularity, int numberGroupMembers) {
        this.number = number;
        this.name = name;
        this.price = price;
        this.popularity = popularity;
        this.numberGroupMembers = numberGroupMembers;
    }

    public int getNumberGroupMembers() {
        return numberGroupMembers;
    }

    public Price getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public int getPopularity() {
        return popularity;
    }

	public ArtistNumber getNumber() {
		return number;
	}

	public boolean hasName(String name) {
        return this.name.equals(name);
    }

    public int compareToBasedOnPrice(Artist otherArtist) {
        return price.compareTo(otherArtist.price);
    }

    public int compareToBasedOnPopularity(Artist otherArtist) {
        return Integer.compare(popularity, otherArtist.popularity);
    }
}
