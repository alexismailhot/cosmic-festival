package ca.ulaval.glo4002.domain.pass;

import ca.ulaval.glo4002.domain.discount.PassDiscount;
import ca.ulaval.glo4002.domain.price.Price;

import java.time.LocalDate;
import java.util.List;

public class EventPass {

    private final PassNumber passNumber;
    private final PassCategory passCategory;
    private final Price passPrice;

    public EventPass(PassNumber passNumber, PassCategory passCategory, Price passPrice) {
        this.passNumber = passNumber;
        this.passCategory = passCategory;
        this.passPrice = passPrice;
    }

    public PassNumber getPassNumber() {
        return passNumber;
    }

    public PassCategory getPassCategory() {
        return passCategory;
    }

    public Price calculatePrice(List<PassDiscount> passDiscounts) {
        Price price = passPrice;
        for (PassDiscount discount : passDiscounts) {
            price = discount.applyDiscountIfApplicable(price, passCategory);
        }
        return price;
    }

    public boolean isValidOn(LocalDate eventDate) {
        return true;
    }
}
