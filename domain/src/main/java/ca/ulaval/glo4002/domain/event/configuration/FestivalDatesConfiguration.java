package ca.ulaval.glo4002.domain.event.configuration;

import java.time.LocalDate;

public interface FestivalDatesConfiguration {

    LocalDate getFestivalStartDate();

    LocalDate getFestivalEndDate();

    LocalDate getFestivalProgramUnveilingDate();

    void updateFestivalStartDate(LocalDate localDate);

    void updateFestivalEndDate(LocalDate localDate);
}
