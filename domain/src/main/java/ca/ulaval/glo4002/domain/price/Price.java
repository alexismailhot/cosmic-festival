package ca.ulaval.glo4002.domain.price;

import java.math.BigDecimal;

public class Price implements Comparable<Price> {

    private final BigDecimal amount;

    private Price(BigDecimal amount) {
        this.amount = amount;
    }

    public static Price of(double amount) {
        return new Price(BigDecimal.valueOf(amount));
    }

    public static Price zero() {
        return new Price(BigDecimal.valueOf(0));
    }

    public BigDecimal getValue() {
        return amount;
    }

    public Price add(Price price) {
        return new Price(this.amount.add(price.amount));
    }

    public Price subtract(Price price) {
        return new Price(this.amount.subtract(price.amount));
    }

    public Price applyPercentDiscount(double percentDiscount) {
        if (percentDiscount < 0 || percentDiscount > 100) {
            throw new IllegalArgumentException("percentDiscount should be between 0 and 100");
        }
        return this.multiplyBy((100 - percentDiscount) / 100);
    }

    public Price multiplyBy(double factor) {
        return new Price(amount.multiply(BigDecimal.valueOf(factor)));
    }

    @Override
    public int compareTo(Price price) {
        return this.amount.compareTo(price.amount);
    }

    public Price divideBy(int denominator) {
        return new Price(amount.divide(BigDecimal.valueOf(denominator), BigDecimal.ROUND_HALF_UP));
    }
}
