package ca.ulaval.glo4002.domain.artist;

import ca.ulaval.glo4002.domain.artist.configuration.ArtistShuttleTypeConfiguration;
import ca.ulaval.glo4002.domain.transport.ShuttleType;

public class ArtistShuttleType {

    private final ArtistShuttleTypeConfiguration shuttleTypeConfiguration;

    public ArtistShuttleType(ArtistShuttleTypeConfiguration shuttleTypeConfiguration) {
        this.shuttleTypeConfiguration = shuttleTypeConfiguration;
    }

    public ShuttleType getShuttleTypeFor(int numberOfGroupMembers) {
        ShuttleType shuttleTypeForSoloArtists = shuttleTypeConfiguration.getShuttleTypeForSoloArtists();
        ShuttleType shuttleTypeForGroupArtists = shuttleTypeConfiguration.getShuttleTypeForGroupArtists();
        return numberOfGroupMembers > 1 ? shuttleTypeForGroupArtists : shuttleTypeForSoloArtists;
    }
}
