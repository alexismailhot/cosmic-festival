package ca.ulaval.glo4002.domain.discount.configuration;

import ca.ulaval.glo4002.domain.discount.OrderDiscount;
import ca.ulaval.glo4002.domain.discount.PassDiscount;

import java.util.List;

public interface DiscountsConfiguration {

    List<OrderDiscount> getOrderDiscounts();

    List<PassDiscount> getPassDiscounts();
}
