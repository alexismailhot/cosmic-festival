package ca.ulaval.glo4002.domain.discount;

import ca.ulaval.glo4002.domain.pass.PassCategory;
import ca.ulaval.glo4002.domain.price.Price;

import java.util.List;

public interface OrderDiscount {

    Price applyDiscount(Price initialPrice);

    boolean isDiscountApplicable(List<PassCategory> passCategories);
}
