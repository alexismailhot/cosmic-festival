package ca.ulaval.glo4002.domain.pass;

import ca.ulaval.glo4002.domain.transport.PassengerNumber;

import java.util.Objects;

public class PassNumber implements PassengerNumber {

    private final long value;

    public PassNumber(long value) {
        this.value = value;
    }

    @Override
    public long getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PassNumber that = (PassNumber) o;
        return value == that.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
