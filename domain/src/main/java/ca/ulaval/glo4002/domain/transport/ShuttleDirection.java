package ca.ulaval.glo4002.domain.transport;

public enum ShuttleDirection {
    DEPARTURE,
    ARRIVAL
}
