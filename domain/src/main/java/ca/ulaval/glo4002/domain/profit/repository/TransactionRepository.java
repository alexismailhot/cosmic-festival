package ca.ulaval.glo4002.domain.profit.repository;

import ca.ulaval.glo4002.domain.profit.Transaction;

import java.util.List;

public interface TransactionRepository {

    void add(Transaction transaction);

    List<Transaction> getTransactions();
}
