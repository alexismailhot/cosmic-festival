package ca.ulaval.glo4002.domain.discount;

import ca.ulaval.glo4002.domain.pass.PassCategory;
import ca.ulaval.glo4002.domain.price.Price;

import java.util.List;

public class MultiplePassesOfSameCategoryOrderDiscount implements OrderDiscount {

    private final PassCategory applicableCategory;
    private final int numberOfPassesRequired;
    private final double percentDiscount;

    public MultiplePassesOfSameCategoryOrderDiscount(PassCategory applicableCategory, int numberOfPassesRequired, double percentDiscount) {
        this.applicableCategory = applicableCategory;
        this.numberOfPassesRequired = numberOfPassesRequired;
        this.percentDiscount = percentDiscount;
    }

    @Override
    public Price applyDiscount(Price initialPrice) {
        return initialPrice.applyPercentDiscount(percentDiscount);
    }

    @Override
    public boolean isDiscountApplicable(List<PassCategory> passCategories) {
        return passCategories.stream().filter(this::isApplicableCategory).count() >= numberOfPassesRequired;
    }

    private boolean isApplicableCategory(PassCategory passCategory) {
        return passCategory.equals(applicableCategory);
    }
}
