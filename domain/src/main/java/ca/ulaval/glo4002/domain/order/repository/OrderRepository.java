package ca.ulaval.glo4002.domain.order.repository;

import ca.ulaval.glo4002.domain.order.Order;
import ca.ulaval.glo4002.domain.order.OrderNumber;

import java.util.Optional;

public interface OrderRepository {

    Optional<Order> findByOrderNumber(OrderNumber orderNumber);

    void add(Order order);
    
    int getCount();
}
