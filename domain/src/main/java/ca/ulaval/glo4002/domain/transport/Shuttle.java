package ca.ulaval.glo4002.domain.transport;

import ca.ulaval.glo4002.domain.transport.exception.CannotAddGroupOfPassengersToShuttleException;
import ca.ulaval.glo4002.domain.transport.exception.CannotAddPassengerToShuttleException;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Shuttle {

    private final ShuttleType type;
    private final LocalDate eventDate;
    private final ShuttleDirection direction;
    private final List<PassengerNumber> passengers = new ArrayList<>();

    public Shuttle(ShuttleType type, LocalDate eventDate, ShuttleDirection direction) {
        this.type = type;
        this.eventDate = eventDate;
        this.direction = direction;
    }

    public ShuttleType getShuttleType() {
        return type;
    }

    public LocalDate getEventDate() {
        return eventDate;
    }

    public ShuttleDirection getShuttleDirection() {
        return direction;
    }

    public List<PassengerNumber> getPassengers() {
        return Collections.unmodifiableList(passengers);
    }

    public void addPassenger(PassengerNumber passengerNumber) {
        if (isFull()) {
            throw new CannotAddPassengerToShuttleException();
        }
        passengers.add(passengerNumber);
    }

    public void addGroupOfPassengers(List<PassengerNumber> passengerNumbers) {
        if (!hasEnoughPlacesFor(passengerNumbers.size())) {
            throw new CannotAddGroupOfPassengersToShuttleException();
        }
        passengers.addAll(passengerNumbers);
    }

    public boolean isFull() {
        return type.isPassengerCapacityReached(passengers.size());
    }

    public boolean hasEnoughPlacesFor(int numberOfPassengers) {
        return type.calculateNumberOfPlacesRemaining(passengers.size()) >= numberOfPassengers;
    }

    public boolean isSameType(ShuttleType shuttleType) {
        return type.equals(shuttleType);
    }

    public boolean isSameDirection(ShuttleDirection direction) {
        return this.direction.equals(direction);
    }
}
