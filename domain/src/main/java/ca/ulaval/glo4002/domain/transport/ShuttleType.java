package ca.ulaval.glo4002.domain.transport;

import ca.ulaval.glo4002.domain.pass.PassCategory;
import ca.ulaval.glo4002.domain.price.Price;

import java.util.Arrays;

public enum ShuttleType {

    ET_SPACESHIP(PassCategory.SUPERNOVA, 1, "ET Spaceship", Price.of(100000)),
    MILLENIUM_FALCON(PassCategory.SUPERGIANT, 20, "Millenium Falcon", Price.of(65000)),
    SPACEX(PassCategory.NEBULA, 30, "SpaceX", Price.of(30000));

    private final PassCategory category;
    private final int passengerCapacity;
    private final String shuttleName;
    private final Price shuttlePrice;

    ShuttleType(PassCategory category, int passengerCapacity, String shuttleName, Price shuttlePrice) {
        this.category = category;
        this.passengerCapacity = passengerCapacity;
        this.shuttleName = shuttleName;
        this.shuttlePrice = shuttlePrice;
    }

    public static ShuttleType fromPassCategory(PassCategory passCategory) {
        return Arrays.stream(ShuttleType.values())
                .filter(shuttleType -> shuttleType.category.equals(passCategory))
                .findFirst().orElseThrow(IllegalArgumentException::new);
    }

    public String getName() {
        return shuttleName;
    }

    public Price getShuttlePrice() {
        return shuttlePrice;
    }

    public boolean isPassengerCapacityReached(int numberOfPassengers) {
        return numberOfPassengers >= passengerCapacity;
    }

    public int calculateNumberOfPlacesRemaining(int numberOfPassengers) {
        return passengerCapacity - numberOfPassengers;
    }
}
