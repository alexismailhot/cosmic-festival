package ca.ulaval.glo4002.domain.pass.factory;

import ca.ulaval.glo4002.domain.pass.DailyEventPass;
import ca.ulaval.glo4002.domain.pass.EventPass;
import ca.ulaval.glo4002.domain.pass.PassCategory;
import ca.ulaval.glo4002.domain.pass.PassPrices;

import java.time.LocalDate;

public class EventPassFactory {

    private final PassNumberFactory passNumberGenerator;
    private final PassPrices passPrices;

    public EventPassFactory(PassNumberFactory passNumberGenerator, PassPrices passPrices) {
        this.passNumberGenerator = passNumberGenerator;
        this.passPrices = passPrices;
    }

    public EventPass createEventPass(PassCategory passCategory) {
        return new EventPass(passNumberGenerator.generate(), passCategory, passPrices.getEventPassPrice(passCategory));
    }

    public DailyEventPass createDailyEventPass(PassCategory passCategory, LocalDate eventDate) {
        return new DailyEventPass(passNumberGenerator.generate(), passCategory, eventDate,
                passPrices.getDailyEventPassPrice(passCategory));
    }
}
