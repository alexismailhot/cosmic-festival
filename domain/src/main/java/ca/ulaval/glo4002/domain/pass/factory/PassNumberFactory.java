package ca.ulaval.glo4002.domain.pass.factory;

import ca.ulaval.glo4002.domain.pass.PassNumber;
import ca.ulaval.glo4002.domain.pass.repository.EventPassRepository;

public class PassNumberFactory {

    private final EventPassRepository eventPassRepository;

    public PassNumberFactory(EventPassRepository eventPassRepository) {
        this.eventPassRepository = eventPassRepository;
    }

    public PassNumber generate() {
        return new PassNumber(eventPassRepository.getCount());
    }
}
