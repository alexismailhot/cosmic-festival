package ca.ulaval.glo4002.domain.oxygen.method;

import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;
import ca.ulaval.glo4002.domain.oxygen.OxygenHistory;
import ca.ulaval.glo4002.domain.oxygen.OxygenInventory;
import ca.ulaval.glo4002.domain.oxygen.OxygenSupplyReport;
import ca.ulaval.glo4002.domain.oxygen.factory.OxygenTankFactory;
import ca.ulaval.glo4002.domain.price.Price;

public class PremadeOxygenSupplyMethod extends OxygenSupplyMethod {

    private final int numberOfTanksBoughtPerBatch;

    public PremadeOxygenSupplyMethod(OxygenTankFactory oxygenTankFactory,
                                     OxygenGrade oxygenGrade,
                                     int numberOfTanksProduced,
                                     int fabricationDays,
                                     int numberOfTanksBoughtPerBatch,
                                     Price purchaseCost) {
        super(oxygenTankFactory, oxygenGrade, numberOfTanksProduced, fabricationDays, purchaseCost);
        this.numberOfTanksBoughtPerBatch = numberOfTanksBoughtPerBatch;
    }

    @Override
    public OxygenSupplyReport supplyNeededOxygen(OxygenHistory oxygenHistory, OxygenInventory oxygenInventory, int oxygenTanksNeeded) {
        int numberOfPurchases = (int) Math.ceil((double) oxygenTanksNeeded / numberOfTanksBoughtPerBatch);

        int numberOfTanksBought = numberOfTanksBoughtPerBatch * numberOfPurchases;
        oxygenHistory.recordOxygenTanksBought(numberOfTanksBought);

        generateOxygenTanks(oxygenInventory, numberOfTanksBought);

        Price oxygenSuppliedExpense = methodCost.multiplyBy(numberOfPurchases);

        return new OxygenSupplyReport(oxygenSuppliedExpense, numberOfTanksProducedPerBatch);
    }
}
