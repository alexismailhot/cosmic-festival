package ca.ulaval.glo4002.domain.pass.configuration;

import ca.ulaval.glo4002.domain.pass.PassCategory;
import ca.ulaval.glo4002.domain.price.Price;

import java.util.EnumMap;

public interface PassPricesConfiguration {

    EnumMap<PassCategory, Price> getEventPassPrices();

    EnumMap<PassCategory, Price> getDailyEventPassPrices();
}
