package ca.ulaval.glo4002.domain.profit.factory;

import ca.ulaval.glo4002.domain.price.Price;
import ca.ulaval.glo4002.domain.profit.Transaction;
import ca.ulaval.glo4002.domain.profit.TransactionType;

public class TransactionFactory {

    public TransactionFactory() {
    }

    public Transaction createExpense(Price amount) {
        return new Transaction(amount, TransactionType.EXPENSE);
    }

    public Transaction createRevenue(Price amount) {
        return new Transaction(amount, TransactionType.REVENUE);
    }
 }
