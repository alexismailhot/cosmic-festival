package ca.ulaval.glo4002.domain.oxygen;

public class OxygenTank {
    
    private final OxygenGrade oxygenGrade;
    private boolean attributed;

    public OxygenTank(OxygenGrade oxygenGrade) {
        this.oxygenGrade = oxygenGrade;
        this.attributed = false;
    } 

    public OxygenGrade getOxygenGrade() {
        return oxygenGrade;
    }

    public boolean isAttributed() {
        return attributed;
    }

    public void markAsAttributed() {
        this.attributed = true;
    }
}
