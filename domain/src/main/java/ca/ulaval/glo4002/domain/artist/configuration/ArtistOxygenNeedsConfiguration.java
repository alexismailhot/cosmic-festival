package ca.ulaval.glo4002.domain.artist.configuration;

import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;

public interface ArtistOxygenNeedsConfiguration {

    OxygenGrade getMinimumOxygenGradeNeeded();

    int getNumberOfOxygenTanksNeededPerGroupMember();
}
