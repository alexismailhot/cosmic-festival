package ca.ulaval.glo4002.domain.transport;

import ca.ulaval.glo4002.domain.event.FestivalDates;
import ca.ulaval.glo4002.domain.price.Price;
import ca.ulaval.glo4002.domain.profit.Transaction;
import ca.ulaval.glo4002.domain.profit.factory.TransactionFactory;
import ca.ulaval.glo4002.domain.profit.repository.TransactionRepository;
import ca.ulaval.glo4002.domain.transport.factory.ShuttleFactory;
import ca.ulaval.glo4002.domain.transport.repository.ShuttleRepository;

import java.time.LocalDate;
import java.util.List;

public class TransportBooker {

    private final FestivalDates festivalDates;
    private final ShuttleRepository shuttleRepository;
    private final ShuttleFactory shuttleFactory;
    private final TransactionRepository transactionRepository;
    private final TransactionFactory transactionFactory;

    public TransportBooker(FestivalDates festivalDates,
                           ShuttleRepository shuttleRepository,
                           ShuttleFactory shuttleFactory,
                            TransactionRepository transactionRepository,
                            TransactionFactory transactionFactory) {
        this.festivalDates = festivalDates;
        this.shuttleRepository = shuttleRepository;
        this.shuttleFactory = shuttleFactory;
        this.transactionRepository = transactionRepository;
        this.transactionFactory = transactionFactory;
    }

    public void bookEventTransport(PassengerNumber passengerNumber, ShuttleType shuttleType) {
        bookDepartureShuttle(festivalDates.getFestivalStartDate(), passengerNumber, shuttleType);
        bookArrivalShuttle(festivalDates.getFestivalEndDate(), passengerNumber, shuttleType);
    }

    public void bookDailyEventTransport(LocalDate eventDate, PassengerNumber passengerNumber, ShuttleType shuttleType) {
        bookDepartureShuttle(eventDate, passengerNumber, shuttleType);
        bookArrivalShuttle(eventDate, passengerNumber, shuttleType);
    }

    public void bookDailyEventTransportForAGroup(LocalDate eventDate, List<PassengerNumber> passengerNumbers,
                                                 ShuttleType shuttleType) {
        bookDepartureShuttleForAGroup(eventDate, passengerNumbers, shuttleType);
        bookArrivalShuttleForAGroup(eventDate, passengerNumbers, shuttleType);
    }

    private void bookDepartureShuttle(LocalDate eventDate, PassengerNumber passengerNumber, ShuttleType shuttleType) {
        Shuttle shuttle = getOrCreateShuttle(eventDate, ShuttleDirection.DEPARTURE, shuttleType);
        bookShuttle(passengerNumber, shuttle);
    }

    private void bookArrivalShuttle(LocalDate eventDate, PassengerNumber passengerNumber, ShuttleType shuttleType) {
        Shuttle shuttle = getOrCreateShuttle(eventDate, ShuttleDirection.ARRIVAL, shuttleType);
        bookShuttle(passengerNumber, shuttle);
    }

    private void bookDepartureShuttleForAGroup(LocalDate eventDate, List<PassengerNumber> passengerNumbers,
                                               ShuttleType shuttleType) {
        int numberOfPassengers = passengerNumbers.size();
        Shuttle shuttle = getOrCreateShuttleForAGroup(eventDate, ShuttleDirection.DEPARTURE, shuttleType, numberOfPassengers);
        bookShuttleForAGroup(passengerNumbers, shuttle);
    }

    private void bookArrivalShuttleForAGroup(LocalDate eventDate, List<PassengerNumber> passengerNumbers,
                                             ShuttleType shuttleType) {
        int numberOfPassengers = passengerNumbers.size();
        Shuttle shuttle = getOrCreateShuttleForAGroup(eventDate, ShuttleDirection.ARRIVAL, shuttleType, numberOfPassengers);
        bookShuttleForAGroup(passengerNumbers, shuttle);
    }

    private void bookShuttle(PassengerNumber passengerNumber, Shuttle shuttle) {
        shuttle.addPassenger(passengerNumber);
        shuttleRepository.upsertShuttle(shuttle);
    }

    private void bookShuttleForAGroup(List<PassengerNumber> passengerNumbers, Shuttle shuttle) {
        shuttle.addGroupOfPassengers(passengerNumbers);
        shuttleRepository.upsertShuttle(shuttle);
    }

    private Shuttle getOrCreateShuttle(LocalDate eventDate, ShuttleDirection shuttleDirection, ShuttleType shuttleType) {
        return shuttleRepository.findByEventDateDirectionAndShuttleType(eventDate, shuttleDirection, shuttleType)
                .stream()
                .filter(shuttle -> !shuttle.isFull())
                .findFirst()
                .orElseGet(() -> createShuttle(eventDate, shuttleDirection, shuttleType));
    }

    private Shuttle getOrCreateShuttleForAGroup(LocalDate eventDate, ShuttleDirection shuttleDirection, ShuttleType shuttleType,
                                                int numberOfPassengers) {
        return shuttleRepository.findByEventDateDirectionAndShuttleType(eventDate, shuttleDirection, shuttleType)
                .stream()
                .filter(shuttle -> shuttle.hasEnoughPlacesFor(numberOfPassengers))
                .findFirst()
                .orElseGet(() -> createShuttle(eventDate, shuttleDirection, shuttleType));
    }

    private Shuttle createShuttle(LocalDate eventDate, ShuttleDirection shuttleDirection, ShuttleType shuttleType) {
        Shuttle shuttle = shuttleFactory.create(shuttleType, eventDate, shuttleDirection);
        Transaction transaction = transactionFactory.createExpense(getOneWayShuttlePrice(shuttleType));
        transactionRepository.add(transaction);
        return shuttle;
    }

    private Price getOneWayShuttlePrice(ShuttleType shuttleType) {
        return shuttleType.getShuttlePrice().divideBy(2);
    }
}
