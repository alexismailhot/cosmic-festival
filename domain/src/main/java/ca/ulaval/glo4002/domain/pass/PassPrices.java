package ca.ulaval.glo4002.domain.pass;

import ca.ulaval.glo4002.domain.pass.configuration.PassPricesConfiguration;
import ca.ulaval.glo4002.domain.price.Price;

public class PassPrices {

    private final PassPricesConfiguration passPriceConfiguration;

    public PassPrices(PassPricesConfiguration passPriceConfiguration) {
        this.passPriceConfiguration = passPriceConfiguration;
    }

    public Price getEventPassPrice(PassCategory passCategory) {
        if (!passPriceConfiguration.getEventPassPrices().containsKey(passCategory)) {
            throw new IllegalAccessError(String.format("no price defined for eventPass of category %s", passCategory.getName()));
        }

        return passPriceConfiguration.getEventPassPrices().get(passCategory);
    }

    public Price getDailyEventPassPrice(PassCategory passCategory) {
        if (!passPriceConfiguration.getDailyEventPassPrices().containsKey(passCategory)) {
            throw new IllegalAccessError(String.format("no price defined for dailyEventPass of category %s", passCategory.getName()));
        }

        return passPriceConfiguration.getDailyEventPassPrices().get(passCategory);
    }
}
