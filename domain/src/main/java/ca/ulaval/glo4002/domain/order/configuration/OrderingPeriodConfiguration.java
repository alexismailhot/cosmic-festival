package ca.ulaval.glo4002.domain.order.configuration;

import java.time.LocalDate;

public interface OrderingPeriodConfiguration {

    LocalDate getOrderPeriodStart();

    LocalDate getOrderPeriodEnd();

    void updateOrderingPeriodStartDate(LocalDate orderingPeriodStartDate);

    void updateOrderingPeriodEndDate(LocalDate orderingPeriodEndDate);
}
