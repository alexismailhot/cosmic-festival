package ca.ulaval.glo4002.domain.oxygen.exception;

public class OxygenRequirementsException extends RuntimeException {

    private static final String MESSAGE = "Cannot meet oxygen requirements: %s";

    public OxygenRequirementsException(String reason) {
        super(String.format(MESSAGE, reason));
    }
}
