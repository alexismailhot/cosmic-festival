package ca.ulaval.glo4002.domain.order;

public enum VendorCode {

    TEAM("TEAM");

    private final String name;

    VendorCode(String name) {
        this.name = name;
    }
}
