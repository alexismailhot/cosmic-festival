package ca.ulaval.glo4002.domain.order.exception;

public class InvalidOrderDateException extends InvalidOrderException {

    private static final String ERROR_MESSAGE = "INVALID_ORDER_DATE";
    private static final String ERROR_DESCRIPTION = "order date should be between January 1 2050 and July 16 2050";

    public InvalidOrderDateException() {
        super(ERROR_MESSAGE, ERROR_DESCRIPTION);
    }

    public String getErrorMessage() {
        return super.errorMessage;
    }

    public String getErrorDescription() { 
        return super.errorDescription;
    }
}
