package ca.ulaval.glo4002.domain.oxygen;

public class OxygenGradeComparator {

    public static int compare(OxygenGrade oxygenGrade, OxygenGrade otherOxygenGrade) {
        return Character.compare(oxygenGrade.getGrade(), otherOxygenGrade.getGrade());
    }
}
