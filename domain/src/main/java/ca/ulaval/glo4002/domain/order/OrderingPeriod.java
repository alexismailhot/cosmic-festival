package ca.ulaval.glo4002.domain.order;

import ca.ulaval.glo4002.domain.order.configuration.OrderingPeriodConfiguration;

import java.time.LocalDate;

public class OrderingPeriod {

    private final static int MAXIMUM_NUMBER_OF_DAYS_ALLOWED_TO_ORDER_BEFORE_FESTIVAL_START_DATE = 180;
    private static final int MINIMUM_NUMBER_OF_DAYS_ALLOWED_TO_ORDER_BEFORE_FESTIVAL_START_DATE = 1;

    private final OrderingPeriodConfiguration orderingPeriodConfiguration;

    public OrderingPeriod(OrderingPeriodConfiguration orderingPeriodConfiguration) {
        this.orderingPeriodConfiguration = orderingPeriodConfiguration;
    }

    public boolean isDateInOrderingPeriod(LocalDate orderDate) {
        LocalDate orderPeriodStart = orderingPeriodConfiguration.getOrderPeriodStart();
        LocalDate orderPeriodEnd = orderingPeriodConfiguration.getOrderPeriodEnd();
        return !(orderDate.isBefore(orderPeriodStart) || orderDate.isAfter(orderPeriodEnd));
    }

    public void updateOrderingPeriodDates(LocalDate newEventStartDate) {
        LocalDate newOrderingPeriodStartDate = calculateNewOrderingPeriodStartDate(newEventStartDate);
        LocalDate newOrderingPeriodEndDate = calculateNewOrderingPeriodEndDate(newEventStartDate);

        orderingPeriodConfiguration.updateOrderingPeriodStartDate(newOrderingPeriodStartDate);
        orderingPeriodConfiguration.updateOrderingPeriodEndDate(newOrderingPeriodEndDate);
    }

    private LocalDate calculateNewOrderingPeriodStartDate(LocalDate newEventStartDate) {
        return newEventStartDate.minusDays(MAXIMUM_NUMBER_OF_DAYS_ALLOWED_TO_ORDER_BEFORE_FESTIVAL_START_DATE);
    }

    private LocalDate calculateNewOrderingPeriodEndDate(LocalDate newEventStartDate) {
        return newEventStartDate.minusDays(MINIMUM_NUMBER_OF_DAYS_ALLOWED_TO_ORDER_BEFORE_FESTIVAL_START_DATE);
    }
}
