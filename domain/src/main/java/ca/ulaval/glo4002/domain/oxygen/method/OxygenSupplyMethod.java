package ca.ulaval.glo4002.domain.oxygen.method;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;
import ca.ulaval.glo4002.domain.oxygen.OxygenGradeComparator;
import ca.ulaval.glo4002.domain.oxygen.OxygenHistory;
import ca.ulaval.glo4002.domain.oxygen.OxygenInventory;
import ca.ulaval.glo4002.domain.oxygen.OxygenSupplyReport;
import ca.ulaval.glo4002.domain.oxygen.OxygenTank;
import ca.ulaval.glo4002.domain.oxygen.factory.OxygenTankFactory;
import ca.ulaval.glo4002.domain.price.Price;

public abstract class OxygenSupplyMethod {

    private final OxygenTankFactory oxygenTankFactory;
    private final OxygenGrade oxygenGrade;
    private final int fabricationDays;
    protected final Price methodCost;
    protected final int numberOfTanksProducedPerBatch;

    protected OxygenSupplyMethod(OxygenTankFactory oxygenTankFactory,
                                 OxygenGrade oxygenGrade,
                                 int numberOfTanksProducedPerBatch,
                                 int fabricationDays,
                                 Price methodCost) {
        this.oxygenTankFactory = oxygenTankFactory;
        this.oxygenGrade = oxygenGrade;
        this.numberOfTanksProducedPerBatch = numberOfTanksProducedPerBatch;
        this.fabricationDays = fabricationDays;
        this.methodCost = methodCost;
    }

    public OxygenGrade getOxygenGrade() {
        return oxygenGrade;
    }

    public boolean meetsGradeRequirement(OxygenGrade minimumOxygenGrade) {
        return OxygenGradeComparator.compare(oxygenGrade, minimumOxygenGrade) >= 0;
    }

    public boolean canProduceInTime(int numberOfDaysLeft) {
        return numberOfDaysLeft >= fabricationDays;
    }

    public int getFabricationDays() {
        return fabricationDays;
    }

    public abstract OxygenSupplyReport supplyNeededOxygen(OxygenHistory oxygenHistory,
                                                          OxygenInventory oxygenInventory,
                                                          int oxygenTanksNeeded);

    protected void generateOxygenTanks(OxygenInventory oxygenInventory, int numberOfOxygenTanksToGenerate) {
        List<OxygenTank> tanksProduced = Stream.generate(() -> oxygenTankFactory.create(oxygenGrade))
                .limit(numberOfOxygenTanksToGenerate)
                .collect(Collectors.toList());
        oxygenInventory.addOxygenTanks(tanksProduced);
    }
}
