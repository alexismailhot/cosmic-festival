package ca.ulaval.glo4002.domain.event.exception;

import ca.ulaval.glo4002.domain.order.exception.InvalidOrderException;

public class InvalidEventDateException extends InvalidOrderException {

    private static final String ERROR_MESSAGE = "INVALID_EVENT_DATE";
    private static final String ERROR_DESCRIPTION = "event date should be between July 17 2050 and July 24 2050";

    public InvalidEventDateException() {
        super(ERROR_MESSAGE, ERROR_DESCRIPTION);
    }

    public String getErrorMessage() {
        return super.errorMessage;
    }

    public String getErrorDescription() { 
        return super.errorDescription;
    }
}
