package ca.ulaval.glo4002.domain.order.exception;

public abstract class OrderException extends RuntimeException {

    protected final String errorMessage;
    protected final String errorDescription;

    public OrderException(String errorMessage, String errorDescription) {
        this.errorMessage = errorMessage;
        this.errorDescription = errorDescription;
    }

    public abstract String getErrorMessage();

    public abstract String getErrorDescription();
}
