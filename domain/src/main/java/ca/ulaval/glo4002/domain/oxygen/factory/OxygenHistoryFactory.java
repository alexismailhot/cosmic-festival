package ca.ulaval.glo4002.domain.oxygen.factory;

import ca.ulaval.glo4002.domain.oxygen.OxygenHistory;

import java.time.LocalDate;

public class OxygenHistoryFactory {

    public OxygenHistory create(LocalDate date) {
        return new OxygenHistory(date);
    }
}
