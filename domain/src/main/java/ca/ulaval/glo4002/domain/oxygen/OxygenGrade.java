package ca.ulaval.glo4002.domain.oxygen;

public enum OxygenGrade {

    GRADE_E('E'),
    GRADE_B('B'),
    GRADE_A('A');

    private final char grade;

    OxygenGrade(char grade) {
        this.grade = grade;
    }

    public char getGrade() {
        return grade;
    }

    @Override
    public String toString() {
        return String.valueOf(grade);
    }
}
