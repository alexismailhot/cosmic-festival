package ca.ulaval.glo4002.domain.order.exception;

import ca.ulaval.glo4002.domain.order.OrderNumber;

public class OrderNotFoundException extends OrderException {

    private static final String ERROR_MESSAGE = "ORDER_NOT_FOUND";
    private static final String ERROR_DESCRIPTION = "order with number %s not found";

    public OrderNotFoundException(OrderNumber orderNumber) {
        super(ERROR_MESSAGE, String.format(ERROR_DESCRIPTION, orderNumber.toString()));
    }

    public String getErrorMessage() { 
        return super.errorMessage;
    }

    public String getErrorDescription() {
        return super.errorDescription;
    }
}
