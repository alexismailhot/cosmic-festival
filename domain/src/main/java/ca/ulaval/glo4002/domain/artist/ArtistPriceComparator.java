package ca.ulaval.glo4002.domain.artist;

import java.util.Comparator;

public class ArtistPriceComparator implements Comparator<Artist> {

    private static final int PRICES_ARE_EQUAL = 0;

    @Override
    public int compare(Artist artist, Artist otherArtist) {
        int priceComparison = artist.compareToBasedOnPrice(otherArtist);
        return priceComparison != PRICES_ARE_EQUAL ? priceComparison : comparePopularityRank(artist, otherArtist);
    }

    private int comparePopularityRank(Artist artist, Artist otherArtist) {
        return artist.compareToBasedOnPopularity(otherArtist);
    }
}
