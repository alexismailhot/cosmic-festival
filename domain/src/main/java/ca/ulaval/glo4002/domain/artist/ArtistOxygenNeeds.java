package ca.ulaval.glo4002.domain.artist;

import ca.ulaval.glo4002.domain.artist.configuration.ArtistOxygenNeedsConfiguration;
import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;

public class ArtistOxygenNeeds {

    private final ArtistOxygenNeedsConfiguration artistOxygenNeedsConfiguration;

    public ArtistOxygenNeeds(ArtistOxygenNeedsConfiguration artistOxygenNeedsConfiguration) {
        this.artistOxygenNeedsConfiguration = artistOxygenNeedsConfiguration;
    }

    public OxygenGrade getMinimumOxygenGradeNeeded() {
        return artistOxygenNeedsConfiguration.getMinimumOxygenGradeNeeded();
    }

    public int getNumberOfOxygenTanksNeededFor(int numberGroupMembers) {
        return artistOxygenNeedsConfiguration.getNumberOfOxygenTanksNeededPerGroupMember() * numberGroupMembers;
    }
}
