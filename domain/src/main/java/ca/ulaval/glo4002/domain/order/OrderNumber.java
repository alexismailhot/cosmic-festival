package ca.ulaval.glo4002.domain.order;

import java.util.Objects;

public class OrderNumber {

    private final String value;

    public OrderNumber(String value) {
        this.value = value;
    }

    public String toString() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderNumber orderNumber = (OrderNumber) o;
        return value.equals(orderNumber.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
