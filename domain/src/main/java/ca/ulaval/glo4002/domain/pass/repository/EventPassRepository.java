package ca.ulaval.glo4002.domain.pass.repository;

import ca.ulaval.glo4002.domain.pass.EventPass;

import java.time.LocalDate;

public interface EventPassRepository {

    void add(EventPass eventPass);

    int getCount();

    int getCountByDate(LocalDate localDate);
}
