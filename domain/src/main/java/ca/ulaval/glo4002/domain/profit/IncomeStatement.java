package ca.ulaval.glo4002.domain.profit;

import ca.ulaval.glo4002.domain.price.Price;

public class IncomeStatement {

    private final Price expenses;
    private final Price revenues;
    private final Price profits;

    public IncomeStatement(Price revenues, Price expenses, Price profits) {
        this.expenses = expenses;
        this.revenues = revenues;
        this.profits = profits;
    }

    public Price getRevenues() {
        return revenues;
    }

    public Price getExpenses() {
        return expenses;
    }

    public Price getProfits() { return profits; }
}
