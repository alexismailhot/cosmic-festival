package ca.ulaval.glo4002.domain.oxygen.repository;

import ca.ulaval.glo4002.domain.oxygen.OxygenHistory;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface OxygenHistoryRepository {

    Optional<OxygenHistory> findByDate(LocalDate date);

    void updateHistory(OxygenHistory history);

    List<OxygenHistory> getAll();
}
