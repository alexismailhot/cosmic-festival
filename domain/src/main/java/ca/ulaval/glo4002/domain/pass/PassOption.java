package ca.ulaval.glo4002.domain.pass;

public enum PassOption {

    PACKAGE("package"),
    SINGLEPASS("singlePass");

    private final String name;

    PassOption(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
