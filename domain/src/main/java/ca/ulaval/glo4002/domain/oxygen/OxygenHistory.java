package ca.ulaval.glo4002.domain.oxygen;

import java.time.LocalDate;

public class OxygenHistory implements Comparable<OxygenHistory> {

    private final LocalDate date;
    private int numberOfTanksBought = 0;
    private int litersOfWaterUsed = 0;
    private int numberOfCandlesUsed = 0;
    private int numberOfTanksMade = 0;

    public OxygenHistory(LocalDate date) {
        this.date = date;
    }

    public void recordCandlesUsed(int numberOfCandlesUsed) {
        this.numberOfCandlesUsed += numberOfCandlesUsed;
    }

    public void recordWaterUsed(int litersOfWaterUsed) {
        this.litersOfWaterUsed += litersOfWaterUsed;
    }

    public void recordOxygenTanksBought(int numberOfTanksBought) {
        this.numberOfTanksBought += numberOfTanksBought;
    }

    public void recordOxygenTanksMade(int numberOfTanksMade) {
        this.numberOfTanksMade += numberOfTanksMade;
    }

    public LocalDate getDate() {
        return date;
    }

    public int getNumberOfTanksBought() {
        return numberOfTanksBought;
    }

    public int getLitersOfWaterUsed() {
        return litersOfWaterUsed;
    }

    public int getNumberOfCandlesUsed() {
        return numberOfCandlesUsed;
    }

    public int getNumberOfTanksMade() {
        return numberOfTanksMade;
    }

    @Override
    public int compareTo(OxygenHistory oxygenHistory) {
        return date.compareTo(oxygenHistory.date);
    }
}
