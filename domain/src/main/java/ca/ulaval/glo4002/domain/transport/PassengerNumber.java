package ca.ulaval.glo4002.domain.transport;

public interface PassengerNumber {

    long getValue();
}
