package ca.ulaval.glo4002.domain.program;

import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;
import ca.ulaval.glo4002.domain.program.exception.InvalidEventActivityException;

import java.util.Arrays;

public enum EventActivity {
    CARDIO_SPACE("cardio", 15, OxygenGrade.GRADE_E),
    WEIGHTLESS_YOGA("yoga", 10, OxygenGrade.GRADE_E);

    private final String name;
    private final int numberOfOxygenTanksNeeded;
    private final OxygenGrade oxygenGradeNeeded;

    EventActivity(String name, int numberOfOxygenTanksNeeded, OxygenGrade oxygenGradeNeeded) {
        this.name = name;
        this.numberOfOxygenTanksNeeded = numberOfOxygenTanksNeeded;
        this.oxygenGradeNeeded = oxygenGradeNeeded;
    }

    public static EventActivity fromString(String activityName) {
        return Arrays.stream(values())
                .filter(eventActivity -> eventActivity.name.equalsIgnoreCase(activityName))
                .findFirst().orElseThrow(InvalidEventActivityException::new);
    }

    public int calculateNumberOfOxygenTanksNeeded(int numberOfParticipants) {
        return numberOfOxygenTanksNeeded * numberOfParticipants;
    }

    public OxygenGrade getOxygenGradeNeeded() {
        return oxygenGradeNeeded;
    }
}
