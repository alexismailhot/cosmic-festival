package ca.ulaval.glo4002.domain.discount;

import ca.ulaval.glo4002.domain.discount.configuration.DiscountsConfiguration;
import ca.ulaval.glo4002.domain.pass.PassCategory;

import java.util.List;
import java.util.stream.Collectors;

public class Discounts {

    private final DiscountsConfiguration discountsConfiguration;

    public Discounts(DiscountsConfiguration discountsConfiguration) {
        this.discountsConfiguration = discountsConfiguration;
    }

    public List<OrderDiscount> getApplicableOrderDiscounts(List<PassCategory> passCategories) {
        return discountsConfiguration.getOrderDiscounts().stream()
                .filter(discount -> discount.isDiscountApplicable(passCategories))
                .collect(Collectors.toList());
    }

    public List<PassDiscount> getApplicablePassDiscounts(List<PassCategory> passCategories) {
        return  discountsConfiguration.getPassDiscounts().stream()
                .filter(discount -> discount.isDiscountApplicable(passCategories))
                .collect(Collectors.toList());
    }
}
