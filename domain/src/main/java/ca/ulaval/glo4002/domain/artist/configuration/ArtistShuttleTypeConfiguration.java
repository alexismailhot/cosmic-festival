package ca.ulaval.glo4002.domain.artist.configuration;

import ca.ulaval.glo4002.domain.transport.ShuttleType;

public interface ArtistShuttleTypeConfiguration {

    ShuttleType getShuttleTypeForSoloArtists();

    ShuttleType getShuttleTypeForGroupArtists();
}
