package ca.ulaval.glo4002.domain.order.factory;

import ca.ulaval.glo4002.domain.order.OrderNumber;
import ca.ulaval.glo4002.domain.order.VendorCode;
import ca.ulaval.glo4002.domain.order.repository.OrderRepository;

public class OrderNumberFactory {

    private static final String HYPHEN = "-";
    private final OrderRepository orderRepository;

    public OrderNumberFactory(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public OrderNumber create(VendorCode vendorCode) {
        String orderNumber = vendorCode.name() + HYPHEN + orderRepository.getCount();
        return new OrderNumber(orderNumber);
    }
}
