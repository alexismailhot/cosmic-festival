package ca.ulaval.glo4002.domain.transport.repository;

import ca.ulaval.glo4002.domain.transport.Shuttle;
import ca.ulaval.glo4002.domain.transport.ShuttleDirection;
import ca.ulaval.glo4002.domain.transport.ShuttleType;

import java.time.LocalDate;
import java.util.List;

public interface ShuttleRepository {

    List<Shuttle> findByEventDate(LocalDate date);

    List<Shuttle> findByEventDateDirectionAndShuttleType(LocalDate eventDate, ShuttleDirection shuttleDirection, ShuttleType shuttleType);

    void upsertShuttle(Shuttle shuttle);

    List<Shuttle> getAllShuttles();
}
