package ca.ulaval.glo4002.domain.profit;

import ca.ulaval.glo4002.domain.price.Price;

public class Transaction {

    private final Price amount;
    private final TransactionType transactionType;

    public Transaction(Price amount, TransactionType transactionType) {
        this.amount = amount;
        this.transactionType = transactionType;
    }

    public Price getPrice() {
        return amount;
    }

    public boolean isExpense() {
        return transactionType.equals(TransactionType.EXPENSE);
    }

    public boolean isRevenue() {
        return transactionType.equals(TransactionType.REVENUE);
    }
}
