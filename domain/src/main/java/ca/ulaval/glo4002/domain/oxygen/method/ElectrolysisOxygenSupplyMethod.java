package ca.ulaval.glo4002.domain.oxygen.method;

import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;
import ca.ulaval.glo4002.domain.oxygen.OxygenHistory;
import ca.ulaval.glo4002.domain.oxygen.OxygenInventory;
import ca.ulaval.glo4002.domain.oxygen.OxygenSupplyReport;
import ca.ulaval.glo4002.domain.oxygen.factory.OxygenTankFactory;
import ca.ulaval.glo4002.domain.price.Price;

public class ElectrolysisOxygenSupplyMethod extends OxygenSupplyMethod {

    private final int litersOfWaterUsed;

    public ElectrolysisOxygenSupplyMethod(OxygenTankFactory oxygenTankFactory,
                                          OxygenGrade oxygenGrade,
                                          int numberOfTanksProduced,
                                          int fabricationDays,
                                          int litersOfWaterUsed,
                                          Price cost) {
        super(oxygenTankFactory, oxygenGrade, numberOfTanksProduced, fabricationDays, cost);
        this.litersOfWaterUsed = litersOfWaterUsed;
    }

    @Override
    public OxygenSupplyReport supplyNeededOxygen(OxygenHistory oxygenHistory, OxygenInventory oxygenInventory, int oxygenTanksNeeded) {
        int numberOfBatches = (int) Math.ceil((double) oxygenTanksNeeded / numberOfTanksProducedPerBatch);

        oxygenHistory.recordWaterUsed(litersOfWaterUsed * numberOfBatches);

        int numberOfTanksProduced = numberOfTanksProducedPerBatch * numberOfBatches;
        generateOxygenTanks(oxygenInventory, numberOfTanksProduced);

        Price productionExpense = methodCost.multiplyBy(numberOfBatches);

        return new OxygenSupplyReport(productionExpense, numberOfTanksProduced);
    }
}
