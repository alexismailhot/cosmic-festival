package ca.ulaval.glo4002.domain.artist;

import ca.ulaval.glo4002.domain.artist.exception.InvalidArtistSortingTypeException;

import java.util.Arrays;

public enum ArtistSortingType {

    LOW_COSTS("lowCosts"),
    MOST_POPULAR("mostPopular");

    private final String name;

    ArtistSortingType(String name) {
        this.name = name;
    }

    public static ArtistSortingType fromString(String sortingTypeName) {
        return Arrays.stream(ArtistSortingType.values())
                .filter(artistSortingType -> artistSortingType.name.equalsIgnoreCase(sortingTypeName))
                .findFirst().orElseThrow(InvalidArtistSortingTypeException::new);
    }
}
