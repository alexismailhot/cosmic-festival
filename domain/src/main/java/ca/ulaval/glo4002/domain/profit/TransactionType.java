package ca.ulaval.glo4002.domain.profit;

public enum TransactionType {
    REVENUE,
    EXPENSE
}
