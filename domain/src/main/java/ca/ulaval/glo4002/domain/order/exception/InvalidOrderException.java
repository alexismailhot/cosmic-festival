package ca.ulaval.glo4002.domain.order.exception;

public class InvalidOrderException extends OrderException {

    private static final String ERROR_MESSAGE = "INVALID_FORMAT";
    private static final String ERROR_DESCRIPTION = "invalid format";

    public InvalidOrderException() {
        super(ERROR_MESSAGE, ERROR_DESCRIPTION);
    }

    public InvalidOrderException(String errorMessage, String errorDescription) {
        super(errorMessage, errorDescription);
    }

    public String getErrorMessage() {
        return super.errorMessage;
    }

    public String getErrorDescription() { 
        return super.errorDescription;
    }
}
