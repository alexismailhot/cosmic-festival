package ca.ulaval.glo4002.domain.event;

import ca.ulaval.glo4002.domain.event.configuration.FestivalDatesConfiguration;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static java.time.temporal.ChronoUnit.DAYS;

public class FestivalDates {

    private final FestivalDatesConfiguration festivalDatesConfiguration;

    public FestivalDates(FestivalDatesConfiguration festivalDatesConfiguration) {
        this.festivalDatesConfiguration = festivalDatesConfiguration;
    }

    public LocalDate getFestivalStartDate() {
        return festivalDatesConfiguration.getFestivalStartDate();
    }

    public LocalDate getFestivalEndDate() {
        return festivalDatesConfiguration.getFestivalEndDate();
    }

    public LocalDate getFestivalProgramUnveilingDate() {
        return festivalDatesConfiguration.getFestivalProgramUnveilingDate();
    }

    public int getFestivalDurationInDays() {
        LocalDate festivalStartDate = festivalDatesConfiguration.getFestivalStartDate();
        LocalDate festivalEndDate = festivalDatesConfiguration.getFestivalEndDate();
        return Math.toIntExact(DAYS.between(festivalStartDate, festivalEndDate) + 1);
    }

    public boolean isDateInFestivalDates(LocalDate date) {
        LocalDate festivalStartDate = festivalDatesConfiguration.getFestivalStartDate();
        LocalDate festivalEndDate = festivalDatesConfiguration.getFestivalEndDate();
        return !(festivalEndDate.isBefore(date) || festivalStartDate.isAfter(date));
    }

    public boolean doDatesContainExactlyAllFestivalDates(List<LocalDate> dates) {
        List<LocalDate> festivalDates = getAllFestivalDates();
        return dates.containsAll(festivalDates) && dates.size() == festivalDates.size();
    }

    public void updateFestivalStartDate(LocalDate startDate) {
        festivalDatesConfiguration.updateFestivalStartDate(startDate);
    }

    public void updateFestivalEndDate(LocalDate endDate) {
        festivalDatesConfiguration.updateFestivalEndDate(endDate);
    }

    private List<LocalDate> getAllFestivalDates() {
        LocalDate festivalStartDate = festivalDatesConfiguration.getFestivalStartDate();
        LocalDate festivalEndDate = festivalDatesConfiguration.getFestivalEndDate();
        LocalDate start = festivalStartDate;
        List<LocalDate> dates = new ArrayList<>();
        while (!start.isAfter(festivalEndDate)) {
            dates.add(start);
            start = start.plusDays(1);
        }
        return dates;
    }
}
