package ca.ulaval.glo4002.domain.pass;

import ca.ulaval.glo4002.domain.price.Price;

import java.time.LocalDate;

public class DailyEventPass extends EventPass {

    private final LocalDate eventDate;

    public DailyEventPass(PassNumber passNumber, PassCategory passCategory, LocalDate eventDate, Price passPrice) {
        super(passNumber, passCategory, passPrice);
        this.eventDate = eventDate;
    }

    public LocalDate getEventDate() {
        return eventDate;
    }

    public boolean isValidOn(LocalDate eventDate) {
        return this.eventDate.equals(eventDate);
    }
}
