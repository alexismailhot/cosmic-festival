package ca.ulaval.glo4002.domain.order;

import ca.ulaval.glo4002.domain.discount.OrderDiscount;
import ca.ulaval.glo4002.domain.discount.PassDiscount;
import ca.ulaval.glo4002.domain.pass.EventPass;
import ca.ulaval.glo4002.domain.price.Price;

import java.util.List;

public class Order {

    private final OrderNumber orderNumber;
    private final List<EventPass> eventPasses;
    private final List<OrderDiscount> orderDiscounts;
    private final List<PassDiscount> passDiscounts;

    public Order(OrderNumber orderNumber, List<EventPass> eventPasses, List<OrderDiscount> applicableOrderDiscounts,
                 List<PassDiscount> applicablePassDiscounts) {
        this.orderNumber = orderNumber;
        this.eventPasses = eventPasses;
        this.orderDiscounts = applicableOrderDiscounts;
        this.passDiscounts = applicablePassDiscounts;
    }

    public OrderNumber getNumber() {
        return orderNumber;
    }

    public List<EventPass> getEventPasses() {
        return eventPasses;
    }

    public Price calculateTotalPrice() {
        Price price = calculateBasePrice();
        for (OrderDiscount discount : orderDiscounts) {
            price = discount.applyDiscount(price);
        }
        return price;
    }

    private Price calculateBasePrice() {
        return eventPasses.stream()
                .map(eventPass -> eventPass.calculatePrice(passDiscounts))
                .reduce(Price::add)
                .orElseThrow(() -> new IllegalStateException("passes shouldn't be empty"));
    }
}
