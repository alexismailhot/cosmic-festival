package ca.ulaval.glo4002.domain.order.factory;

import ca.ulaval.glo4002.domain.discount.Discounts;
import ca.ulaval.glo4002.domain.discount.OrderDiscount;
import ca.ulaval.glo4002.domain.discount.PassDiscount;
import ca.ulaval.glo4002.domain.order.Order;
import ca.ulaval.glo4002.domain.order.VendorCode;
import ca.ulaval.glo4002.domain.pass.EventPass;
import ca.ulaval.glo4002.domain.pass.PassCategory;

import java.util.List;
import java.util.stream.Collectors;

public class OrderFactory {

    private final OrderNumberFactory orderNumberFactory;
    private final Discounts discounts;

    public OrderFactory(OrderNumberFactory orderNumberFactory, Discounts discounts) {
        this.orderNumberFactory = orderNumberFactory;
        this.discounts = discounts;
    }

    public Order create(VendorCode vendorCode, List<EventPass> eventPasses) {
        List<PassCategory> passCategories = eventPasses.stream()
                .map(EventPass::getPassCategory)
                .collect(Collectors.toList());
        List<OrderDiscount> applicableOrderDiscounts = discounts.getApplicableOrderDiscounts(passCategories);
        List<PassDiscount> applicablePassDiscounts = discounts.getApplicablePassDiscounts(passCategories);
        return new Order(orderNumberFactory.create(vendorCode), eventPasses, applicableOrderDiscounts, applicablePassDiscounts);
    }
}
