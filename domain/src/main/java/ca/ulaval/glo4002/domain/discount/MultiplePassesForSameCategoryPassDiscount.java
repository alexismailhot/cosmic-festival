package ca.ulaval.glo4002.domain.discount;

import ca.ulaval.glo4002.domain.pass.PassCategory;
import ca.ulaval.glo4002.domain.price.Price;

import java.util.List;

public class MultiplePassesForSameCategoryPassDiscount implements PassDiscount {

    private final PassCategory applicableCategory;
    private final int numberOfPassesRequired;
    private final Price priceDiscount;

    public MultiplePassesForSameCategoryPassDiscount(PassCategory applicableCategory, int numberOfPassesRequired, Price priceDiscount) {
        this.applicableCategory = applicableCategory;
        this.numberOfPassesRequired = numberOfPassesRequired;
        this.priceDiscount = priceDiscount;
    }

    @Override
    public Price applyDiscountIfApplicable(Price initialPrice, PassCategory passCategory) {
        return isApplicableToPass(passCategory) ? initialPrice.subtract(priceDiscount) : initialPrice;
    }

    @Override
    public boolean isDiscountApplicable(List<PassCategory> passCategories) {
        return passCategories.stream().filter(this::isApplicableToPass).count() >= numberOfPassesRequired;
    }

    private boolean isApplicableToPass(PassCategory passCategory) {
        return passCategory.equals(applicableCategory);
    }
}
