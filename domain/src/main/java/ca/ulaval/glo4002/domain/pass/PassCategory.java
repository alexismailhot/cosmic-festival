package ca.ulaval.glo4002.domain.pass;

import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;

public enum PassCategory {

    SUPERNOVA("supernova", 5, OxygenGrade.GRADE_E),
    SUPERGIANT("supergiant",3, OxygenGrade.GRADE_B),
    NEBULA("nebula", 3, OxygenGrade.GRADE_A);

    private final String name;
    private final int dailyOxygenTanks;
    private final OxygenGrade minimumOxygenGrade;

    PassCategory(String name, int dailyOxygenTanks, OxygenGrade minimumOxygenGrade) {
        this.name = name;
        this.dailyOxygenTanks = dailyOxygenTanks;
        this.minimumOxygenGrade = minimumOxygenGrade;
    }

    public String getName() {
        return name;
    }

    public OxygenGrade getMinimumOxygenGrade() {
        return minimumOxygenGrade;
    }

    public int getDailyOxygenNeeds() {
        return dailyOxygenTanks;
    }
}
