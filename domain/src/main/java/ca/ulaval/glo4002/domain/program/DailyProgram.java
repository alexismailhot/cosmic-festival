package ca.ulaval.glo4002.domain.program;

import ca.ulaval.glo4002.domain.artist.Artist;

import java.time.LocalDate;

public class DailyProgram {

    private final LocalDate eventDate;
    private final EventActivity eventActivity;
    private final Artist artist;

    public DailyProgram(LocalDate eventDate, EventActivity eventActivity, Artist artist) {
        this.eventDate = eventDate;
        this.eventActivity = eventActivity;
        this.artist = artist;
    }

    public LocalDate getEventDate() {
        return eventDate;
    }

    public EventActivity getEventActivity() {
        return eventActivity;
    }

    public Artist getArtist() {
        return artist;
    }
}
