package ca.ulaval.glo4002.domain.discount;

import ca.ulaval.glo4002.domain.pass.PassCategory;
import ca.ulaval.glo4002.domain.price.Price;

import java.util.List;

public interface PassDiscount {

    Price applyDiscountIfApplicable(Price initialPrice, PassCategory passCategory);

    boolean isDiscountApplicable(List<PassCategory> passCategories);
}
