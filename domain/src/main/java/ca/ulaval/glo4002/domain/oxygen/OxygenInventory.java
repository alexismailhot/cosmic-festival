package ca.ulaval.glo4002.domain.oxygen;

import ca.ulaval.glo4002.domain.oxygen.repository.OxygenTankRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OxygenInventory {

    private final OxygenTankRepository oxygenTankRepository;

    public OxygenInventory(OxygenTankRepository oxygenTankRepository) {
        this.oxygenTankRepository = oxygenTankRepository;
    }

    public void addOxygenTanks(List<OxygenTank> oxygenTanks) {
        oxygenTankRepository.addOxygenTanks(oxygenTanks);
    }

    public void attributeOxygen(int numberOfTanksToAttribute, OxygenGrade oxygenGrade) {
        List<OxygenTank> availableTanks = oxygenTankRepository.findAvailableTanksByGrade(oxygenGrade);
        if (numberOfTanksToAttribute > availableTanks.size()) {
            throw new IllegalArgumentException("number of tanks needed should not exceed currently available tanks");
        }
        Stream<OxygenTank> availableTanksToAttribute = availableTanks.stream().limit(numberOfTanksToAttribute);
        availableTanksToAttribute.forEach(this::markOxygenTankAsAttributed);
    }

    private void markOxygenTankAsAttributed(OxygenTank oxygenTank) {
        oxygenTank.markAsAttributed();
        oxygenTankRepository.update(oxygenTank);
    }

    public int useAvailableTanks(int numberOfTanksNeeded, OxygenGrade minimumOxygenGrade, OxygenGrade methodOxygenGrade) {
        if (OxygenGradeComparator.compare(minimumOxygenGrade, methodOxygenGrade) > 0) {
            throw new IllegalArgumentException("Method oxygen grade has to be greater than minimum oxygen grade");
        }

        List<OxygenGrade> oxygenGradesUseable = Arrays.stream(OxygenGrade.values())
                .filter(oxygenGrade -> areTanksOfGradeUseable(oxygenGrade, minimumOxygenGrade, methodOxygenGrade))
                .sorted(OxygenGradeComparator::compare)
                .collect(Collectors.toList());

        int numberOfTanksToAttribute = numberOfTanksNeeded;
        List<OxygenTank> availableTanks = new ArrayList<>();
        for (OxygenGrade oxygenGrade : oxygenGradesUseable) {
            List<OxygenTank> availableTanksToAttribute = oxygenTankRepository.findAvailableTanksByGrade(oxygenGrade)
                    .stream()
                    .limit(numberOfTanksToAttribute)
                    .collect(Collectors.toList());
            availableTanksToAttribute.forEach(this::markOxygenTankAsAttributed);
            numberOfTanksToAttribute -= availableTanksToAttribute.size();
        }
        oxygenGradesUseable.stream().map(oxygenTankRepository::findAvailableTanksByGrade).forEach(availableTanks::addAll);
        return Math.max(0, numberOfTanksToAttribute - availableTanks.size());
    }

    private boolean areTanksOfGradeUseable(OxygenGrade oxygenGrade, OxygenGrade minimumOxygenGrade, OxygenGrade methodOxygenGrade) {
        boolean doesGradeMeetGradeRequirements = OxygenGradeComparator.compare(oxygenGrade, minimumOxygenGrade) >= 0;
        boolean isGradeLessOrEqualToMethodGrade = OxygenGradeComparator.compare(oxygenGrade, methodOxygenGrade) <= 0;
        return doesGradeMeetGradeRequirements && isGradeLessOrEqualToMethodGrade;
    }

    public int getNumberOfTanks() {
        return oxygenTankRepository.getAllTanks().size();
    }

    public List<OxygenGrade> getDistinctProducedGrades() {
        return oxygenTankRepository.getAllTanks().stream()
                .map(OxygenTank::getOxygenGrade)
                .distinct()
                .collect(Collectors.toList());
    }

    public int getNumberOfTanksForGrade(OxygenGrade oxygenGrade) {
        return oxygenTankRepository.findByGrade(oxygenGrade).size();
    }
}
