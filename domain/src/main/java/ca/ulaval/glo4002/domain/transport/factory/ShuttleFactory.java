package ca.ulaval.glo4002.domain.transport.factory;

import ca.ulaval.glo4002.domain.transport.Shuttle;
import ca.ulaval.glo4002.domain.transport.ShuttleDirection;
import ca.ulaval.glo4002.domain.transport.ShuttleType;

import java.time.LocalDate;

public class ShuttleFactory {

    public Shuttle create(ShuttleType shuttleType, LocalDate eventDate, ShuttleDirection shuttleDirection) {
        return new Shuttle(shuttleType, eventDate, shuttleDirection);
    }
}
