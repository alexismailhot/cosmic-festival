package ca.ulaval.glo4002.service.pass;

import ca.ulaval.glo4002.domain.order.OrderNumber;
import ca.ulaval.glo4002.domain.order.VendorCode;
import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;
import ca.ulaval.glo4002.domain.pass.DailyEventPass;
import ca.ulaval.glo4002.domain.pass.EventPass;
import ca.ulaval.glo4002.domain.pass.PassCategory;
import ca.ulaval.glo4002.domain.pass.PassNumber;
import ca.ulaval.glo4002.domain.pass.factory.EventPassFactory;
import ca.ulaval.glo4002.domain.pass.repository.EventPassRepository;
import ca.ulaval.glo4002.domain.transport.ShuttleType;
import ca.ulaval.glo4002.service.order.OrderService;
import ca.ulaval.glo4002.service.order.OrderValidator;
import ca.ulaval.glo4002.service.oxygen.OxygenService;
import ca.ulaval.glo4002.domain.transport.TransportBooker;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class PassService {

    private final EventPassFactory eventPassFactory;
    private final EventPassRepository eventPassRepository;
    private final OrderService orderService;
    private final TransportBooker transportBooker;
    private final OxygenService oxygenService;
    private final OrderValidator orderValidator;

    public PassService(EventPassFactory eventPassFactory, EventPassRepository eventPassRepository, OrderService orderService,
                       TransportBooker transportBooker, OxygenService oxygenService, OrderValidator orderValidator) {
        this.eventPassFactory = eventPassFactory;
        this.orderService = orderService;
        this.eventPassRepository = eventPassRepository;
        this.transportBooker = transportBooker;
        this.oxygenService = oxygenService;
        this.orderValidator = orderValidator;
    }

    public OrderNumber buyDailyEventPasses(LocalDate orderDate, VendorCode vendorCode, PassCategory passCategory,
                                           List<LocalDate> dates) {
        orderValidator.validateOrderDate(orderDate);
        orderValidator.validateDatesAreFestivalDates(dates);
        List<EventPass> eventPasses = dates.stream()
                .map(eventDate -> createDailyPass(eventDate, passCategory, orderDate))
                .collect(Collectors.toList());
        return orderService.createOrder(vendorCode, eventPasses);
    }
    
    public OrderNumber buyEventPass(LocalDate orderDate, VendorCode vendorCode, PassCategory passCategory) {
        orderValidator.validateOrderDate(orderDate);
        EventPass eventPass = eventPassFactory.createEventPass(passCategory);
        eventPassRepository.add(eventPass);

        bookTransportForAnEventPass(passCategory, eventPass);
        supplyOxygenForAnEventPass(passCategory, orderDate);

        return orderService.createOrder(vendorCode, Collections.singletonList(eventPass));
    }

    private EventPass createDailyPass(LocalDate eventDate, PassCategory passCategory, LocalDate orderDate) {
        DailyEventPass dailyEventPass = eventPassFactory.createDailyEventPass(passCategory, eventDate);
        eventPassRepository.add(dailyEventPass);

        bookTransportForADailyPass(eventDate, passCategory, dailyEventPass);
        supplyOxygenForADailyPass(passCategory, orderDate);

        return dailyEventPass;
    }

    private void bookTransportForAnEventPass(PassCategory passCategory, EventPass eventPass) {
        PassNumber passNumber = eventPass.getPassNumber();
        ShuttleType shuttleType = ShuttleType.fromPassCategory(passCategory);

        transportBooker.bookEventTransport(passNumber, shuttleType);
    }

    private void bookTransportForADailyPass(LocalDate eventDate, PassCategory passCategory, DailyEventPass dailyEventPass) {
        PassNumber passNumber = dailyEventPass.getPassNumber();
        ShuttleType shuttleType = ShuttleType.fromPassCategory(passCategory);

        transportBooker.bookDailyEventTransport(eventDate, passNumber, shuttleType);
    }

    private void supplyOxygenForAnEventPass(PassCategory passCategory, LocalDate orderDate) {
        int numberOfTanksNeededDaily = passCategory.getDailyOxygenNeeds();
        OxygenGrade minimumOxygenGrade = passCategory.getMinimumOxygenGrade();

        oxygenService.supplyOxygenForAllFestivalDates(orderDate, minimumOxygenGrade, numberOfTanksNeededDaily);
    }

    private void supplyOxygenForADailyPass(PassCategory passCategory, LocalDate orderDate) {
        int numberOfTanksNeeded = passCategory.getDailyOxygenNeeds();
        OxygenGrade minimumOxygenGrade = passCategory.getMinimumOxygenGrade();

        oxygenService.supplyDailyEventOxygen(orderDate, minimumOxygenGrade, numberOfTanksNeeded);
    }
}
