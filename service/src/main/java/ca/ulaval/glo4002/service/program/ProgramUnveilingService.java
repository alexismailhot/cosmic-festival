package ca.ulaval.glo4002.service.program;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import ca.ulaval.glo4002.domain.artist.Artist;
import ca.ulaval.glo4002.domain.artist.ArtistOxygenNeeds;
import ca.ulaval.glo4002.domain.artist.ArtistShuttleType;
import ca.ulaval.glo4002.domain.event.FestivalDates;
import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;
import ca.ulaval.glo4002.domain.pass.repository.EventPassRepository;
import ca.ulaval.glo4002.domain.profit.Transaction;
import ca.ulaval.glo4002.domain.profit.factory.TransactionFactory;
import ca.ulaval.glo4002.domain.profit.repository.TransactionRepository;
import ca.ulaval.glo4002.domain.program.DailyProgram;
import ca.ulaval.glo4002.domain.program.EventActivity;
import ca.ulaval.glo4002.domain.program.exception.InvalidProgramException;
import ca.ulaval.glo4002.domain.transport.PassengerNumber;
import ca.ulaval.glo4002.domain.transport.ShuttleType;
import ca.ulaval.glo4002.service.artist.ArtistService;
import ca.ulaval.glo4002.service.oxygen.OxygenService;
import ca.ulaval.glo4002.service.program.assembler.DailyProgramAssembler;
import ca.ulaval.glo4002.domain.transport.TransportBooker;

public class ProgramUnveilingService {

    private final TransportBooker transportBooker;
    private final ArtistService artistService;
    private final OxygenService oxygenService;
    private final TransactionRepository transactionRepository;
    private final EventPassRepository eventPassRepository;
    private final FestivalDates festivalDates;
    private final ProgramValidator programValidator;
    private final DailyProgramAssembler dailyProgramAssembler;
    private final ArtistOxygenNeeds artistOxygenNeeds;
    private final ArtistShuttleType artistShuttleType;
    private final TransactionFactory transactionFactory;

    public ProgramUnveilingService(TransportBooker transportBooker,
                                   ArtistService artistService,
                                   OxygenService oxygenService,
                                   TransactionRepository transactionRepository,
                                   EventPassRepository eventPassRepository,
                                   FestivalDates festivalDates,
                                   ProgramValidator programValidator,
                                   DailyProgramAssembler dailyProgramAssembler,
                                   ArtistOxygenNeeds artistOxygenNeeds, 
                                   ArtistShuttleType artistShuttleType, 
                                   TransactionFactory transactionFactory) {
        this.transportBooker = transportBooker;
        this.artistService = artistService;
        this.oxygenService = oxygenService;
        this.transactionRepository = transactionRepository;
        this.eventPassRepository = eventPassRepository;
        this.festivalDates = festivalDates;
        this.programValidator = programValidator;
        this.dailyProgramAssembler = dailyProgramAssembler;
        this.artistOxygenNeeds = artistOxygenNeeds;
        this.artistShuttleType = artistShuttleType;
        this.transactionFactory = transactionFactory;
    }

    public void setFestivalProgram(ProgramSubmission programSubmission) {
        programSubmission.validateProgram(programValidator);
        programSubmission.getDailyProgramSubmissions().stream()
                .map(this::assembleDailyProgram)
                .collect(Collectors.toList())
                .forEach(this::setDailyProgram);
    }

    private DailyProgram assembleDailyProgram(DailyProgramSubmission dailyProgramSubmission) {
        return dailyProgramAssembler.assemble(dailyProgramSubmission,
                artistService.findArtistByName(dailyProgramSubmission.getArtistName())
                        .orElseThrow(InvalidProgramException::new));
    }

    private void setDailyProgram(DailyProgram dailyProgram) {
        LocalDate unveilingDate = festivalDates.getFestivalProgramUnveilingDate();
        LocalDate eventDate = dailyProgram.getEventDate();
        setAMProgram(eventDate, dailyProgram.getEventActivity(), unveilingDate);
        setPMProgram(eventDate, dailyProgram.getArtist(), unveilingDate);
    }

    private void setAMProgram(LocalDate eventDate, EventActivity activity, LocalDate unveilingDate) {
        int numberOfParticipants = eventPassRepository.getCountByDate(eventDate);
        orderOxygenForAllActivityParticipants(activity, unveilingDate, numberOfParticipants);
    }

    private void setPMProgram(LocalDate eventDate, Artist artist, LocalDate unveilingDate) {
        orderOxygenForArtist(artist, unveilingDate);
        bookTransportForArtist(artist, eventDate);
        addArtistCostToExpenses(artist);
    }

    private void orderOxygenForAllActivityParticipants(EventActivity activity, LocalDate unveilingDate, int numberOfParticipants) {
        int totalNumberOfOxygenTanksNeeded = activity.calculateNumberOfOxygenTanksNeeded(numberOfParticipants);
        OxygenGrade oxygenGradeNeeded = activity.getOxygenGradeNeeded();

        oxygenService.supplyDailyEventOxygen(unveilingDate, oxygenGradeNeeded, totalNumberOfOxygenTanksNeeded);
    }

    private void orderOxygenForArtist(Artist artist, LocalDate unveilingDate) {
        OxygenGrade oxygenGradeNeeded = artistOxygenNeeds.getMinimumOxygenGradeNeeded();
        int numberGroupMembers = artist.getNumberGroupMembers();
        int numberOfTanksNeeded = artistOxygenNeeds.getNumberOfOxygenTanksNeededFor(numberGroupMembers);

        oxygenService.supplyDailyEventOxygen(unveilingDate, oxygenGradeNeeded, numberOfTanksNeeded);
    }

    private void bookTransportForArtist(Artist artist, LocalDate eventDate) {
        int numberGroupMembers = artist.getNumberGroupMembers();
        List<PassengerNumber> passengerNumbers = Collections.nCopies(numberGroupMembers, artist.getNumber());
        ShuttleType shuttleTypeNeeded = artistShuttleType.getShuttleTypeFor(numberGroupMembers);

        transportBooker.bookDailyEventTransportForAGroup(eventDate, passengerNumbers, shuttleTypeNeeded);
    }

    private void addArtistCostToExpenses(Artist artist) {
        Transaction transaction = transactionFactory.createExpense(artist.getPrice());
        transactionRepository.add(transaction);
    }
}
