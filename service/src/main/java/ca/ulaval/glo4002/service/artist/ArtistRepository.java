package ca.ulaval.glo4002.service.artist;

import ca.ulaval.glo4002.domain.artist.Artist;

import java.util.List;

public interface ArtistRepository {

    List<Artist> getArtists();
}
