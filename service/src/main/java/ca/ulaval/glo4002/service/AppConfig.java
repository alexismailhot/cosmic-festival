package ca.ulaval.glo4002.service;

public interface AppConfig {

    String getExternalServiceApiUrl();
}
