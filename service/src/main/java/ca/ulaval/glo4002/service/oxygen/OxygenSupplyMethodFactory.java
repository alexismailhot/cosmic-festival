package ca.ulaval.glo4002.service.oxygen;

import ca.ulaval.glo4002.domain.event.FestivalDates;
import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;
import ca.ulaval.glo4002.domain.oxygen.OxygenGradeComparator;
import ca.ulaval.glo4002.domain.oxygen.exception.OxygenRequirementsException;
import ca.ulaval.glo4002.domain.oxygen.method.OxygenSupplyMethod;

import java.time.LocalDate;
import java.util.List;

import static java.time.temporal.ChronoUnit.DAYS;

public class OxygenSupplyMethodFactory {

    private final FestivalDates festivalDates;
    private final List<OxygenSupplyMethod> oxygenSupplyMethods;

    public OxygenSupplyMethodFactory(FestivalDates festivalDates, List<OxygenSupplyMethod> oxygenSupplyMethods) {
        this.festivalDates = festivalDates;
        this.oxygenSupplyMethods = oxygenSupplyMethods;
    }

    public OxygenSupplyMethod getLowestGradeSupplyMethod(OxygenGrade minimumOxygenGrade, LocalDate orderDate) {
        return oxygenSupplyMethods.stream()
                .filter(method -> method.meetsGradeRequirement(minimumOxygenGrade))
                .filter(method -> canProduceInTime(method, orderDate))
                .min((method, otherMethod) -> OxygenGradeComparator.compare(method.getOxygenGrade(), otherMethod.getOxygenGrade()))
                .orElseThrow(() -> new OxygenRequirementsException("cannot create required oxygen grade by festival start date"));
    }

    private boolean canProduceInTime(OxygenSupplyMethod method, LocalDate orderDate) {
        return method.canProduceInTime(Math.toIntExact(DAYS.between(orderDate, festivalDates.getFestivalStartDate())));
    }
}
