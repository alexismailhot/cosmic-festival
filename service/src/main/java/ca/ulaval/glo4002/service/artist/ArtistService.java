package ca.ulaval.glo4002.service.artist;

import ca.ulaval.glo4002.domain.artist.Artist;
import ca.ulaval.glo4002.domain.artist.ArtistPriceComparator;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ArtistService {

    private final ArtistRepository artistRepository;
    private final ArtistPriceComparator artistPriceComparator;

    public ArtistService(ArtistRepository artistRepository, ArtistPriceComparator artistPriceComparator) {
        this.artistRepository = artistRepository;
        this.artistPriceComparator = artistPriceComparator;
    }

    public List<Artist> getArtistsByLowCosts() {
        return artistRepository.getArtists()
                .stream()
                .sorted(artistPriceComparator.reversed())
                .collect(Collectors.toList());
    }

    public List<Artist> getArtistsByPopularity() {
        return artistRepository.getArtists()
                .stream()
                .sorted(Comparator.comparing(Artist::getPopularity))
                .collect(Collectors.toList());
    }

    public Optional<Artist> findArtistByName(String artistName) {
        return artistRepository.getArtists()
                .stream()
                .filter(artist -> artist.hasName(artistName))
                .findFirst();
    }
}
