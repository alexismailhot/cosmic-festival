package ca.ulaval.glo4002.service.program.assembler;

import ca.ulaval.glo4002.domain.artist.Artist;
import ca.ulaval.glo4002.domain.program.DailyProgram;
import ca.ulaval.glo4002.service.program.DailyProgramSubmission;

public class DailyProgramAssembler {
    
    public DailyProgram assemble(DailyProgramSubmission dailyProgramSubmission, Artist artist) {
        return new DailyProgram(dailyProgramSubmission.getDate(), dailyProgramSubmission.getEventActivity(), artist);
    }
}
