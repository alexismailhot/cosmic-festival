package ca.ulaval.glo4002.service.program;

import java.util.List;
import java.util.stream.Collectors;

public class ProgramSubmission {

    private final List<DailyProgramSubmission> dailyProgramSubmissions;

    public ProgramSubmission(List<DailyProgramSubmission> dailyProgramSubmissions) {
        this.dailyProgramSubmissions = dailyProgramSubmissions;
    }

    public List<DailyProgramSubmission> getDailyProgramSubmissions() {
        return dailyProgramSubmissions;
    }

    public void validateProgram(ProgramValidator programValidator) {
        validateDates(programValidator);
        programValidator.validateNoArtistsDuplicate(dailyProgramSubmissions
                .stream()
                .map(DailyProgramSubmission::getArtistName)
                .collect(Collectors.toList()));
    }

    private void validateDates(ProgramValidator programValidator) {
        programValidator.validateNoEventDateIsMissingOrDuplicate(dailyProgramSubmissions
                .stream()
                .map(DailyProgramSubmission::getDate)
                .collect(Collectors.toList()));
    }
}
