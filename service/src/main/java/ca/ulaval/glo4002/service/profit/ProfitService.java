package ca.ulaval.glo4002.service.profit;

import ca.ulaval.glo4002.domain.profit.IncomeStatement;
import ca.ulaval.glo4002.domain.profit.Transaction;
import ca.ulaval.glo4002.domain.profit.repository.TransactionRepository;

import java.util.List;

public class ProfitService {

    private final TransactionRepository transactionRepository;
    private final IncomeStatementFactory incomeStatementFactory;

    public ProfitService(TransactionRepository transactionRepository, IncomeStatementFactory incomeStatementFactory) {
        this.transactionRepository = transactionRepository;
        this.incomeStatementFactory = incomeStatementFactory;
    }

    public IncomeStatement getIncomeStatement() {
        List<Transaction> transactions = transactionRepository.getTransactions();
        return incomeStatementFactory.create(transactions);
    }
}
