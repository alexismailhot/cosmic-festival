package ca.ulaval.glo4002.service.order;

import ca.ulaval.glo4002.domain.order.Order;
import ca.ulaval.glo4002.domain.order.OrderNumber;
import ca.ulaval.glo4002.domain.order.VendorCode;
import ca.ulaval.glo4002.domain.order.exception.OrderNotFoundException;
import ca.ulaval.glo4002.domain.order.factory.OrderFactory;
import ca.ulaval.glo4002.domain.order.repository.OrderRepository;
import ca.ulaval.glo4002.domain.pass.EventPass;
import ca.ulaval.glo4002.domain.profit.Transaction;
import ca.ulaval.glo4002.domain.profit.factory.TransactionFactory;
import ca.ulaval.glo4002.domain.profit.repository.TransactionRepository;

import java.util.List;

public class OrderService {

    private final OrderFactory orderFactory;
    private final OrderRepository orderRepository;
    private final TransactionRepository transactionRepository;
    private final TransactionFactory transactionFactory;

    public OrderService(OrderFactory orderFactory,
                        OrderRepository orderRepository,
                        TransactionRepository transactionRepository, TransactionFactory transactionFactory) {
        this.orderFactory = orderFactory;
        this.orderRepository = orderRepository;
        this.transactionRepository = transactionRepository;
        this.transactionFactory = transactionFactory;
    }

    public OrderNumber createOrder(VendorCode vendorCode, List<EventPass> eventPasses) {
        Order order = orderFactory.create(vendorCode, eventPasses);
        Transaction transaction = transactionFactory.createRevenue(order.calculateTotalPrice());
        transactionRepository.add(transaction);
        orderRepository.add(order);
        return order.getNumber();
    }

    public Order getOrder(OrderNumber orderNumber) {
        return orderRepository.findByOrderNumber(orderNumber).orElseThrow(() -> new OrderNotFoundException(orderNumber));
    }
}
