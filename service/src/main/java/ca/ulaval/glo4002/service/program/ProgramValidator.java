package ca.ulaval.glo4002.service.program;

import ca.ulaval.glo4002.domain.event.FestivalDates;
import ca.ulaval.glo4002.domain.program.exception.InvalidProgramException;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class ProgramValidator {

    private final FestivalDates festivalDates;

    public ProgramValidator(FestivalDates festivalDates) {
        this.festivalDates = festivalDates;
    }

    public void validateNoEventDateIsMissingOrDuplicate(List<LocalDate> eventDates) {
        if (!festivalDates.doDatesContainExactlyAllFestivalDates(eventDates)) {
            throw new InvalidProgramException();
        }
    }

    public void validateNoArtistsDuplicate(List<String> artistNames) {
        List<String> distinctArtistNames = artistNames.stream().distinct().collect(Collectors.toList());
        if (!(distinctArtistNames.containsAll(artistNames) && artistNames.size() == distinctArtistNames.size())) {
            throw new InvalidProgramException();
        }
    }
}
