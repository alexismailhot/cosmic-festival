package ca.ulaval.glo4002.service.program;

import ca.ulaval.glo4002.domain.program.EventActivity;

import java.time.LocalDate;

public class DailyProgramSubmission {

    private final LocalDate date;
    private final EventActivity eventActivity;
    private final String artistName;

    public DailyProgramSubmission(LocalDate date, EventActivity eventActivity, String artistName) {
        this.date = date;
        this.eventActivity = eventActivity;
        this.artistName = artistName;
    }

    public LocalDate getDate() {
        return date;
    }

    public EventActivity getEventActivity() {
        return eventActivity;
    }

    public String getArtistName() {
        return artistName;
    }
}
