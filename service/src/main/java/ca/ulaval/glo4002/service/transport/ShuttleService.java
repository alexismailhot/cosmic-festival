package ca.ulaval.glo4002.service.transport;

import ca.ulaval.glo4002.domain.event.FestivalDates;
import ca.ulaval.glo4002.domain.event.exception.InvalidEventDateException;
import ca.ulaval.glo4002.domain.transport.Shuttle;
import ca.ulaval.glo4002.domain.transport.repository.ShuttleRepository;

import java.time.LocalDate;
import java.util.List;

public class ShuttleService {

    private final ShuttleRepository shuttleRepository;
    private final FestivalDates festivalDates;


    public ShuttleService(ShuttleRepository shuttleRepository, FestivalDates festivalDates) {
        this.shuttleRepository = shuttleRepository;
        this.festivalDates = festivalDates;
    }

    public List<Shuttle> getShuttles(LocalDate date) {
        if (!festivalDates.isDateInFestivalDates(date)) {
            throw new InvalidEventDateException();
        }
        return shuttleRepository.findByEventDate(date);
    }

    public List<Shuttle> getAllShuttles() {
        return shuttleRepository.getAllShuttles();
    }
}
