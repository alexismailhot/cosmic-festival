package ca.ulaval.glo4002.service.oxygen;

import ca.ulaval.glo4002.domain.event.FestivalDates;
import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;
import ca.ulaval.glo4002.domain.oxygen.OxygenHistory;
import ca.ulaval.glo4002.domain.oxygen.OxygenInventory;
import ca.ulaval.glo4002.domain.oxygen.OxygenSupplyReport;
import ca.ulaval.glo4002.domain.oxygen.factory.OxygenHistoryFactory;
import ca.ulaval.glo4002.domain.oxygen.method.OxygenSupplyMethod;
import ca.ulaval.glo4002.domain.oxygen.repository.OxygenHistoryRepository;
import ca.ulaval.glo4002.domain.profit.Transaction;
import ca.ulaval.glo4002.domain.profit.factory.TransactionFactory;
import ca.ulaval.glo4002.domain.profit.repository.TransactionRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class OxygenService {

    private final FestivalDates festivalDates;
    private final OxygenInventory oxygenInventory;
    private final OxygenSupplyMethodFactory oxygenSupplyMethodFactory;
    private final OxygenHistoryRepository oxygenHistoryRepository;
    private final OxygenHistoryFactory oxygenHistoryFactory;
    private final TransactionRepository transactionRepository;
    private final TransactionFactory transactionFactory;

    public OxygenService(FestivalDates festivalDates,
                         OxygenInventory oxygenInventory,
                         OxygenSupplyMethodFactory oxygenSupplyMethodFactory,
                         OxygenHistoryRepository oxygenHistoryRepository,
                         OxygenHistoryFactory oxygenHistoryFactory,
                         TransactionRepository transactionRepository,
                         TransactionFactory transactionFactory) {
        this.festivalDates = festivalDates;
        this.oxygenInventory = oxygenInventory;
        this.oxygenSupplyMethodFactory = oxygenSupplyMethodFactory;
        this.oxygenHistoryRepository = oxygenHistoryRepository;
        this.oxygenHistoryFactory = oxygenHistoryFactory;
        this.transactionRepository = transactionRepository;
        this.transactionFactory = transactionFactory;
    }

    public void supplyOxygenForAllFestivalDates(LocalDate orderDate, OxygenGrade minimumOxygenGrade, int numberOfTanksDaily) {
        int numberOfTanksNeeded = numberOfTanksDaily * festivalDates.getFestivalDurationInDays();
        supplyOxygen(orderDate, minimumOxygenGrade, numberOfTanksNeeded);
    }

    public void supplyDailyEventOxygen(LocalDate orderDate, OxygenGrade minimumOxygenGrade, int numberOfTanksNeeded) {
        supplyOxygen(orderDate, minimumOxygenGrade, numberOfTanksNeeded);
    }

    public OxygenInventory getInventory() {
        return oxygenInventory;
    }

    public List<OxygenHistory> getAllOxygenHistories() {
        return oxygenHistoryRepository.getAll()
                .stream()
                .sorted()
                .collect(Collectors.toList());
    }

    private void supplyOxygen(LocalDate orderDate, OxygenGrade minimumOxygenGrade, int numberOfTanks) {
        OxygenSupplyMethod oxygenSupplyMethod = oxygenSupplyMethodFactory.getLowestGradeSupplyMethod(minimumOxygenGrade, orderDate);

        OxygenSupplyReport oxygenSupplyReport = supplyNeededOxygen(orderDate, numberOfTanks, minimumOxygenGrade, oxygenSupplyMethod);
        addProducedTanksToHistoryIfNeeded(oxygenSupplyReport.getNumberOfTanksProduced(), orderDate, oxygenSupplyMethod);

        Transaction transaction = transactionFactory.createExpense(oxygenSupplyReport.getSupplyCost());
        transactionRepository.add(transaction);
    }

    private OxygenSupplyReport supplyNeededOxygen(LocalDate orderDate,
                                                  int numberOfTanksNeeded,
                                                  OxygenGrade minimumOxygenGrade,
                                                  OxygenSupplyMethod oxygenSupplyMethod) {

        OxygenGrade oxygenMethodGrade = oxygenSupplyMethod.getOxygenGrade();
        int numberOfNewTanksNeeded = oxygenInventory.useAvailableTanks(numberOfTanksNeeded, minimumOxygenGrade, oxygenMethodGrade);

        OxygenSupplyReport oxygenSupplyReport = OxygenSupplyReport.createNullOxygenSupplyReport();

        if (tanksMustBeSupplied(numberOfNewTanksNeeded)) {
            OxygenHistory history = getOrCreateOxygenHistory(orderDate);
            oxygenSupplyReport = oxygenSupplyMethod.supplyNeededOxygen(history, oxygenInventory, numberOfNewTanksNeeded);
            oxygenHistoryRepository.updateHistory(history);
        }

        oxygenInventory.attributeOxygen(numberOfNewTanksNeeded, oxygenMethodGrade);

        return oxygenSupplyReport;
    }

    private boolean tanksMustBeSupplied(int numberOfNewTanksNeeded) {
        return numberOfNewTanksNeeded > 0;
    }

    private void addProducedTanksToHistoryIfNeeded(int numberOfTanksProduced, LocalDate orderDate, OxygenSupplyMethod oxygenSupplyMethod) {
        if (tanksWereProduced(numberOfTanksProduced)) {
            LocalDate fabricationEndDate = orderDate.plusDays(oxygenSupplyMethod.getFabricationDays());

            OxygenHistory historyOfFabricationEndDay = getOrCreateOxygenHistory(fabricationEndDate);
            historyOfFabricationEndDay.recordOxygenTanksMade(numberOfTanksProduced);

            oxygenHistoryRepository.updateHistory(historyOfFabricationEndDay);
        }
    }

    private boolean tanksWereProduced(int numberOfTanksProduced) {
        return numberOfTanksProduced > 0;
    }

    private OxygenHistory getOrCreateOxygenHistory(LocalDate orderDate) {
        return oxygenHistoryRepository.findByDate(orderDate)
                .orElse(oxygenHistoryFactory.create(orderDate));
    }
}
