package ca.ulaval.glo4002.service.order;

import ca.ulaval.glo4002.domain.event.FestivalDates;
import ca.ulaval.glo4002.domain.event.exception.InvalidEventDateException;
import ca.ulaval.glo4002.domain.order.OrderingPeriod;
import ca.ulaval.glo4002.domain.order.exception.InvalidOrderDateException;

import java.time.LocalDate;
import java.util.List;

public class OrderValidator {

    private final OrderingPeriod orderingPeriod;
    private final FestivalDates festivalDates;

    public OrderValidator(OrderingPeriod orderingPeriod, FestivalDates festivalDates) {
        this.orderingPeriod = orderingPeriod;
        this.festivalDates = festivalDates;
    }

    public void validateOrderDate(LocalDate orderDate) {
        if (!orderingPeriod.isDateInOrderingPeriod(orderDate)) {
            throw new InvalidOrderDateException();
        }
    }

    public void validateDatesAreFestivalDates(List<LocalDate> dates) {
        dates.forEach(this::throwIfDateIsNotInFestivalDates);
    }

    private void throwIfDateIsNotInFestivalDates(LocalDate date) {
        if (!festivalDates.isDateInFestivalDates(date)) {
            throw new InvalidEventDateException();
        }
    }
}
