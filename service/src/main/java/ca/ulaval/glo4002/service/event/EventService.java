package ca.ulaval.glo4002.service.event;

import ca.ulaval.glo4002.domain.event.FestivalDates;
import ca.ulaval.glo4002.domain.order.OrderingPeriod;

import java.time.LocalDate;

public class EventService {

    private final FestivalDates festivalDates;
    private final OrderingPeriod orderingPeriod;

    public EventService(FestivalDates festivalDates, OrderingPeriod orderingPeriod) {
        this.festivalDates = festivalDates;
        this.orderingPeriod = orderingPeriod;
    }

    public void updateFestivalStartDate(LocalDate startDate) { festivalDates.updateFestivalStartDate(startDate); }

    public void updateFestivalEndDate(LocalDate endDate) {
        festivalDates.updateFestivalEndDate(endDate);
    }

    public void updateOrderingPeriodDates(LocalDate startDate) {
        orderingPeriod.updateOrderingPeriodDates(startDate);
    }
}
