package ca.ulaval.glo4002.service.profit;

import ca.ulaval.glo4002.domain.price.Price;
import ca.ulaval.glo4002.domain.profit.IncomeStatement;
import ca.ulaval.glo4002.domain.profit.Transaction;

import java.util.List;

public class IncomeStatementFactory {

    public IncomeStatementFactory() {
    }

    public IncomeStatement create(List<Transaction> transactions) {
        Price revenues = getRevenues(transactions);
        Price expenses = getExpenses(transactions);
        Price profits = calculateProfits(revenues, expenses);
        return new IncomeStatement(revenues, expenses, profits);
    }

    private Price getRevenues(List<Transaction> transactions) {
        return transactions.stream()
                .filter(Transaction::isRevenue)
                .map(Transaction::getPrice)
                .reduce(Price::add)
                .orElse(Price.zero());
    }

    private Price getExpenses(List<Transaction> transactions) {
        return transactions.stream()
                .filter(Transaction::isExpense)
                .map(Transaction::getPrice)
                .reduce(Price::add)
                .orElse(Price.zero());
    }

    private Price calculateProfits(Price revenues, Price expenses) {
        return revenues.subtract(expenses);
    }
}
