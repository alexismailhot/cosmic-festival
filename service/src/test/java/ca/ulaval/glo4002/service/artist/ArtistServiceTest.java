package ca.ulaval.glo4002.service.artist;

import ca.ulaval.glo4002.domain.artist.Artist;
import ca.ulaval.glo4002.domain.artist.ArtistNumber;
import ca.ulaval.glo4002.domain.artist.ArtistPriceComparator;
import ca.ulaval.glo4002.domain.price.Price;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ArtistServiceTest {

    private static final String AN_ARTIST_NAME = "aName";
    private static final int AN_ARTIST_ID = 4;
    private static final ArtistNumber AN_ARTIST_NUMBER = new ArtistNumber(AN_ARTIST_ID);
    private static final int NB_GROUP_MEMBERS = 2;
    private static final Price A_HIGH_PRICE = Price.of(100);
    private static final Price A_MID_RANGE_PRICE = Price.of(10);
    private static final Price A_LOW_PRICE = Price.of(1);
    private static final int A_LOW_POPULARITY = 10;
    private static final int A_HIGH_POPULARITY = 1;
    private static final int A_MID_RANGE_POPULARITY = 5;

    @Mock
    private ArtistRepository artistRepository;

    private ArtistService artistService;

    @BeforeEach
    public void setUp() {
        ArtistPriceComparator artistPriceComparator = new ArtistPriceComparator();
        artistService = new ArtistService(artistRepository, artistPriceComparator);
    }

    @Test
    public void whenGetArtistsByLowCosts_thenArtistsAreOrderedFromHighestPriceToLowestPrice() {
        // when
        Artist artistWithHighPrice = createArtist(A_HIGH_PRICE);
        Artist artistWithLowPrice = createArtist(A_LOW_PRICE);
        Artist artistWithMidRangePrice = createArtist(A_MID_RANGE_PRICE);

        List<Artist> artists = Arrays.asList(artistWithMidRangePrice, artistWithHighPrice, artistWithLowPrice);
        when(artistRepository.getArtists()).thenReturn(artists);

        List<Artist> sortedArtists = artistService.getArtistsByLowCosts();

        // then
        assertThat(sortedArtists).containsExactly(artistWithHighPrice, artistWithMidRangePrice, artistWithLowPrice).inOrder();
    }

    @Test
    public void whenGetArtistsByLowCostsAndArtistsHaveSamePrice_thenArtistsAreOrderedFromLeastPopularToMostPopular() {
        // when
        Artist artistWithLowPopularity = createArtist(A_LOW_POPULARITY);
        Artist artistWithMidRangePopularity = createArtist(A_MID_RANGE_POPULARITY);
        Artist artistWithHighPopularity = createArtist(A_HIGH_POPULARITY);

        List<Artist> artists = Arrays.asList(artistWithLowPopularity, artistWithMidRangePopularity, artistWithHighPopularity);
        when(artistRepository.getArtists()).thenReturn(artists);

        List<Artist> sortedArtists = artistService.getArtistsByLowCosts();

        // then
        assertThat(sortedArtists).containsExactly(artistWithLowPopularity, artistWithMidRangePopularity, artistWithHighPopularity)
                .inOrder();
    }


    @Test
    public void whenGetArtistsByPopularity_thenArtistsAreOrderedFromMostPopularToLeastPopular() {
        // when
        Artist artistWithHighPopularity = createArtist(A_HIGH_POPULARITY);
        Artist artistWithLowPopularity = createArtist(A_LOW_POPULARITY);
        Artist artistWithMidRangePopularity = createArtist(A_MID_RANGE_POPULARITY);

        List<Artist> artists = Arrays.asList(artistWithMidRangePopularity, artistWithHighPopularity, artistWithLowPopularity);
        when(artistRepository.getArtists()).thenReturn(artists);

        List<Artist> sortedArtists = artistService.getArtistsByPopularity();

        // then
        assertThat(sortedArtists).containsExactly(artistWithHighPopularity, artistWithMidRangePopularity, artistWithLowPopularity)
                .inOrder();
    }

    @Test
    public void whenFindArtistByNameAndArtistIsNotFound_thenReturnEmptyOptional() {
        // when
        when(artistRepository.getArtists()).thenReturn(Collections.emptyList());

        Optional<Artist> artist = artistService.findArtistByName(AN_ARTIST_NAME);

        // then
        assertThat(artist).isEqualTo(Optional.empty());
    }

    @Test
    public void whenFindArtistByNameAndArtistIsFound_thenReturnOptionalOfThisArtist() {
        // when
        Artist artist = createArtist();
        when(artistRepository.getArtists()).thenReturn(Collections.singletonList(artist));

        Optional<Artist> foundArtist = artistService.findArtistByName(AN_ARTIST_NAME);

        // then
        assertThat(foundArtist.get()).isEqualTo(artist);
    }

    private Artist createArtist(Price price) {
        return new Artist(AN_ARTIST_NUMBER, AN_ARTIST_NAME, price, A_LOW_POPULARITY, NB_GROUP_MEMBERS);
    }

    private Artist createArtist(int popularityRank) {
        return new Artist(AN_ARTIST_NUMBER, AN_ARTIST_NAME, A_HIGH_PRICE, popularityRank, NB_GROUP_MEMBERS);
    }

    private Artist createArtist() {
        return new Artist(AN_ARTIST_NUMBER, AN_ARTIST_NAME, A_HIGH_PRICE, A_LOW_POPULARITY, NB_GROUP_MEMBERS);
    }
}
