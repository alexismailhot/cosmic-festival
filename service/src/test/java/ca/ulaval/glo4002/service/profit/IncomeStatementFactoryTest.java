package ca.ulaval.glo4002.service.profit;

import ca.ulaval.glo4002.domain.price.Price;
import ca.ulaval.glo4002.domain.profit.IncomeStatement;
import ca.ulaval.glo4002.domain.profit.Transaction;
import ca.ulaval.glo4002.domain.profit.TransactionType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(MockitoExtension.class)
public class IncomeStatementFactoryTest {

    private static final Transaction A_REVENUE_TRANSACTION = new Transaction(Price.of(40), TransactionType.REVENUE) ;
    private static final Transaction AN_EXPENSE_TRANSACTION = new Transaction(Price.of(20), TransactionType.EXPENSE);
    private static final int A_NUMBER_OF_TRANSACTIONS = 2;

    private IncomeStatementFactory incomeStatementFactory;

    @BeforeEach
    void setUp() {
        incomeStatementFactory = new IncomeStatementFactory();
    }

   @Test
   void givenNoTransactions_whenCreate_thenReturnedIncomeStatementIsEmpty() {
        // given
       List<Transaction> anEmptyTransactionList = Collections.emptyList();

       // when
       IncomeStatement incomeStatement = incomeStatementFactory.create(anEmptyTransactionList);

       // then
       assertThat(incomeStatement.getRevenues()).isEquivalentAccordingToCompareTo(Price.of(0));
       assertThat(incomeStatement.getExpenses()).isEquivalentAccordingToCompareTo(Price.of(0));
       assertThat(incomeStatement.getProfits()).isEquivalentAccordingToCompareTo(Price.of(0));
   }

   @Test
   void givenRevenues_whenCreate_thenReturnedIncomeStatementContainsTotalOfRevenues() {
        // given
        List<Transaction> aTransactionListWithRevenues = Collections.nCopies(A_NUMBER_OF_TRANSACTIONS, A_REVENUE_TRANSACTION);
        Price totalRevenue = A_REVENUE_TRANSACTION.getPrice().multiplyBy(A_NUMBER_OF_TRANSACTIONS);

        // when
        IncomeStatement incomeStatement = incomeStatementFactory.create(aTransactionListWithRevenues);

        // then
        assertThat(incomeStatement.getRevenues()).isEquivalentAccordingToCompareTo(totalRevenue);
   }

    @Test
    void givenExpenses_whenCreate_thenReturnedIncomeStatementContainsTotalOfExpenses() {
        // given
        List<Transaction> aTransactionListWithExpense = Collections.nCopies(A_NUMBER_OF_TRANSACTIONS, AN_EXPENSE_TRANSACTION);
        Price totalExpenses = AN_EXPENSE_TRANSACTION.getPrice().multiplyBy(A_NUMBER_OF_TRANSACTIONS);

        // when
        IncomeStatement incomeStatement = incomeStatementFactory.create(aTransactionListWithExpense);

        // then
        assertThat(incomeStatement.getExpenses()).isEquivalentAccordingToCompareTo(totalExpenses);
    }

    @Test
    void givenRevenuesAndExpenses_whenCreate_thenReturnedIncomeStatementContainsTotalProfit(){
        // given
        List<Transaction> aTransactionListWithRevenuesAndExpenses = Arrays.asList(A_REVENUE_TRANSACTION, AN_EXPENSE_TRANSACTION);
        Price totalProfit = A_REVENUE_TRANSACTION.getPrice().subtract(AN_EXPENSE_TRANSACTION.getPrice());

        // when
        IncomeStatement incomeStatement = incomeStatementFactory.create(aTransactionListWithRevenuesAndExpenses);

        // then
        assertThat(incomeStatement.getProfits()).isEquivalentAccordingToCompareTo(totalProfit);
    }
}
