package ca.ulaval.glo4002.service.oxygen;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ca.ulaval.glo4002.domain.event.FestivalDates;
import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;
import ca.ulaval.glo4002.domain.oxygen.exception.OxygenRequirementsException;
import ca.ulaval.glo4002.domain.oxygen.factory.OxygenTankFactory;
import ca.ulaval.glo4002.domain.oxygen.method.CandlesOxygenSupplyMethod;
import ca.ulaval.glo4002.domain.oxygen.method.OxygenSupplyMethod;
import ca.ulaval.glo4002.domain.price.Price;

@ExtendWith(MockitoExtension.class)
class OxygenSupplyMethodFactoryTest {

    private static final LocalDate AN_ORDER_DATE = LocalDate.of(2012, 10, 1);
    private static final LocalDate FESTIVAL_START_DATE = LocalDate.of(2012, 10, 24);
    private static final OxygenGrade HIGHEST_GRADE = OxygenGrade.GRADE_E;
    private static final OxygenGrade MIDDLE_GRADE = OxygenGrade.GRADE_B;
    private static final OxygenGrade LOWEST_GRADE = OxygenGrade.GRADE_A;
    private static final int A_NUMBER_OF_TANKS_PRODUCED = 2;
    private static final int A_FABRICATION_TIME_TOO_LONG = 24;
    private static final int AN_ACCEPTABLE_FABRICATION_TIME = 10;
    private static final int A_NUMBER_OF_CANDLES_USED = 1;
    private static final Price A_FABRICATION_COST = Price.of(1500);

    private OxygenSupplyMethodFactory oxygenSupplyMethodFactory;

    @Mock
    private FestivalDates festivalDates;

    @Test
    public void whenGetLowestGradeSupplyMethodAndAllMethodsMeetGradeAndTimeRequirements_thenReturnMethodWithLowestGrade() {
        // when
        OxygenSupplyMethod highestGradeMethod = createSupplyMethodThatMeetsTimeRequirement(HIGHEST_GRADE);
        OxygenSupplyMethod middleGradeMethod = createSupplyMethodThatMeetsTimeRequirement(MIDDLE_GRADE);
        OxygenSupplyMethod lowestGradeMethod = createSupplyMethodThatMeetsTimeRequirement(LOWEST_GRADE);

        setUpOxygenSupplyMethodProviderWithMethods(highestGradeMethod, middleGradeMethod, lowestGradeMethod);

        OxygenSupplyMethod oxygenSupplyMethod = oxygenSupplyMethodFactory.getLowestGradeSupplyMethod(LOWEST_GRADE, AN_ORDER_DATE);

        // then
        assertThat(oxygenSupplyMethod).isEqualTo(lowestGradeMethod);
    }

    @Test
    public void whenGetLowestGradeSupplyMethodAndAllMethodsMeetTimeRequirement_thenReturnMethodWithLowestGradeThatMeetsGradeRequirement() {
        // when
        OxygenSupplyMethod meetsGradeRequirementMethod = createSupplyMethodThatMeetsTimeRequirement(HIGHEST_GRADE);
        OxygenSupplyMethod lowestGradeToMeetGradeRequirementMethod = createSupplyMethodThatMeetsTimeRequirement(MIDDLE_GRADE);
        OxygenSupplyMethod doesNotMeetGradeRequirementMethod = createSupplyMethodThatMeetsTimeRequirement(LOWEST_GRADE);

        setUpOxygenSupplyMethodProviderWithMethods(meetsGradeRequirementMethod, lowestGradeToMeetGradeRequirementMethod,
                doesNotMeetGradeRequirementMethod);

        OxygenSupplyMethod oxygenSupplyMethod = oxygenSupplyMethodFactory.getLowestGradeSupplyMethod(MIDDLE_GRADE, AN_ORDER_DATE);

        // then
        assertThat(oxygenSupplyMethod).isEqualTo(lowestGradeToMeetGradeRequirementMethod);
    }

    @Test
    public void whenGetLowestGradeSupplyMethodAndAllMethodsMeetGradeRequirement_thenReturnMethodWithLowestGradeThatMeetsTimeRequirement() {
        // when
        OxygenSupplyMethod meetsTimeRequirementMethod = createSupplyMethodThatMeetsTimeRequirement(HIGHEST_GRADE);
        OxygenSupplyMethod lowestGradeToMeetTimeRequirementMethod = createSupplyMethodThatMeetsTimeRequirement(MIDDLE_GRADE);
        OxygenSupplyMethod doesNotMeetTimeRequirementMethod = createSupplyMethodThatDoesNotMeetTimeRequirement(LOWEST_GRADE);

        setUpOxygenSupplyMethodProviderWithMethods(meetsTimeRequirementMethod, lowestGradeToMeetTimeRequirementMethod,
                doesNotMeetTimeRequirementMethod);

        OxygenSupplyMethod oxygenSupplyMethod = oxygenSupplyMethodFactory.getLowestGradeSupplyMethod(LOWEST_GRADE, AN_ORDER_DATE);

        // then
        assertThat(oxygenSupplyMethod).isEqualTo(lowestGradeToMeetTimeRequirementMethod);
    }

    @Test
    public void whenNoMethodMeetsGradeRequirement_thenThrowOxygenRequirementsException() {
        // then
        Assertions.assertThrows(OxygenRequirementsException.class, () -> {
            // when
            OxygenSupplyMethod doesNotMeetGradeRequirementMethod = createSupplyMethodThatMeetsTimeRequirement(LOWEST_GRADE);

            oxygenSupplyMethodFactory = new OxygenSupplyMethodFactory(festivalDates,
                    Collections.singletonList(doesNotMeetGradeRequirementMethod));

            oxygenSupplyMethodFactory.getLowestGradeSupplyMethod(MIDDLE_GRADE, AN_ORDER_DATE);
        });
    }

    @Test
    public void whenNoMethodMeetsTimeRequirement_thenThrowOxygenRequirementsException() {
        // then
        Assertions.assertThrows(OxygenRequirementsException.class, () -> {
            // when
            OxygenSupplyMethod doesNotMeetTimeRequirementMethod = createSupplyMethodThatDoesNotMeetTimeRequirement(HIGHEST_GRADE);

            setUpOxygenSupplyMethodProvider(Collections.singletonList(doesNotMeetTimeRequirementMethod));

            oxygenSupplyMethodFactory.getLowestGradeSupplyMethod(MIDDLE_GRADE, AN_ORDER_DATE);
        });
    }

    private OxygenSupplyMethod createSupplyMethodThatMeetsTimeRequirement(OxygenGrade oxygenGrade) {
        return createOxygenSupplyMethod(oxygenGrade, AN_ACCEPTABLE_FABRICATION_TIME);
    }

    private OxygenSupplyMethod createSupplyMethodThatDoesNotMeetTimeRequirement(OxygenGrade oxygenGrade) {
        return createOxygenSupplyMethod(oxygenGrade, A_FABRICATION_TIME_TOO_LONG);
    }

    private OxygenSupplyMethod createOxygenSupplyMethod(OxygenGrade oxygenGrade, int fabricationTime) {
        return new CandlesOxygenSupplyMethod(new OxygenTankFactory(), oxygenGrade, A_NUMBER_OF_TANKS_PRODUCED, fabricationTime,
                A_NUMBER_OF_CANDLES_USED, A_FABRICATION_COST);
    }

    private void setUpOxygenSupplyMethodProviderWithMethods(OxygenSupplyMethod firstMethod,
                                                            OxygenSupplyMethod secondMethod,
                                                            OxygenSupplyMethod lastMethod) {
        List<OxygenSupplyMethod> oxygenSupplyMethods = Arrays.asList(firstMethod, secondMethod, lastMethod);
        setUpOxygenSupplyMethodProvider(oxygenSupplyMethods);
    }

    private void setUpOxygenSupplyMethodProvider(List<OxygenSupplyMethod> oxygenSupplyMethods) {
        oxygenSupplyMethodFactory = new OxygenSupplyMethodFactory(festivalDates, oxygenSupplyMethods);
        when(festivalDates.getFestivalStartDate()).thenReturn(FESTIVAL_START_DATE);
    }
}
