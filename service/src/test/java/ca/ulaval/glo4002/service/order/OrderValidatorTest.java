package ca.ulaval.glo4002.service.order;

import ca.ulaval.glo4002.domain.event.FestivalDates;
import ca.ulaval.glo4002.domain.event.exception.InvalidEventDateException;
import ca.ulaval.glo4002.domain.order.OrderingPeriod;
import ca.ulaval.glo4002.domain.order.exception.InvalidOrderDateException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OrderValidatorTest {

    private static final LocalDate AN_ORDER_DATE = LocalDate.of(2091, 1, 1);
    private static final LocalDate A_DATE = LocalDate.of(2019, 1, 1);
    private static final List<LocalDate> SOME_DATES = Arrays.asList(A_DATE, A_DATE);

    @Mock
    private OrderingPeriod orderingPeriod;

    @Mock
    private FestivalDates festivalDates;

    private OrderValidator orderValidator;

    @BeforeEach
    public void setUp() {
        orderValidator = new OrderValidator(orderingPeriod, festivalDates);
    }

    @Test
    public void whenValidateOrderDateTimeAndOrderDateTimeIsNotInOrderingPeriod_thenDoNotThrowAnyException() {
        // then
        Assertions.assertDoesNotThrow(() -> {
            // when
            when(orderingPeriod.isDateInOrderingPeriod(AN_ORDER_DATE)).thenReturn(true);
            orderValidator.validateOrderDate(AN_ORDER_DATE);
        });
    }

    @Test
    public void whenValidateOrderDateTimeAndOrderDateTimeIsNotInOrderingPeriod_thenThrowInvalidOrderException() {
        // then
        Assertions.assertThrows(InvalidOrderDateException.class, () -> {
            // when
            when(orderingPeriod.isDateInOrderingPeriod(AN_ORDER_DATE)).thenReturn(false);
            orderValidator.validateOrderDate(AN_ORDER_DATE);
        });
    }

    @Test
    public void whenDatesAreFestivalDatesAndAtLeastOneDateIsNotInFestivalDates_thenDoNotThrowAnyException() {
        // then
        Assertions.assertDoesNotThrow(() -> {
            // when
            when(festivalDates.isDateInFestivalDates(A_DATE)).thenReturn(true);
            orderValidator.validateDatesAreFestivalDates(SOME_DATES);
        });
    }

    @Test
    public void whenDatesAreFestivalDatesAndAtLeastOneDateIsNotInFestivalDates_thenThrowInvalidEventDateException() {
        // then
        Assertions.assertThrows(InvalidEventDateException.class, () -> {
            // when
            when(festivalDates.isDateInFestivalDates(A_DATE)).thenReturn(false);
            orderValidator.validateDatesAreFestivalDates(SOME_DATES);
        });
    }
}
