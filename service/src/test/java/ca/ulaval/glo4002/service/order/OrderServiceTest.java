package ca.ulaval.glo4002.service.order;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import ca.ulaval.glo4002.domain.profit.repository.TransactionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ca.ulaval.glo4002.domain.discount.OrderDiscount;
import ca.ulaval.glo4002.domain.discount.PassDiscount;
import ca.ulaval.glo4002.domain.order.Order;
import ca.ulaval.glo4002.domain.order.OrderNumber;
import ca.ulaval.glo4002.domain.order.VendorCode;
import ca.ulaval.glo4002.domain.order.exception.OrderNotFoundException;
import ca.ulaval.glo4002.domain.order.factory.OrderFactory;
import ca.ulaval.glo4002.domain.order.repository.OrderRepository;
import ca.ulaval.glo4002.domain.pass.EventPass;
import ca.ulaval.glo4002.domain.pass.PassCategory;
import ca.ulaval.glo4002.domain.pass.PassNumber;
import ca.ulaval.glo4002.domain.price.Price;
import ca.ulaval.glo4002.domain.profit.Transaction;
import ca.ulaval.glo4002.domain.profit.factory.TransactionFactory;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {

    private static final PassNumber A_PASS_NUMBER = new PassNumber(0L);
    private static final OrderNumber AN_ORDER_NUMBER = new OrderNumber("an order number");
    private static final PassCategory A_PASS_CATEGORY = PassCategory.SUPERGIANT;
    private static final Price A_PRICE = Price.of(100);
    private static final EventPass EVENT_PASS = new EventPass(A_PASS_NUMBER, A_PASS_CATEGORY, A_PRICE);
    private static final List<EventPass> EVENT_PASSES = Collections.singletonList(EVENT_PASS);
    private static final VendorCode A_VENDOR_CODE = VendorCode.TEAM;
    private static final List<OrderDiscount> SOME_ORDER_DISCOUNTS = Collections.emptyList();
    private static final List<PassDiscount> SOME_PASS_DISCOUNTS = Collections.emptyList();

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private TransactionRepository transactionRepository;

    @Mock
    private OrderFactory orderFactory;

    private OrderService orderService;

    @BeforeEach
    public void setUp() {
        orderService = new OrderService(orderFactory, orderRepository, transactionRepository, new TransactionFactory());
    }

    @Test
    public void whenCreateOrder_thenOrderFactoryIsCalledToCreateOrderWithEventPasses() {
        // when
        when(orderFactory.create(A_VENDOR_CODE, EVENT_PASSES)).thenReturn(createDefaultOrder());
        orderService.createOrder(A_VENDOR_CODE, EVENT_PASSES);

        // then
        verify(orderFactory).create(A_VENDOR_CODE, EVENT_PASSES);
    }

    @Test
    public void givenOrder_whenCreateOrder_thenOrderRepositoryIsCalledToPersistCreatedOrder() {
        // given
        Order order = createDefaultOrder();

        // when
        when(orderFactory.create(A_VENDOR_CODE, EVENT_PASSES)).thenReturn(order);
        orderService.createOrder(A_VENDOR_CODE, EVENT_PASSES);

        // then
        verify(orderRepository).add(order);
    }

    @Test
    public void givenOrder_whenCreateOrder_thenTransactionRepositoryIsCalledToAddTransaction() {
        // given
        Order order = createDefaultOrder();

        // when
        when(orderFactory.create(A_VENDOR_CODE, EVENT_PASSES)).thenReturn(order);
        orderService.createOrder(A_VENDOR_CODE, EVENT_PASSES);

        // then
        ArgumentCaptor<Transaction> transactionArgumentCaptor = ArgumentCaptor.forClass(Transaction.class);
        verify(transactionRepository).add(transactionArgumentCaptor.capture());
        assertThat(transactionArgumentCaptor.getValue().getPrice()).isEqualTo(order.calculateTotalPrice());
    }

    @Test
    public void givenOrder_whenGetOrderAndOrderIsFoundInOrderRepository_thenReturnOrder() {
        // given
        Order order = createDefaultOrder();

        // when
        when(orderRepository.findByOrderNumber(AN_ORDER_NUMBER)).thenReturn(Optional.of(order));
        Order returnedOrder = orderService.getOrder(AN_ORDER_NUMBER);

        // then
        assertThat(returnedOrder).isEqualTo(order);
    }

    @Test
    public void whenGetOrderAndOrderIsNotFoundInOrderRepository_thenThrowOrderNotFoundException() {
        // then
        Assertions.assertThrows(OrderNotFoundException.class, () -> {
            // when
            orderService.getOrder(AN_ORDER_NUMBER);
        });
    }

    private Order createDefaultOrder() {
        return new Order(AN_ORDER_NUMBER, EVENT_PASSES, SOME_ORDER_DISCOUNTS, SOME_PASS_DISCOUNTS);
    }
}
