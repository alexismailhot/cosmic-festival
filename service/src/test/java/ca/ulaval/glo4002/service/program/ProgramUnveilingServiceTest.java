package ca.ulaval.glo4002.service.program;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import ca.ulaval.glo4002.domain.profit.repository.TransactionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ca.ulaval.glo4002.domain.artist.Artist;
import ca.ulaval.glo4002.domain.artist.ArtistNumber;
import ca.ulaval.glo4002.domain.artist.ArtistOxygenNeeds;
import ca.ulaval.glo4002.domain.artist.ArtistShuttleType;
import ca.ulaval.glo4002.domain.event.FestivalDates;
import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;
import ca.ulaval.glo4002.domain.pass.repository.EventPassRepository;
import ca.ulaval.glo4002.domain.price.Price;
import ca.ulaval.glo4002.domain.profit.Transaction;
import ca.ulaval.glo4002.domain.profit.factory.TransactionFactory;
import ca.ulaval.glo4002.domain.program.EventActivity;
import ca.ulaval.glo4002.domain.program.exception.InvalidProgramException;
import ca.ulaval.glo4002.domain.transport.PassengerNumber;
import ca.ulaval.glo4002.domain.transport.ShuttleType;
import ca.ulaval.glo4002.service.artist.ArtistService;
import ca.ulaval.glo4002.service.oxygen.OxygenService;
import ca.ulaval.glo4002.service.program.assembler.DailyProgramAssembler;
import ca.ulaval.glo4002.domain.transport.TransportBooker;


@ExtendWith(MockitoExtension.class)
public class ProgramUnveilingServiceTest {

    private static final int ARTIST_ID = 6;
    private static final ArtistNumber AN_ARTIST_NUMBER = new ArtistNumber(ARTIST_ID);
    private static final EventActivity AN_ACTIVITY = EventActivity.CARDIO_SPACE;
    private static final String AN_ARTIST_NAME = "Coldray";
    private static final Price A_PRICE = Price.of(6000);
    private static final int A_POPULARITY = 5;
    private static final int MANY_GROUP_MEMBERS = 3;
    private static final LocalDate AN_EVENT_DATE = LocalDate.of(2050, 7, 17);
    private static final LocalDate A_FESTIVAL_PROGRAM_UNVEILING_DATE = LocalDate.of(2050, 7, 12);
    private static final int A_NUMBER_OF_PARTICIPANTS = 42;
    private static final Artist AN_ARTIST = createArtist(MANY_GROUP_MEMBERS);
    private static final int ONE_GROUPER_MEMBER = 1;
    private static final String UNKNOWN_ARTIST_NAME = "";

    private ProgramUnveilingService programUnveilingService;

    @Mock
    private TransportBooker transportBooker;

    @Mock
    private ArtistService artistService;

    @Mock
    private EventPassRepository eventPassRepository;

    @Mock
    private OxygenService oxygenService;

    @Mock
    private ProgramValidator programValidator;

    @Mock
    private FestivalDates festivalDates;

    @Mock
    private ArtistOxygenNeeds artistOxygenNeeds;

    @Mock
    private ArtistShuttleType artistShuttleType;

    @Mock
    private TransactionRepository transactionRepository;

    @BeforeEach
    public void setUp() {
        programUnveilingService = new ProgramUnveilingService(transportBooker, artistService, oxygenService, transactionRepository,
                eventPassRepository,
                festivalDates,
                programValidator,
                new DailyProgramAssembler(),
                artistOxygenNeeds,
                artistShuttleType,
                new TransactionFactory());
    }

    @Test
    public void whenSetFestivalProgramAndManyGroupMembers_thenMilleniumFalconShuttleIsBookedForAllGroupMembers() {
        // when
        whenFindArtistByNameThenReturnAnArtist();
        when(artistShuttleType.getShuttleTypeFor(MANY_GROUP_MEMBERS)).thenReturn(ShuttleType.MILLENIUM_FALCON);
        programUnveilingService.setFestivalProgram(createProgramSubmission());

        // then
        List<PassengerNumber> expectedListOfPassengerNumbers = Collections.nCopies(MANY_GROUP_MEMBERS, AN_ARTIST_NUMBER);
        verify(transportBooker).bookDailyEventTransportForAGroup(AN_EVENT_DATE, expectedListOfPassengerNumbers,
                ShuttleType.MILLENIUM_FALCON);
    }

    @Test
    public void whenSetFestivalProgramAndOneGroupMember_thenEtSpaceshipShuttleIsBookedForArtist() {
        // when
        Artist artist = createArtist(ONE_GROUPER_MEMBER);
        when(artistService.findArtistByName(AN_ARTIST_NAME)).thenReturn(Optional.of(artist));
        when(artistShuttleType.getShuttleTypeFor(ONE_GROUPER_MEMBER)).thenReturn(ShuttleType.ET_SPACESHIP);
        programUnveilingService.setFestivalProgram(createProgramSubmission());

        // then
        List<PassengerNumber> expectedListOfPassengerNumbers = Collections.singletonList(AN_ARTIST_NUMBER);
        verify(transportBooker).bookDailyEventTransportForAGroup(AN_EVENT_DATE, expectedListOfPassengerNumbers, ShuttleType.ET_SPACESHIP);
    }

    @Test
    public void whenSetFestivalProgramWithUnknownArtist_thenExceptionIsThrown() {
        // then
        Assertions.assertThrows(InvalidProgramException.class, () -> {
            // when
            programUnveilingService.setFestivalProgram(createProgramSubmissionWithUnknownArtistName());
        });
    }

    @Test
    public void whenSetFestivalProgram_thenOxygenIsOrderedForAllGroupMembers() {
        // when
        when(festivalDates.getFestivalProgramUnveilingDate()).thenReturn(A_FESTIVAL_PROGRAM_UNVEILING_DATE);
        whenFindArtistByNameThenReturnAnArtist();

        programUnveilingService.setFestivalProgram(createProgramSubmission());

        // then
        OxygenGrade expectedOxygenGrade = artistOxygenNeeds.getMinimumOxygenGradeNeeded();
        int expectedNumberOfTanks = artistOxygenNeeds.getNumberOfOxygenTanksNeededFor(AN_ARTIST.getNumberGroupMembers());
        verify(oxygenService).supplyDailyEventOxygen(A_FESTIVAL_PROGRAM_UNVEILING_DATE, expectedOxygenGrade, expectedNumberOfTanks);
    }

    @Test
    public void whenSetFestivalProgram_thenOxygenIsOrderedForAllActivityParticipants() {
        // when
        when(festivalDates.getFestivalProgramUnveilingDate()).thenReturn(A_FESTIVAL_PROGRAM_UNVEILING_DATE);
        when(eventPassRepository.getCountByDate(any())).thenReturn(A_NUMBER_OF_PARTICIPANTS);
        whenFindArtistByNameThenReturnAnArtist();
        programUnveilingService.setFestivalProgram(createProgramSubmission());

        // then
        OxygenGrade expectedOxygenGrade = AN_ACTIVITY.getOxygenGradeNeeded();
        int expectedNumberOfTanks = AN_ACTIVITY.calculateNumberOfOxygenTanksNeeded(A_NUMBER_OF_PARTICIPANTS);
        verify(oxygenService).supplyDailyEventOxygen(A_FESTIVAL_PROGRAM_UNVEILING_DATE, expectedOxygenGrade, expectedNumberOfTanks);
    }

    @Test
    public void whenSetFestivalProgram_thenEventPassRepositoryIsCalledToGetNumberOfParticipants() {
        // when
        whenFindArtistByNameThenReturnAnArtist();
        programUnveilingService.setFestivalProgram(createProgramSubmission());

        // then
        verify(eventPassRepository).getCountByDate(AN_EVENT_DATE);
    }

    @Test
    public void wheSetFestivalProgram_thenArtistServiceIsCalledToFindArtistByName() {
        // when
        whenFindArtistByNameThenReturnAnArtist();
        programUnveilingService.setFestivalProgram(createProgramSubmission());

        // then
        verify(artistService).findArtistByName(AN_ARTIST_NAME);
    }

    @Test
    public void whenSetFestivalProgram_thenArtistIsAddedToExpense() {
        // when
        whenFindArtistByNameThenReturnAnArtist();
        programUnveilingService.setFestivalProgram(createProgramSubmission());

        // then
        Price expectedPrice = AN_ARTIST.getPrice();
        ArgumentCaptor<Transaction> transactionArgumentCaptor = ArgumentCaptor.forClass(Transaction.class);
        verify(transactionRepository).add(transactionArgumentCaptor.capture());
        assertThat(transactionArgumentCaptor.getValue().getPrice()).isEqualTo(expectedPrice);
    }

    private void whenFindArtistByNameThenReturnAnArtist() {
        when(artistService.findArtistByName(AN_ARTIST_NAME)).thenReturn(Optional.of(AN_ARTIST));
    }

    private static Artist createArtist(int numberOfGrouperMembers) {
        return new Artist(AN_ARTIST_NUMBER, AN_ARTIST_NAME, A_PRICE, A_POPULARITY, numberOfGrouperMembers);
    }

    private ProgramSubmission createProgramSubmission() {
        List<DailyProgramSubmission> dailyProgramSubmissions = Collections.singletonList(new DailyProgramSubmission(
                AN_EVENT_DATE, AN_ACTIVITY, AN_ARTIST_NAME));
        return new ProgramSubmission(dailyProgramSubmissions);
    }

    private ProgramSubmission createProgramSubmissionWithUnknownArtistName() {
        List<DailyProgramSubmission> dailyProgramSubmissions = Collections.singletonList(new DailyProgramSubmission(
                AN_EVENT_DATE, AN_ACTIVITY, UNKNOWN_ARTIST_NAME));
        return new ProgramSubmission(dailyProgramSubmissions);
    }
}
