package ca.ulaval.glo4002.service.oxygen;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import ca.ulaval.glo4002.domain.profit.repository.TransactionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ca.ulaval.glo4002.domain.event.FestivalDates;
import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;
import ca.ulaval.glo4002.domain.oxygen.OxygenHistory;
import ca.ulaval.glo4002.domain.oxygen.OxygenInventory;
import ca.ulaval.glo4002.domain.oxygen.factory.OxygenHistoryFactory;
import ca.ulaval.glo4002.domain.oxygen.factory.OxygenTankFactory;
import ca.ulaval.glo4002.domain.oxygen.method.OxygenSupplyMethod;
import ca.ulaval.glo4002.domain.oxygen.method.PremadeOxygenSupplyMethod;
import ca.ulaval.glo4002.domain.oxygen.repository.OxygenHistoryRepository;
import ca.ulaval.glo4002.domain.price.Price;
import ca.ulaval.glo4002.domain.profit.Transaction;
import ca.ulaval.glo4002.domain.profit.factory.TransactionFactory;

@ExtendWith(MockitoExtension.class)
class OxygenServiceTest {

    private static final OxygenGrade A_GRADE = OxygenGrade.GRADE_B;
    private static final int A_NUMBER_OF_TANKS_BOUGHT = 2;
    private static final int A_NUMBER_OF_TANKS_NEEDED_DAILY = 123;
    private static final Price A_PURCHASE_COST = Price.of(5000);
    private static final int A_NUMBER_OF_TANKS_PRODUCED = 2;
    private static final int FABRICATION_DAYS = 2;
    private static final OxygenSupplyMethod AN_OXYGEN_SUPPLY_METHOD = new PremadeOxygenSupplyMethod(new OxygenTankFactory(), A_GRADE,
            A_NUMBER_OF_TANKS_PRODUCED, FABRICATION_DAYS, A_NUMBER_OF_TANKS_BOUGHT, A_PURCHASE_COST);
    private static final LocalDate A_DATE = LocalDate.of(2050, 7, 12);
    private static final OxygenHistory AN_OXYGEN_HISTORY = new OxygenHistory(A_DATE);
    private static final LocalDate EARLIEST_DATE = LocalDate.of(2010, 1, 1);
    private static final LocalDate LATEST_DATE = LocalDate.of(2020, 1, 2);
    private static final LocalDate DATE_IN_THE_MIDDLE = LocalDate.of(2014, 3, 3);

    @Mock
    private FestivalDates festivalDates;

    @Mock
    private OxygenHistoryRepository oxygenHistoryRepository;

    @Mock
    private OxygenSupplyMethodFactory oxygenSupplyMethodFactory;

    @Mock
    private TransactionRepository transactionRepository;

    @Mock
    private OxygenInventory oxygenInventory;

    private OxygenService oxygenService;

    @BeforeEach
    public void setUp() {
        oxygenService = new OxygenService(
                festivalDates,
                oxygenInventory,
                oxygenSupplyMethodFactory,
                oxygenHistoryRepository,
                new OxygenHistoryFactory(),
                transactionRepository,
                new TransactionFactory()
        );
    }

    @Test
    public void whenSupplyEventOxygen_thenOxygenHistoryRepositoryIsCalledToGetOxygenHistoryForGivenDate() {
        // given
        givenOxygenHistoryInventoryAndSupplyMethodProviderReturnHistoryInventoryAndSupplyMethod();
        when(oxygenInventory.useAvailableTanks(0, A_GRADE, A_GRADE)).thenReturn(1);

        // when
        oxygenService.supplyOxygenForAllFestivalDates(A_DATE, A_GRADE, A_NUMBER_OF_TANKS_NEEDED_DAILY);

        // then
        verify(oxygenHistoryRepository).findByDate(A_DATE);
    }

    @Test
    public void whenSupplyDailyEventOxygen_thenOxygenHistoryRepositoryIsCalledToGetOxygenHistoryForGivenDate() {
        // given
        givenOxygenHistoryInventoryAndSupplyMethodProviderReturnHistoryInventoryAndSupplyMethod();
        when(oxygenInventory.useAvailableTanks(A_NUMBER_OF_TANKS_NEEDED_DAILY, A_GRADE, A_GRADE)).thenReturn(1);

        // when
        oxygenService.supplyDailyEventOxygen(A_DATE, A_GRADE, A_NUMBER_OF_TANKS_NEEDED_DAILY);

        // then
        verify(oxygenHistoryRepository).findByDate(A_DATE);
    }

    @Test
    public void whenSupplyEventOxygen_thenOxygenHistoryRepositoryIsCalledToUpdateOxygenHistory() {
        // given
        givenOxygenHistoryInventoryAndSupplyMethodProviderReturnHistoryInventoryAndSupplyMethod();
        when(oxygenInventory.useAvailableTanks(0, A_GRADE, A_GRADE)).thenReturn(1);

        // when
        oxygenService.supplyOxygenForAllFestivalDates(A_DATE, A_GRADE, A_NUMBER_OF_TANKS_NEEDED_DAILY);

        // then
        verify(oxygenHistoryRepository).updateHistory(AN_OXYGEN_HISTORY);
    }

    @Test
    public void whenSupplyDailyEventOxygen_thenOxygenHistoryRepositoryIsCalledToUpdateOxygenHistory() {
        // given
        givenOxygenHistoryInventoryAndSupplyMethodProviderReturnHistoryInventoryAndSupplyMethod();
        when(oxygenInventory.useAvailableTanks(A_NUMBER_OF_TANKS_NEEDED_DAILY, A_GRADE, A_GRADE)).thenReturn(1);

        // when
        oxygenService.supplyDailyEventOxygen(A_DATE, A_GRADE, A_NUMBER_OF_TANKS_NEEDED_DAILY);

        // then
        verify(oxygenHistoryRepository).updateHistory(AN_OXYGEN_HISTORY);
    }

    @Test
    public void whenSupplyEventOxygen_thenFestivalDatesConfigurationIsCalledToGetFestivalDurationInDays() {
        // given
        givenOxygenHistoryInventoryAndSupplyMethodProviderReturnHistoryInventoryAndSupplyMethod();
        when(oxygenInventory.useAvailableTanks(0, A_GRADE, A_GRADE)).thenReturn(1);

        // when
        oxygenService.supplyOxygenForAllFestivalDates(A_DATE, A_GRADE, A_NUMBER_OF_TANKS_NEEDED_DAILY);

        // then
        verify(festivalDates).getFestivalDurationInDays();
    }

    @Test
    public void whenSupplyOxygenForADay_thenCostOfOxygenIsAddedToExpenses() {
        // given
        givenOxygenHistoryInventoryAndSupplyMethodProviderReturnHistoryInventoryAndSupplyMethod();
        when(oxygenInventory.useAvailableTanks(A_NUMBER_OF_TANKS_NEEDED_DAILY, A_GRADE, A_GRADE)).thenReturn(1);

        // when
        oxygenService.supplyDailyEventOxygen(A_DATE, A_GRADE, A_NUMBER_OF_TANKS_NEEDED_DAILY);

        // then
        verify(transactionRepository).add(any(Transaction.class));
    }

    @Test
    public void whenSupplyOxygenForAllFestivalDays_thenCostOfOxygenIsAddedToExpenses() {
        // given
        givenOxygenHistoryInventoryAndSupplyMethodProviderReturnHistoryInventoryAndSupplyMethod();
        when(oxygenInventory.useAvailableTanks(0, A_GRADE, A_GRADE)).thenReturn(1);

        // when
        oxygenService.supplyOxygenForAllFestivalDates(A_DATE, A_GRADE, A_NUMBER_OF_TANKS_NEEDED_DAILY);

        // then
        verify(transactionRepository).add(any(Transaction.class));
    }

    @Test
    public void whenGetAllOxygenHistories_thenAllOxygenHistoriesInRepositoryAreReturnedOrderedByDate() {
        // when
        OxygenHistory earliestOxygenHistory = new OxygenHistory(EARLIEST_DATE);
        OxygenHistory middleOxygenHistory = new OxygenHistory(DATE_IN_THE_MIDDLE);
        OxygenHistory latestOxygenHistory = new OxygenHistory(LATEST_DATE);
        when(oxygenHistoryRepository.getAll())
                .thenReturn(Arrays.asList(latestOxygenHistory, earliestOxygenHistory, middleOxygenHistory));
        List<OxygenHistory> oxygenHistories = oxygenService.getAllOxygenHistories();

        // then
        assertThat(oxygenHistories).containsExactly(earliestOxygenHistory, middleOxygenHistory, latestOxygenHistory)
                .inOrder();
    }

    private void givenOxygenHistoryInventoryAndSupplyMethodProviderReturnHistoryInventoryAndSupplyMethod() {
        when(oxygenSupplyMethodFactory.getLowestGradeSupplyMethod(A_GRADE, A_DATE))
                .thenReturn(AN_OXYGEN_SUPPLY_METHOD);
        when(oxygenHistoryRepository.findByDate(A_DATE)).thenReturn(Optional.of(AN_OXYGEN_HISTORY));
    }
}
