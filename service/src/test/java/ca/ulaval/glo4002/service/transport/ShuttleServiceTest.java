package ca.ulaval.glo4002.service.transport;

import ca.ulaval.glo4002.domain.event.FestivalDates;
import ca.ulaval.glo4002.domain.event.exception.InvalidEventDateException;
import ca.ulaval.glo4002.domain.transport.repository.ShuttleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class ShuttleServiceTest {

    private static final LocalDate AN_EVENT_DATE = LocalDate.of(2050, 7, 17);

    private ShuttleService shuttleService;

    @Mock
    private ShuttleRepository shuttleRepository;

    @Mock
    private FestivalDates festivalDates;

    @BeforeEach
    public void setUp() {
        shuttleService = new ShuttleService(shuttleRepository, festivalDates);
    }

    @Test
    public void whenGetShuttles_thenRepositoryIsCalledToGetShuttlesByDate() {
        // when
        when(festivalDates.isDateInFestivalDates(AN_EVENT_DATE)).thenReturn(true);
        shuttleService.getShuttles(AN_EVENT_DATE);

        // then
        verify(shuttleRepository).findByEventDate(AN_EVENT_DATE);
    }

    @Test
    public void whenGetShuttlesAndDateIsNotInFestivalDates_thenThrowInvalidEventDateException() {
        // then
        Assertions.assertThrows(InvalidEventDateException.class, () -> {
            // when
            when(festivalDates.isDateInFestivalDates(AN_EVENT_DATE)).thenReturn(false);
            shuttleService.getShuttles(AN_EVENT_DATE);
        });
    }

    @Test
    void whenGetAllShuttles_thenShuttleRepositoryIsCalledToGetAllShuttles() {
        // when
        shuttleService.getAllShuttles();

        // then
        verify(shuttleRepository).getAllShuttles();
    }

}