package ca.ulaval.glo4002.service.program;

import ca.ulaval.glo4002.domain.event.FestivalDates;
import ca.ulaval.glo4002.domain.program.exception.InvalidProgramException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProgramValidatorTest {

    private static final LocalDate A_DATE = LocalDate.of(2019, 1, 1);
    private static final List<LocalDate> SOME_DATES = Collections.singletonList(A_DATE);
    private static final String AN_ARTIST_NAME = "an artist name";
    private static final String OTHER_ARTIST_NAME = "other artist name";

    @Mock
    private FestivalDates festivalDates;

    private ProgramValidator programValidator;

    @BeforeEach
    public void setUp() {
        programValidator = new ProgramValidator(festivalDates);
    }

    @Test
    public void whenValidateNoEventDateIsMissingOrDuplicateAndEventDateIsMissingOrDuplicate_thenThrowInvalidProgramException() {
        // then
        Assertions.assertThrows(InvalidProgramException.class, () -> {
            // when
            when(festivalDates.doDatesContainExactlyAllFestivalDates(SOME_DATES)).thenReturn(false);
            programValidator.validateNoEventDateIsMissingOrDuplicate(SOME_DATES);
        });
    }

    @Test
    public void whenValidateNoEventDateIsMissingOrDuplicateAndThereIsNoMissingOrDuplicateEventDate_thenDoNotThrowAnyException() {
        // then
        Assertions.assertDoesNotThrow(() -> {
            // when
            when(festivalDates.doDatesContainExactlyAllFestivalDates(SOME_DATES)).thenReturn(true);
            programValidator.validateNoEventDateIsMissingOrDuplicate(SOME_DATES);
        });
    }

    @Test
    public void whenValidateNoArtistsDuplicateAndArtistNameIsPresentMoreThanOneTime_thenThrowInvalidProgramException() {
        // then
        Assertions.assertThrows(InvalidProgramException.class, () -> {
            // when
            List<String> artistNames = Arrays.asList(AN_ARTIST_NAME, AN_ARTIST_NAME);
            programValidator.validateNoArtistsDuplicate(artistNames);
        });
    }

    @Test
    public void whenValidateNoArtistsDuplicateAndNoArtistNameIsDuplicate_thenDoNotThrowAnyException() {
        // then
        Assertions.assertDoesNotThrow(() -> {
            // when
            List<String> artistNames = Arrays.asList(AN_ARTIST_NAME, OTHER_ARTIST_NAME);
            programValidator.validateNoArtistsDuplicate(artistNames);
        });
    }
}
