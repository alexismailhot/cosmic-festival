package ca.ulaval.glo4002.service.pass;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ca.ulaval.glo4002.domain.order.VendorCode;
import ca.ulaval.glo4002.domain.oxygen.OxygenGrade;
import ca.ulaval.glo4002.domain.pass.DailyEventPass;
import ca.ulaval.glo4002.domain.pass.EventPass;
import ca.ulaval.glo4002.domain.pass.PassCategory;
import ca.ulaval.glo4002.domain.pass.PassNumber;
import ca.ulaval.glo4002.domain.pass.factory.EventPassFactory;
import ca.ulaval.glo4002.domain.pass.repository.EventPassRepository;
import ca.ulaval.glo4002.domain.price.Price;
import ca.ulaval.glo4002.domain.transport.ShuttleType;
import ca.ulaval.glo4002.service.order.OrderService;
import ca.ulaval.glo4002.service.order.OrderValidator;
import ca.ulaval.glo4002.service.oxygen.OxygenService;
import ca.ulaval.glo4002.domain.transport.TransportBooker;

@ExtendWith(MockitoExtension.class)
public class PassServiceTest {

    private static final PassCategory A_PASS_CATEGORY = PassCategory.NEBULA;
    private static final LocalDate AN_ORDER_DATE = LocalDate.of(2050, 7, 10);
    private static final LocalDate A_DATE = LocalDate.of(2050, 7, 12);
    private static final List<LocalDate> SOME_DATES = Arrays.asList(A_DATE, A_DATE, A_DATE);
    private static final LocalDate AN_EVENT_DATE = LocalDate.of(2050, 7, 12);
    private static final PassNumber A_PASS_NUMBER = new PassNumber(0L);
    private static final Price A_PRICE = Price.of(100);
    private static final VendorCode A_VENDOR_CODE = VendorCode.TEAM;

    @Mock
    private EventPassFactory eventPassFactory;

    @Mock
    private EventPassRepository eventPassRepository;

    @Mock
    private OrderService orderService;

    @Mock
    private TransportBooker transportBooker;

    @Mock
    private OxygenService oxygenService;

    @Mock
    private OrderValidator orderValidator;

    private PassService passService;
    private EventPass eventPass;
    private DailyEventPass dailyEventPass;

    @BeforeEach
    public void setUp() {
        passService = new PassService(eventPassFactory, eventPassRepository, orderService, transportBooker, oxygenService, orderValidator);
        eventPass = createDefaultEventPass();
        dailyEventPass = createDefaultDailyEventPass();
    }

    @Test
    public void whenBuyDailyEventPasses_thenPassRepositoryIsCalledToAddAsManyEventPassesAsThereAreLocalDates() {
        // when
        when(eventPassFactory.createDailyEventPass(A_PASS_CATEGORY, AN_EVENT_DATE)).thenReturn(dailyEventPass);
        passService.buyDailyEventPasses(AN_ORDER_DATE, A_VENDOR_CODE, A_PASS_CATEGORY, SOME_DATES);

        // then
        int expectedNumberOfPassesAdded = SOME_DATES.size();
        verify(eventPassRepository, times(expectedNumberOfPassesAdded)).add(any(EventPass.class));
    }

    @Test
    public void whenBuyDailyEventPasses_thenOrderValidatorIsCalledToValidateOrderDate() {
        // when
        when(eventPassFactory.createDailyEventPass(A_PASS_CATEGORY, AN_EVENT_DATE)).thenReturn(dailyEventPass);
        passService.buyDailyEventPasses(AN_ORDER_DATE, A_VENDOR_CODE, A_PASS_CATEGORY, SOME_DATES);

        // then
        verify(orderValidator).validateOrderDate(AN_ORDER_DATE);
    }

    @Test
    public void whenBuyDailyEventPasses_thenOrderValidatorIsCalledToValidateLocalDates() {
        // when
        when(eventPassFactory.createDailyEventPass(A_PASS_CATEGORY, AN_EVENT_DATE)).thenReturn(dailyEventPass);
        passService.buyDailyEventPasses(AN_ORDER_DATE, A_VENDOR_CODE, A_PASS_CATEGORY, SOME_DATES);

        // then
        verify(orderValidator).validateDatesAreFestivalDates(SOME_DATES);
    }

    @Test
    public void whenBuyDailyEventPasses_thenTransportBookerIsCalledToBookTransportForEachEventPass() {
        // given
        when(eventPassFactory.createDailyEventPass(A_PASS_CATEGORY, AN_EVENT_DATE)).thenReturn(dailyEventPass);
        passService.buyDailyEventPasses(AN_ORDER_DATE, A_VENDOR_CODE, A_PASS_CATEGORY, SOME_DATES);

        // then
        verify(transportBooker, times(SOME_DATES.size()))
            .bookDailyEventTransport(AN_EVENT_DATE, dailyEventPass.getPassNumber(), ShuttleType.fromPassCategory(A_PASS_CATEGORY));
    }

    @Test
    public void whenBuyDailyEventPasses_thenOxygenServiceIsCalledToSupplyOxygenForEachEventPass() {
        // given
        when(eventPassFactory.createDailyEventPass(A_PASS_CATEGORY, AN_EVENT_DATE)).thenReturn(dailyEventPass);
        passService.buyDailyEventPasses(AN_ORDER_DATE, A_VENDOR_CODE, A_PASS_CATEGORY, SOME_DATES);

        // then
        OxygenGrade expectedOxygenGrade = A_PASS_CATEGORY.getMinimumOxygenGrade();
        int expectedNumberOfTanksNeeded = A_PASS_CATEGORY.getDailyOxygenNeeds();
        verify(oxygenService, times(SOME_DATES.size()))
            .supplyDailyEventOxygen(AN_ORDER_DATE, expectedOxygenGrade, expectedNumberOfTanksNeeded);
    }

    @Test
    public void whenBuyEventPass_thenPassRepositoryIsCalledToAddAnEventPass() {
        // when
        when(eventPassFactory.createEventPass(A_PASS_CATEGORY)).thenReturn(eventPass);
        passService.buyEventPass(AN_ORDER_DATE, A_VENDOR_CODE, A_PASS_CATEGORY);

        // then
        verify(eventPassRepository).add(eventPass);
    }

    @Test
    public void whenBuyEventPass_thenOrderValidatorIsCalledToValidateOrderDate() {
        // when
        when(eventPassFactory.createEventPass(A_PASS_CATEGORY)).thenReturn(eventPass);
        passService.buyEventPass(AN_ORDER_DATE, A_VENDOR_CODE, A_PASS_CATEGORY);

        // then
        verify(orderValidator).validateOrderDate(AN_ORDER_DATE);
    }

    @Test
    public void whenBuyEventPass_thenTransportBookerIsCalledToBookTransportForEventPass() {
        // when
        when(eventPassFactory.createEventPass(A_PASS_CATEGORY)).thenReturn(eventPass);
        passService.buyEventPass(AN_ORDER_DATE, A_VENDOR_CODE, A_PASS_CATEGORY);

        // then
        verify(transportBooker).bookEventTransport(eventPass.getPassNumber(), ShuttleType.fromPassCategory(A_PASS_CATEGORY));
    }

    @Test
    public void whenBuyEventPass_thenOxygenServiceIsCalledToSupplyOxygenForAllFestivalDays() {
        // when
        when(eventPassFactory.createEventPass(A_PASS_CATEGORY)).thenReturn(eventPass);
        passService.buyEventPass(AN_ORDER_DATE, A_VENDOR_CODE, A_PASS_CATEGORY);

        // then
        OxygenGrade expectedOxygenGrade = A_PASS_CATEGORY.getMinimumOxygenGrade();
        int expectedNumberOfTanksNeededDaily = A_PASS_CATEGORY.getDailyOxygenNeeds();
        verify(oxygenService).supplyOxygenForAllFestivalDates(AN_ORDER_DATE, expectedOxygenGrade, expectedNumberOfTanksNeededDaily);
    }

    private DailyEventPass createDefaultDailyEventPass() {
        return new DailyEventPass(A_PASS_NUMBER, A_PASS_CATEGORY, AN_EVENT_DATE, A_PRICE);
    }

    private EventPass createDefaultEventPass() {
        return new EventPass(A_PASS_NUMBER, A_PASS_CATEGORY, A_PRICE);
    }
}
