package ca.ulaval.glo4002.organisation.repositories;

import ca.ulaval.glo4002.organisation.domain.Artist;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

public interface ArtistRepository extends Repository<Artist, Integer> {
  @RestResource(exported = false)
  Artist findOneById(@Param("id") Integer id);

  @RestResource(exported = false)
  List<Artist> findAll();

  @RestResource(exported = false)
  Artist save(Artist artist);
}