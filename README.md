# Cosmic festival

## What is it
* School project carried out as part of the _"Qualité et métrique du logiciel (Software quality and metrics)"_ course in fall 2019, at Laval University.
* **Project definition:** Event management system for a cosmic festival.
* **Features:** Purchase of passes, management of customer transport, management of oxygen (it's a cosmic festival), management of guest artists, management of festival programming and management of income.

## How to use

* With Java 8 and Maven installed, run `mvn clean install` then `mvn exec:java -pl application`

## Technologies

* Java
* Maven
* Jetty
* Jersey
* Jackson 
* Mockito
* Google Truth
